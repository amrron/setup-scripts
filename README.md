# AmRRON Setup Scripts

# Installation

You will need git to clone this repository on your system.  Install git with:

    sudo apt install git

Now clone the repository to your local system:

    git clone https://gitlab.com/amrron/setup-scripts.git

Move into the new directory:

    cd setup-scripts 

To get updates to the scripts, ensure you are in the setup-scripts directory and run:

    git pull

# Running the scripts

To run a script simply type:

    ./filename

Alternatively, a bash 'debug' mode can be used to see what the script is doing by running with:

    bash -x filename

If the script is not already executable, then run:
    
    chmod +x filename

You could also run the script by double clicking and selecting 'Run in terminal' from you file browser but any errors will simply close the session and you won't know what errored or why.

If you run into errors, please attempt again and run with bash -x to see the output.  If you can provide the final section of that that shows the error along with your system information it will assist in troubleshooting.   Thank you.

# Setup-Guide

Please read the included AmRRON-Setup-Guide.pdf.  This document is a full walkthrough of installing linux and how to use the scripts.  It will also walk through the basic setup of each program.

        

