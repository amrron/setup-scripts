# Tri-Mode Setup Changelog
# Version 0.7.3
- Added git repo checking before the script runs instead of just a note.
# Version 0.7.2
- added Mint 22.1 to the supported list
# Version 0.7.1
- Added version checking support for Mint 22.
# Version 0.7
- Added prompt for running git pull command.
# Version 0.6.3
 - Added Linux Mint version checking for 21.1 / 21.2 / 21.3

# Version 0.6.2
 - Updated OS support list

# Version 0.6.1
 - Changed to pull Corps icon from local instead of the web.

# Version 0.6
 - Added "Easy Button" for quick setup of USB controlled radios with FLRIG control.

# Version 0.5.3
 - Added FTDX-3000 support

# Version 0.5.2
 - Initial release
