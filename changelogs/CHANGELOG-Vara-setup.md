# Version 1.0
- Added git repo checking before the script runs instead of just a note.
# Version 0.9.4.1
- Edited the note to reference com33.
# Version 0.9.4
- Added instructions for adding com port mapping for RMS Express
# Version 0.9.3
- Fixing issues with Mint 22.1 support
# Version 0.9.2
- added Mint 22.1 to the supported list
# Version 0.9.1
- Fixed errors, cleaned up code, added RMS Express support. Added menus.  Still need to clean up menu.
# Version 0.9.0
- Fixed Mint 22 OS detection support.  Added VarAC to install if running Mint 22.
# Version 0.8.2
- Modified Vara HF download link.  Moved DLL download to prior to VARA download in case link breaks.
# Version 0.8.1
- Again modified link address due to extra spaces input in filename.
# Version 0.8
- Added prompt for running git pull command.
# Version 0.6
- Another edit because download link format changed.
# Version 0.5
- Download link changed and added additional space.  Hard coded into link for current version.  Will likely break on version change.

# Version 0.4
- Updated Linux Mint version checking through 21.3
- Updated download links to point to downloads.winlink.org
- Removed MegaDL

# Version 0.3
- Installs on Bullseye 64bit.

# Version 0.2
- testing sudo keepalive and Mint 20.3 support
- basic shell for pi setup - Not working on Buster, testing Bullseye soon.

# Version 0.1
- initial testing for Vara install on Mint 21.
