# Version 1.2.1
- Added --use-dummy-sudo-password to veracrypt desktop launcher due to bug in current veracrypt. 1.26.18
# Version 1.1
- Added git repo checking before the script runs instead of just a note.
# Version 1.0.2
- Updated FL Suite links as updates are no longer being sent to sourceforge. JS8Call now compiles with Hamlib 4.6.1. Hamlib 4.5.5 no longer default.  Option left in menu for now.  Added getvaralist / findvara commands for offline vara winlink station lists.
# Version 1.0.1
- Added cleaned version of commstat database to get rid of old entries. Update to PDF.
# Version 1.0
- Release with PDF version 1.0.  Also fixed WSJT-X version checking for RC8
# Version 0.9.8
- Fixed version checking for ublox gpsd. Fix veracrypt version checking. Added Menu option for removing pipewire.
# Version 0.9.7.2
- Changed FLAMP v. 2.2.14 to be default due to transmit bug in 2.2.13
# Version 0.9.7.1
- Changed order of commstat in array.
# Version 0.9.7
- another fix for commstatone automation
# Version 0.9.6
- ardopcf install and ardopgui dependency fix
# Version 0.9.5
- Fixing issues with Mint 22.1 support
# Version 0.9.4
- added Mint 22.1 to the supported list
# Version 0.9.3
- Testing changing order of commstat install.
# Version 0.9.2
- Final CommstatOne Mint 22 fully automated install.
# version 0.9.1
- Fixing Commstat installation on Mint 22 with python virtual environments and Desktop launcher.
# Version 0.9.0
- All items appear to work with Mint 22.  Updated AmRRON Forms link to current version with STATREP 5.1
# Version 0.8.6
- Fixed voacap install issues.
# Version 0.8.5
- Fixed veracrypt and chirp installations on Mint 22.
# Version 0.8.4
- found compiling issue with JS8Call when running Hamlib 4.6.  Set hamlib 4.5.5 option and default to that for easy button.  Alternative is to install 4.6 and use flatpak or appimage version of JS8Call.
# Version 0.8.3
- added options for install of FLMSG 4.0.19 and FLAMP 2.2.14 for stability testing.
# Version 0.8.2
- added Virginia to Comstat support
# Version 0.8.1
- Fixed email address for contacting me for issues.  Now tangobravo14@protonmail.com

# Version 0.8
- Added checking for script currency.
# Version 0.7.5.4
- Removed Electrum install option.  No longer supported via this script.

# Version 0.7.5.3
- Fixed WSJTX install
- Moved ICOM Grid Square Map to setup-scripts/Files for storage.  Icom no longer hosts file.

# Version 0.7.5.2
- Added Linux Mint version checking for 21.1 / 21.2 / 21.3

# Version 0.7.5.1
- Added install option for Kleopatra GnuPG key manager and crypto GUI
- Changed Easy button / Security tools Install All option to choose Kleopatra instead of GPA.

# Version 0.7.5.0
- Fixed Hamlib link
- Moved Chirp to Chirp Next
- Fixed Voacapl for Mint 21.1
- Fixed Mint 21.1 Version checking
- Fixed Veracrypt links
- Fixed WSJTX Download
- Fixed FLWrap compile error

# Version 0.7.4.5
- Added ComstatX for Mint installations

# Version 0.7.4.4
- Added sudo keepalive loop for mint installs

# Version 0.7.4.3
- Testing g++ dependency for hamlib
- Removed auto removal of source directories.
- Added VARIM to install
- Tweaked dialout add function
- Switched Chirp and Keepassxc to flatpak install due to snap failures on Mint / snap installation on raspberry pi
- Created "Easy Button" to install everthing to default locations with one click.

# Version 0.7.4.2
- Pulling ardopc from gitlab folder due to website release version issues.

# Version 0.7.4.1
- changed ardopc download to ardopc64 and rename to ardopc for ease of integration.  
- fixed change to make ardop executable

# Version 0.7.4
- added function to add Linux Mint user to dialout group.

# Version 0.7.3
- More clean up for os detection and dependencies
- Removed stated support for old operating systems.  Stated support for Buster, Bullseye(32/64bit), Mint 20.3/21.
- Bitmessage info
- Fixed Hamlib from reinstalling if already up to date.
- Fixed JS8Call from reinstalling if already up to date.
- Fixed PAT version check
- More Bitmessage info changes.
- Veracrypt system check
- Linuxmint PyBitmessage appimage setup.

# Version 0.7.2
 - FLDIGI raspbian 64bit check
 - Fix 64bit check for diff OS based upon apt avaibility.
 - Veracrypt 64bit check
 - FLDIGI 64bit fix
 - YAAC to jdk-11
 - Changed Pi check from platform release to uname -m aarch64 check.

# Version 0.7.1
 - Mint 21 tweaks
 - ARDOPC file instead of from source
 - remove snap restriction
 - ARDOP GUI deps
 - Fix Veracrypt links.
 - fixed missing sudo for snap block
 - Fixed electrum check for python3
 - fixed gpsd deps check for Deb 10 / Deb 11 systems
 - Bitmessage enabled and distro check
 - Veracrypt version info in selection.
 - Security tools version checking fixes.
 - More version checking and added space cleanup after compiling Radio tools.
 - Re-enabled VOACAP / WSJTX for testing
 - WSJTX from source
 - removed deps for voacap testing
 - WSJTX version check
 - Vanessa FLWRAP from repo due to compile error from source

# Version 0.7.0 - Working
 - Working updates for current release 2022-09-22 Raspios Bullseye 32bit
 - Changed gfortran3 dep to gfortran5 wsjtx
 - Changed piardopc to pull from archived working version
 - Changed hamlib deps to only libusb
 - updated fldigi deps
 - modified more deps, fixed error reporting issue.
 - Changed Chirp Install to be from snap.
 - Changed swap file check to only check if less than 3GB RAM.
 - Fixed Veracrypt for Buster / Bullseye
 - Fixed version checking errors for ARDOP / Veracrypt / ARDOP GUI
 - Disabled WSJTX / VOACAP
 - Changed JS8 to source install
 - Fixed Veracrypt version check formatting
 - Changed ublox gpsd deps
 - disabled bitmessage install
 - tweaked JS8 install function

# Version 0.6.9
 - Corrected wsjt-x address from http to https
 - Added libudev-dev as fldigi dependency per merge request.

# Version 0.6.8
 - Returned Hamlib to version checking after patch for last RC.  Should again update appropriately.

# Version 0.6.7
 - Added required dependency for Electrum.
 
# Version 0.6.6
 - Changed KeepassXC on pi install to use snap installation.
 - Corrected Electrum download link.

# Version 0.6.5
 - Fixed more issues with Veracrypt.  Now installs current version properly.
 - Added dependency for Chirp

# Version 0.6.4
 - Fixed fail to compile for Direwolf
 - Fixed Keepassxc cmake failure due to dependencies.  Still not fully operational
 - Fixed Veracrypt broken link.

# Version 0.6.3
 - Hardcoded Version for Hamlib 4.0-rc2

# Version 0.6.2
 - Increased swap size to 2000 due to increasing demand by FLDIGI.

# Version 0.6.1
 - Re-enabled YAAC 
 - Fixed typos and formatting.
 - Fixed error not displaying PyBitmessage instructions
 - Added note in CLI if no failures found instead of "no such file"

# Version 0.6.0
 - Changed build dependencies for FLDIGI suite and HAMLIB.  This no longer requires enabling source code repositories.  That menu has been disabled for now and will be removed after further testing.
 - JS8Call dependencies not compatible with Ubuntu 19.10 at this time.
 - VOACAP Pythonprop GUI dependencies not compatible with Ubuntu 19.10 at this time.

# Version 0.5.9.3
 - Fixed Electrum version check / install issue
 - Changed to pulling images from local git cloned directory instead of pulling from web each run.
 - Added Bitcoin / Monero donate option to allow users to support development costs.

# Version 0.5.9.2
 - Cleaned up some formatting issues.
 - fixing conky ARDOP indication for Raspbian.

# Version 0.5.9.1
 - Updated JS8Call function to pull more accurate download links when changed.
 - Initial version checking changed for two digit version as currently listed on the website.  Will need to make more flexible.

# Version 0.5.9
 - Added Security Tools - GNU Privacy Assist

# Version 0.5.8
 - Added Security Tools - GTK Hash

# Version 0.5.7
 - Added Other Tools - VIM and Filezilla

# Version 0.5.6
 - Added recognition for Linux Mint 19.2 "Tina".  Not tested yet for dependency support.

# Version 0.5.5

 - Updated script to recognize Debian 10 for the new Raspbian OS.
 - Added dependency portaudio19-dev for build-dep fldigi requirements
 - Allowed script to recognize difference between Stretch and Buster for enabling source code.
 - Removed dead dependency for JS8Call.
 - Added Buster to the the list of supported OS's for wsjtx 
 - Added LibreOffice Suite to Other Tools
 - Fixed Error in BTC price display in Conky Large config.
 - Added recognition for Ubuntu 19.04 Disco Dingo
 - Added Raspbian / Debian Buster Electrum BTC wallet support
 - WSJTX installer not working for Ubuntu 19.04 Disco Dingo yet.  Need to tweak dependencies.

# Version 0.5.4.4

 - Updated JS8Call Script to automatically query current version (1.0 released today.)

# Version 0.5.4.3

 - Updated JS8Call Script to 1.0.0-rc3

# Version 0.5.4.1
 - Modified Conky Script to remove CPU cores.  This avoids a crash of conky if the user's system does not have as many cores as the script.
 - Adds autostart configuration for conky during final cleanup for all systems now instead of only the Pi.
 - Added directory checking to the make icons function in case it is ran by itself. 
# Version 0.5.4


 - Implemented Chainsaws Conky Script modifications
 - Hard coded the JS8Call download to the new link for version 1.0.0-rc1. I'll update the auto version checking for this once he releases downloads on the public website that I can query for up to date versions.  The current solution is no longer reliable.  Either update manually from the groups.io page or check back for updated scripts after the new versions are released.
 - Changed icons creation option to the top menu.
 
 
 
# Version 0.5.3.3

 - Made min dialog size slightly larger.
 - Changed JS8Call default to v0.14.1



 
# Version 0.5.3.2

 - Ublox GPS setup added for using the Ublox GPS for system time.


# Version 0.5.3.1


 - Temporarilly disabled YAAC install due to a dependency error.
 - Added Swap File size detection and increasing to 500 to help with the pi locking up during compiling of FLDIGI version 4.1.00 and greater.



# Version 0.5.3.0

 - Added YAAC APRS software
 - Changed ARDOP / ARDOP_GUI to be built from source on x64 systems.
 - Added missing dependency to enable FLDIGI to recognize the HamLib 3.3 installation during compiling.
 - Added an 'Install All' option the various category menus.
 - Updated default version checking of JS8Call to 0.13.0 for the newest release. 



# Version 0.5.2.1


 - Fixed small date error for eastern time on conky.
  

# Version 0.5.2.0

 - Added an ardop-status program and desktop launcher when ARDOP is selected. This opens a small program to give you the status of the ARDOP TNC, ARDOP_GUI and pat server. You can toggle them on or off for easy management of them in the background. (Still working x64 dependencies for ARDOP_GUI for Mint 18.x and Ubuntu 16.xx, current versions 19+ and 18.04+ are working.) (To add to a present install, select ARDOP and ICONS from the Radio Menu)
 - Cleaned up some error handling with desktop shortcuts.
 - Added Conky along with a few choices for conky configuration files. Autostart must be setup by the user on non-raspbian machines, .
 - Added JS8Call
 - Fixed a window sizing issue on the confirmation of selected items page and figured out scroll text.
 - Made window sizing more consistent and min size a little large for better
   readability on the pi 7" screen.
 - Added an AmRRON Corps image to the menus.
 


# Version 0.5.1.3

 - Copied grid square map to Documents folder where findardop is looking for it.


# Version 0.5.1.2

 - Fixed issue with keepassxc initial build on Pi
 
# Version 0.5.1.1

 - Fixed window size issue on failure log location menu.
 - Fixed WSJT-X dependency issue

# Version 0.5.1

 - Added WSJT-X
 - Fixed a formatting error with line wrap on the menus
 - Added some checking and reporting of most of the programs if already installed


# Version 0.5.0

 - Complete change in display program along with logic change to support YAD. Now Fully GUI rather than terminal based (still need to launch from the terminal in order to input sudo password when prompted.)
 - Implemented failure logic and logging.
 - Implemented checks for YAD installation and install if needed on start
 - Implemented checking for server status to avoid erroring out if a link goes down.
 - More formating clean up as I found the IDE formatting doesnt translate properly.
 - Wrapped some items into functions for easier disabling for testing.
 - Added pulseaudio items for install
 - Changed some of the logic to flow better.
 - Enabled program to open Sofware Sources for Ubuntu and Mint for enabling sources
 - Added VOACAP GUI
 - Added FLWRAP
 - Added ARDOP_GUI
 - Added pulseaudio suite


# Changes in 0.4.7.2

 - Fixed typo on line 333 that created --scrolltext error


# Changes in 0.4.7.1

 - Added Electrum for Linux machines (Mint 19+ and Ubuntu 18.04+ for now)
 - Added Veracrypt (user must finish install due to terminal limitations)
 - Added Hamlib 3.3 for up to date rigctl radios
 - Added FLRIG
 - Switched for all Linux machines to use ardopc instead of ardopc_64 as that is
 depricated.
 - Added RaspAP Access Point tool
 - Cleaned up some formatting (more to do)
 - Created Packet Tools location prompts
 - Fixed Paranoia Text Encrypter desktop launcher
 - Added KM4ACK Find Ardop script and Icom Grid Square Map

# Functions available in 0.4.6.2

FLDIGI FLMSG FLAMP ARDOP ARIM GARIM PAT CHIRP AmRRON-Forms Desktop-Shortcuts
MINICOM GTKTERM PUTTY DIREWOLF GEDIT On-Screen-Keyboard SCREEN SPEEDTEST
FAIL2BAN KEEPASSXC ICEWEASEL SSH-Hardening Paranoia-File-Enrypter(and stego) BITMESSAGE
Disable / Enable WiFi/Bluetooth (Pi3 only)
