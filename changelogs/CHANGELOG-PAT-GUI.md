# PAT-GUI Changelog
# Version 1.1.1
- removed sed argument that messed up aliases
# Version 1.1
- Added git repo checking before the script runs instead of just a note.
# Version 1.0
- Added option to select VARA or ARDOP stations for creating aliases. Fixed errors.
# Version 0.8
- Fixed PAT-GUI to add rig definition to varahf as well as ARDOP
# Version 0.7
- Fixed copy of grid square map to Documents.
# Version 0.6
- Added prompt for running git pull command.
# Version 0.5.3
 - Changed location of pat config.json

# Version 0.5.2
 - Changed to pull Corps icon from local instead of the web.

# Version 0.5.1
 - Initial release
