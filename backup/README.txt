#### Backup and Restore scripts ###

This script is a simple implementation of TAR to create an archive of your configurations files.  The intent is a simple method to backup your configs and modifications from a clean install in order to facilitate an easier restore or fresh install in the future.

The backup.sh script will read the directories or files you have listed in the list-backup.txt file.  The resulting archive, when passed to the restore.sh will attempt to copy the files back to the same locations.

LIMITATIONS:
-- YOU must ensure the files and directories listed in the list-backup.txt file are correct and what you need to backup.
-- YOU must have the same user name (home directory name) on the machine you are restoring to.  e.g.  if your current machine is user AB01 and home directory is then /home/AB01/, the new install must have the same name.
-- If you have special udev rules established, after using the restore.sh script make sure to run 'sudo udevadm control --reload-rules'.   If not, the rules will be removed from your system.

