#!/bin/bash
NOW=$(date +"%m-%d-%Y")
FILE=$1

if [[ -z ${1} ]]; then
echo "You must supply the full path to your backup file you with to restore."
echo
echo
echo "e.g. ./restore.sh /home/pi/setup-scripts/backups/backup.12-19-2019.tar.bz"
exit 0

else
clear
echo 
echo
echo "WARNING: THIS WILL OVERWRITE YOUR SYSTEM WITH THE FILES FROM ${1}."
echo
echo
echo
echo "Do you wish to continue?"
select yn in "Yes" "No"; do
case $yn in
	Yes )   sleep 1
		echo
		echo
		echo "Restoring ${1}...."
		echo 
		echo
		sleep 2
		tar -xvjf $FILE -C / || echo "A problem occured while extracting your backup"
		echo
		echo "${1} has been restored"
		echo
		sleep 1
		break;;
	No ) break;;
	esac
done

fi

