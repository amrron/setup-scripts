#!/bin/bash
 ################################################################################
 ################################################################################
 #######################       AmRRON Vara Setup v.1.0.1   ######################
 #######################        Created by TB-14           ######################
 #######################          23 FEB 2025              ######################
 ################################################################################
 ################################################################################


### This script will perform basic setup for VARA.
### Tested on Mint 22.1
### Using guidance and tools from TK-05, KM4ACK, ptitSeb, Box86, and KI7POL
# Ask for the administrator password upfront
clear;echo
echo "###########################################"
echo "###########################################"
echo "╔═╗╔╦╗╦═╗╦═╗╔═╗╔╗╔  ╔═╗╔═╗╦═╗╔═╗╔═╗";
echo "╠═╣║║║╠╦╝╠╦╝║ ║║║║  ║  ║ ║╠╦╝╠═╝╚═╗";
echo "╩ ╩╩ ╩╩╚═╩╚═╚═╝╝╚╝  ╚═╝╚═╝╩╚═╩  ╚═╝";
echo "###########################################"
echo "###########################################"

if $(sudo -n true); then 
  echo "sudo is active"
else 
echo "You will be prompted for your sudo password"
echo "below in order to install vara dependencies."
echo "###########################################"
echo "###########################################"
sudo -v

# Keep-alive: update existing `sudo` time stamp until script has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
fi

function check_for_updates() {
 ACTION='\033[1;90m'
 FINISHED='\033[1;96m'
 READY='\033[1;92m'
 NOCOLOR='\033[0m' # No Color
 ERROR='\033[0;31m'

 echo
 echo -e ${ACTION}Checking Git repo
 echo -e =======================${NOCOLOR}
 BRANCH=$(git rev-parse --abbrev-ref HEAD)
 if [ "$BRANCH" != "master" ]
 then
   echo -e ${ERROR}Not on master. Aborting. ${NOCOLOR}
   echo
   exit 0
 fi


 git fetch
 HEADHASH=$(git rev-parse HEAD)
 UPSTREAMHASH=$(git rev-parse master@{upstream})

 if [ "$HEADHASH" != "$UPSTREAMHASH" ]
 then
   echo -e ${ERROR}You are not running the most current version of these scripts. Aborting.${NOCOLOR}
   echo -e ${ERROR}Please run "git pull" from the setup-scripts directory to receive updates.${NOCOLOR}
   exit 0
 else
   echo -e ${FINISHED}Current branch is up to date with origin/master.${NOCOLOR}
 fi
}

####  Program setup functions
function yad_install() {
# Find out if we have yad and install if not
echo "Yad is used to create the interactive menus this program uses."
echo "If yad is not installed, we will attempt to install it now."
sleep 2
if ! hash yad 2>/dev/null; then
sudo apt-get -y install yad ||
{ echo "Yad install failed.  Yad is required to run this installer.  Please install yad and try again or check your internet connection."; exit 1; }

fi
}

# Find the screen size. Will default to 800x480 if it can not be detected.
function set_screen_size() {
screen_size=$(xdpyinfo | grep dimensions | \
sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/' | sed s/x/" "/ || echo 800 480)
width=$(echo $screen_size | awk '{print $1}')
height=$(echo $screen_size | awk '{print $2}')
#xdpyinfo  | grep 'dimensions:'
# Divide by two so the dialogs take up half of the screen, which looks nice.
rows=$(( height / 2 ))
columns=$(( width / 2 ))
# Unless the screen is tiny
rows=$(( rows < 400 ? 400 : rows ))
columns=$(( columns < 700 ? 700 : columns ))
c="--width=${columns}"
r="--height=${rows}"
}



# Determine location user is running script from.
SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to
          #resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
     #echo "$DIR"

function check_repo_version() {

    echo "To ensure your scripts are up to date with current features and fixes, \
Quit this installer and run the command 

git pull

in the setup-scripts directory and then relaunch the scripts. If your repo is up to \
date, click continue." | yad \
--image=$DIR/images/corps-icon.png \
--center \
--text-info \
--wrap \
--button="Quit:1" \
--button="Continue:0" \
--title "Ensure your scripts are up to date." \
${r} ${c}
rc=$?
  if [[ $rc == 0 ]]; then
    echo "Continuing with current version"
    elif  [[ $rc == 1 ]]; then
        exit 1
  fi
}

# Create a error function to make life easier later on
function errors() {
echo "${ERRCODE}" >> /tmp/varafailures
  }

#Deterimine computer type.
function noOS_Support() {
  yad --image=$DIR/images/corps-icon.png --center --no-buttons \
    --title "INVALID OS DETECTED" \
    --text "A valid OS was not detected.\n\nThis installer was designed for \
Debian based distros; specifically Linux Mint 21.3 / 22" \
    ${r} ${c}
    exit 1

}

function maybeOS_Support() {
  if (yad --image=$DIR/images/corps-icon.png --center \
  --title "Not Supported OS" \
  --text "Your OS may be compatible with this installer but has not been \
tested.\n\nThis installer has been tested on Linux Mint 21.3 and 22.  It should work on most Debian 10 / 11 based distros. \n\nWould you like to continue anyway?" ${r} ${c}) then
      yad --image=$DIR/images/corps-icon.png --center \
      --title "Not Supported OS" \
      --text "::: Did not detect perfectly supported OS but,\n\n \
::: Continuing installation at user's own risk..." ${r} ${c} ||
      { echo "exiting"; exit 1; }
  else
      yad --image=$DIR/images/corps-icon.png --center \
      --title "Not Supported OS" \
      --text "::: Exiting due to unsupported OS" ${r} ${c}
      exit 1
  fi
}

# Compatibility check for OS
  # if lsb_release command is on their system
  if hash lsb_release 2>/dev/null; then

    PLAT=$(lsb_release -si)
    OSCN=$(lsb_release -sc) # We want this to be trusty xenial bionic cosmic disco; jessie strech buster; sara serena sonya sylvia tara tessa tina

  else # else get info from os-release

    PLAT=$(grep "^NAME" /etc/os-release | awk -F "=" '{print $2}' | tr -d '"' \
    | awk '{print $1}')
    VER=$(grep "VERSION_ID" /etc/os-release | awk -F "=" '{print $2}' | tr -d '"')
    declare -A VER_MAP=(["11"]="bullseye" ["10"]="buster" ["9"]="stretch" ["8"]="jessie" ["16.04"]="xenial" \
    ["14.04"]="trusty" ["18.04"]="bionic" ["18.10"]="cosmic" ["19.04"]="disco" ["19.10"]="eoan" ["20.04"]="focal" ["18"]="sara" ["18.1"]="serena" \
    ["18.2"]="sonya" ["18.3"]="sylvia" ["19"]="tara" ["19.1"]="tessa" ["19.2"]="tina" ["19.3"]="tricia" ["20"]="ulyana" ["20.3"]="una" ["21"]="vanessa" ["21.1"]="vera" ["21.2"]="victoria" ["21.3"]="virginia" ["22"]="wilma" ["22.1"]="xia" )
    OSCN=${VER_MAP["${VER}"]}

  fi

  case ${PLAT} in
    Raspbian|Debian|Linuxmint)
      case ${OSCN} in
        buster|bullseye|una|vanessa|vera|victoria|virginia|wilma|xia)
          ;;
        *)
          maybeOS_Support
          ;;
      esac
      ;;
    *)
      noOS_Support
      ;;
  esac


#create a finish trap
function finish {

if [[ -f /tmp/varafailures ]]; then
rm /tmp/varafailures
fi

}
trap finish EXIT


##make sure we dont already have a cleanup, mastermenu, or results text file

if [[ -f /tmp/varafailures ]]; then
  echo "/tmp/varafailures file previously exists.  Moving to /tmp/varafailures.bak"
  mv /tmp/varafailures /tmp/varafailures.bak
fi

function confirm_os() {

if [[ $OSCN =~ bullseye|una|vanessa|vera|victoria|virginia|wilma|xia ]]; then
  echo "Continuing with install"
else
   yad --image=$DIR/images/corps-icon.png --center \
      --title "Not Supported OS" \
      --text "::: Exiting due to unsupported OS.  This script has only been tested on Raspberry Pi Bullseye, Mint 20.3 and 21 at this time." ${r} ${c}
      exit 1
fi
}

#################
echo 
echo "Checking on current versions of VARA HF and FM"
echo 

VARAHFVER=$(curl -s https://rosmodem.wordpress.com/ | grep -m 1 "VARA HF" | awk -F "VARA HF" '{print $2}' | awk -F " " '{print $1}' | sed s/\ //g)
echo "VARA HF: ${VARAHFVER} available"

VARAFMVER=$(curl -s https://rosmodem.wordpress.com/ | grep -m 1 "VARA FM" | awk -F "VARA FM" '{print $2}' | awk -F " " '{print $1}' | sed s/\ //g)
echo "VARA FM: ${VARAFMVER} available"

#VARACHATVER=$(curl -s https://rosmodem.wordpress.com/ | grep -m 1 "VARA Chat" | awk -F "VARA Chat" '{print $2}' | awk -F " " '{print $1}' | sed s/\ //g)


VARAFMLINK=https://downloads.winlink.org/VARA%20Products/VARA%20FM%20${VARAFMVER}%20setup.zip
echo "VARA FM URL: ${VARAFMLINK}"

VARAHFLINK=https://downloads.winlink.org/VARA%20Products/VARA%20HF%20${VARAHFVER}%20%20setup.zip
echo "VARA HF URL: ${VARAHFLINK}"

#https://downloads.winlink.org/VARA%20Products/VARA%20HF%20v4.8.9%20setup.zip

#VARACHATLINK=$(curl -s https://rosmodem.wordpress.com/ | grep -oP '(?=https://mega.nz).*?(?=" target="_blank" rel="noopener noreferrer">VARA Chat v)')

function primer_menu() {

echo "This program is designed to assist you with installing VARA HF and VARA FM, \
on your linux machine.  The current version has been tested on Mint 21.3, and Mint 22. \

VarAC only works on Mint 22 at this time.\

All future updates will be focused on Mint 22.1 support only.

Super User (sudo) priveledges are required to perform the apt-get \
commands.

If on Mint, you should have been asked for your password in the terminal to perform some \
tasks but you should not run the entire program as Root. \

You will need to accept agreements and interact with the installers.
Wine is set to default to pulseaudio.  You can modify the winetricks sound=pulse \
line of this script to alsa if you desire to use alsa instead.

If you choose to install and use RMS Express, you may need to map the windows com port to \
your radio location (i.e. ttyUSB1).  com33 should default to your /dev/ttyUSB0 device.

If you need to map something else, take a note of these instructions:

From the terminal run: wine regedit
Create a string value entry in HKEY_LOCAL_MACHINE -> Software -> Wine -> Ports
Name: com1
Data: ttyUSB1

This script will: \
check and add user to the dialout, audio, and tty usergroups; \
install wine and winetricks; download and install VARA HF,FM, VarAC, and RMS Express." | yad \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--center \
--title "Welcome to the AmRRON Vara Installer." \
${r} ${c} ||
{ yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }

}



function usergroups() {
echo "adding user to tty, dialout, & audio groups"
sleep 1
MYUSER=$(whoami)
CHK_DIALOUT=$(id -nG | grep dialout)
CHK_TTY=$(id -nG | grep tty)
CHK_AUDIO=$(id -nG | grep audio)

if [ -z "$CHK_DIALOUT" ]; then
echo "Adding user to dialout group"
sudo usermod -a -G dialout $MYUSER ||
{ ERRCODE="Failed to add user to dialout group"; errors; return; }
else
echo "User already in dialout group"
fi

if [ -z "$CHK_TTY" ]; then
echo "Adding user to tty group"
sudo usermod -a -G tty $MYUSER ||
{ ERRCODE="Failed to add user to tty group"; errors; return; }
else
echo "User already in tty group"
fi

if [ -z "$CHK_AUDIO" ]; then
echo "Adding user to audio group"
sudo usermod -a -G audio $MYUSER ||
{ ERRCODE="Failed to add user to audio group"; errors; return; }
else
echo "User already in audio group"
fi
}

function install_wine() {
export WINEARCH=win32

#install dependencies
sudo apt install -y wine-stable winetricks || 
{ ERRCODE="Failed to install wine-stable and winetricks"; errors; return; }

### Version 2 attempt
DISPLAY=0 WINEARCH=win32 wine wineboot ||
{ ERRCODE="Failed to set winearch=win32 and wineboot"; errors; return; }
winetricks -q dotnet35sp1 win7 sound=pulse ||
{ ERRCODE="Failed to set winetricks dotnet35sp1 win7 sound=pulse"; errors; return; }
winetricks -q vb6run win7 sound=pulse || # removed pdh_nt4
{ ERRCODE="Failed to set winetricks vb6run win7 sound=pulse"; errors; return; }

#cleanup cache from installers
rm -rf ~/.cache/winetricks/ ||
{ ERRCODE="Failed to remove ~/.cache/winetricks files.  These can be removed to free space."; errors; return; }

echo "You will need to select your sound system for wine in the following dialog." | yad --image=$DIR/images/corps-icon.png \
--text-info --wrap \
--center --button="gtk-ok:0" \
--title "WINECFG - Select audio." ${r} ${c}
winecfg

}

function install_varahf() {
#download/install VARA HF

if [[ $OSCN = una ]]; then
sudo apt install -y curl libcurl4-openssl-dev libssl-dev ||
{ ERRCODE="Failed to install dependencies for pulling vara installation files"; errors; return; }

elif [[ $OSCN =~ (vera|vanessa|victoria|virginia|wilma|xia) ]]; then

sudo apt install -y curl p7zip-full winbind||
{ ERRCODE="Failed to install dependencies for pulling vara installation files"; errors; return; }

fi

#Install Driver
if [[ ! -f $HOME/.wine/drive_c/windows/system32/pdh.dll ]]; then
cd $HOME/Downloads && wget http://files.k6eta.com/pdh.dll.zip ||
{ ERRCODE="Failed to download k6eta pdh.dll drivers"; errors; return; }
unzip pdh.dll.zip && rm pdh.dll.zip ||
{ ERRCODE="Failed to unzip k6eta drivers"; errors; return; }
mv pdh.dll $HOME/.wine/drive_c/windows/system32 ||
{ ERRCODE="Failed to move drivers to $HOME/.wine/drive_c/windows/system32"; errors; return; }
else
echo "Driver already installed"
fi


cd $HOME/Downloads && wget ${VARAHFLINK} ||
{ ERRCODE="Failed to download VARA HF.  Visit https://downloads.winlink.org/ to download appropriate file.  Unzip this file and install from terminal using the command: wine <filename>"; errors; return; }

unzip VARA\ HF\ $VARAHFVER*.zip ||
{ ERRCODE="Failed to unzip VARA HF"; errors; return; }

#install VARA HF with wine
/usr/bin/wine VARA\ setup\ \(Run\ as\ Administrator\).exe ||
{ ERRCODE="Failed to install VARA HF"; errors; return; }

}


function install_varafm() {

if [[ $OSCN = una ]]; then
sudo apt install -y curl libcurl4-openssl-dev libssl-dev ||
{ ERRCODE="Failed to install dependencies for pulling vara installation files"; errors; return; }

elif [[ $OSCN =~ (vera|vanessa|victoria|virginia|wilma|xia) ]]; then

sudo apt install -y curl p7zip-full winbind||
{ ERRCODE="Failed to install dependencies for pulling vara installation files"; errors; return; }

fi

if [[ ! -f $HOME/.wine/drive_c/windows/system32/pdh.dll ]]; then
cd $HOME/Downloads && wget http://files.k6eta.com/pdh.dll.zip ||
{ ERRCODE="Failed to download k6eta pdh.dll drivers"; errors; return; }
unzip pdh.dll.zip && rm pdh.dll.zip ||
{ ERRCODE="Failed to unzip k6eta drivers"; errors; return; }
mv pdh.dll $HOME/.wine/drive_c/windows/system32 ||
{ ERRCODE="Failed to move drivers to $HOME/.wine/drive_c/windows/system32"; errors; return; }
else
echo "Driver already installed"
fi

cd $HOME/Downloads/ && wget ${VARAFMLINK} ||
{ ERRCODE="Failed to download VARA FM.  Visit https://downloads.winlink.org/ to download appropriate file.  Unzip this file and install from terminal using the command: wine <filename>"; errors; return; }

unzip VARA\ FM\ $VARAFMVER*.zip ||
{ ERRCODE="Failed to unzip VARA FM"; errors; return; }

#install VARA FM with wine
/usr/bin/wine VARA\ FM\ setup\ \(Run\ as\ Administrator\).exe ||
{ ERRCODE="Failed to install VARA FM"; errors; return; }

}




function install_varac() {


if [[ $OSCN =~ wilma|xia ]]; then

if [[ ! -f $HOME/.wine/drive_c/windows/system32/pdh.dll ]]; then
cd $HOME/Downloads && wget http://files.k6eta.com/pdh.dll.zip ||
{ ERRCODE="Failed to download k6eta pdh.dll drivers"; errors; return; }
unzip pdh.dll.zip && rm pdh.dll.zip ||
{ ERRCODE="Failed to unzip k6eta drivers"; errors; return; }
mv pdh.dll $HOME/.wine/drive_c/windows/system32 ||
{ ERRCODE="Failed to move drivers to $HOME/.wine/drive_c/windows/system32"; errors; return; }
else
echo "Driver already installed"
fi

sudo apt install -y curl p7zip-full winbind||
{ ERRCODE="Failed to install dependencies for pulling vara installation files"; errors; return; }

winetricks -q dotnet48 ||
{ ERRCODE="Failed to install winetricks dotnet48"; errors; return; }

cd $HOME/setup-scripts/Files/ && wine VarAC_Installer_V10_4_3.exe ||
{ ERRCODE="Failed to install VarAC"; errors; return; }

else
{ ERRCODE="VarAC is only compatible with Mint 22 at this time."; errors; return; }  

fi

}

function install_rmsexpress() {
cd $HOME/Downloads
wget -q -r -l1 -np -nd -A "Winlink_Express_install_*.zip" https://downloads.winlink.org/User%20Programs ||
{ ERRCODE="Failed to download Winlink Express"; errors; return; }
unzip Winlink_Express_install*.zip && rm Winlink_Express_install*.zip ||
{ ERRCODE="Failed to unzip Winlink Express"; errors; return; }
/usr/bin/wine Winlink_Express_install.exe ||
{ ERRCODE="Failed to Install Winlink Express"; errors; return; }

}

#########################################################################################

function installermenu() {

  yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Programs" \
--text "Select the programs you would like to install or update.  WINE is required for initial install."  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Category" --column="Description" --column="Installed" \
 false "WINE" "Windows compatibility layer" "" \
 false "VARAHF" "Vara HF modem" "" \
 false "VARAFM" "Vara FM modem" "" \
 false "VARAC" "VarAC keyboard to keyboard chat program" "" \
 false "RMSEXPRESS" "Winlink RMS Express" "" \
  ${r} ${c} >>/tmp/results
rc=$?

 if [[ $rc == 2 ]]; then
  securityArray=(WINE VARAHF VARAFM VARAC RMSEXPRESS)

  for i in "${securityArray[@]}"
    do
    echo "$i" >> /tmp/results

    done

  elif  [[ $rc == 1 ]]; then
    yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
    exit 1

  fi

}




################################################################################
#########################  Execute the selections  #############################
################################################################################

#install_wine
#nstallvara_mint
#installvarac
#installrmsexpress

function selection_results() {

if [[ -s /tmp/results ]]; then
cat /tmp/results | yad --text-info --center \
--image=$DIR/images/corps-icon.png \
--button="Quit:1" \
--button="Begin:0" \
--title "Confirm Update Selection" \
--text "You have chosen to install / update the following:" ${r} ${c} || exit 1
 else
    yad --center \
  --image=$DIR/images/donate.png \
  --button="gtk-ok:0" \
  --title "Goodbye" \
  --text "You did not make any selections.\n\nThe installer will now exit.\n\nThank you." ${r} ${c}
    exit 1
 fi


 while read results
 do
   case $results in

    WINE) echo "Installing wine"
        install_wine
        ;;
    VARAHF) echo "Installing VaraHF"
        install_varahf
        ;;
    VARAFM) echo "Installing VaraFM"
        install_varafm
        ;;
    VARAC) echo "Installing VarAC"
        install_varac
        ;;
    RMSEXPRESS) echo "Installing RMS express"
        install_rmsexpress
        ;;
 esac
done < /tmp/results

}



#########################################################################################
function failure_handling() {

FAIL=$(cat /tmp/varafailures) || echo "No failures recorded"


if [[ -f /tmp/varafailures ]]; then
yad --image=$DIR/images/corps-icon.png --center --button="Save Log:2" --button="gtk-ok:0" \
--title "Failures" \
--text "The following failures occured during installation:\n\n$FAIL" ${r} ${c} exit=$?
if [[ $? == 2 ]]; then
ERRORLOG=$HOME/AmRRON-Vara-setup-failures
if [[ -e $ERRORLOG.log ]]; then
    i=1
    while [[ -e $ERRORLOG-$i.log ]] ; do
        let i++
    done
    ERRORLOG=$ERRORLOG-$i
fi
cp /tmp/varafailures $ERRORLOG.log
yad --center --button="gtk-ok:0" \
--image=$DIR/images/corps-icon.png \
--title "Failure Log Saved" \
--text "A copy of the failure log was saved at $ERRORLOG.log" ${r} ${c}
fi

fi

}

function pi_install() {
cd $HOME/Downloads && wget https://raw.githubusercontent.com/WheezyE/Winelink/main/install_winelink.sh

sed -i 's/alsa/pulse/g' install_winelink.sh

bash install_winelink.sh vara_only

}

function complete() {
  rm /tmp/results
echo "All selected updates have been completed." | yad --image=$DIR/images/donate.png \
--text-info --center --wrap --button="gtk-ok:0" \
--title "Complete" ${r} ${c}

echo "You will need to set your sound card usage in each program on first use. 

If you installed VarAC, ensure you check the box for Linux Compatible Mode in the \
Settings -- Misc tab. 

It is recommended to restart your system before continuing.

Would you like to restart now?" | yad --center \
--text-info --wrap \
--image=$DIR/images/corps-icon.png \
--title "Restart" --button="No:1" --button="Restart:2" ${r} ${c}
rc=$?
  if [[ $rc -eq 2 ]]; then
  sudo shutdown -r now
  elif [[ $rc -eq 1 ]]; then
echo "Good luck and 73's.   If you have any issues or suggestions please contact tb14@protonmail.com or submit a merge request on gitlab." | yad --image=$DIR/images/corps-icon.png \
--text-info --wrap \
--center --button="gtk-ok:0" \
--title "Thank you." ${r} ${c}
exit 1
  else
  echo "You closed the window without making a selection.  Installer exiting."

exit 1
  fi

}



check_for_updates
yad_install
set_screen_size
#check_repo_version
confirm_os
primer_menu
if [[ $PLAT = Linuxmint ]]; then
installermenu
usergroups
selection_results
else
  pi_install
fi
failure_handling
complete
