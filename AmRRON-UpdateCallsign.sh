#!/bin/bash
 ################################################################################
 ################################################################################
 #######################  AmRRON Update Callsign v0.4  ##########################
 #######################       Created by Delta-57     ##########################
 #######################          06 Sep 2019          ##########################
 ################################################################################
 ################################################################################


### This script will update callsigns across all the radio applications.

### Check for rpl installed and install it if needed ###
if ! hash rpl 2>/dev/null; then
	sudo apt -y install rpl ||  { echo "The package rpl is needed for this script and failed to install.  Try sudo apt install rpl."; exit 1; }
fi

if [[ $# -eq 0 ]] ; then
    echo 'Please specify either a TO callsign, or both a FROM callsign and a TO callsign.'
    echo 'If only TO callsign is specified, N0CALL is assumed to be FROM callsign.'
    exit 1
fi

FROMCALL="N0CALL"
TOCALL=$1

if [[ $# -eq 2 ]] ; then
    FROMCALL=$1
    TOCALL=$2
fi



echo 'Replacing callsign' $FROMCALL 'with callsign' $TOCALL

#rpl -i -v "$FROMCALL" "$TOCALL" ~/junk.txt
rpl -i -v "$FROMCALL" "$TOCALL" ~/garim/garim.ini
rpl -i -v "$FROMCALL" "$TOCALL" ~/.config/JS8Call.ini
rpl -i -v "$FROMCALL" "$TOCALL" ~/.config/WSJT-X.ini
rpl -i -v "$FROMCALL" "$TOCALL" ~/.nbems/FLAMP/FLAMP.prefs
rpl -i -v "$FROMCALL" "$TOCALL" ~/.nbems/FLMSG.prefs
rpl -i -v "$FROMCALL" "$TOCALL" ~/.fldigi/macros/MacroTxt/CK-IN.txt
rpl -i -v "$FROMCALL" "$TOCALL" ~/.fldigi/fldigi_def.xml
rpl -i -v "$FROMCALL" "$TOCALL" ~/.wl2k/config.json

