#!/bin/bash
 ################################################################################
 ################################################################################
 ######################  AmRRON Update Callsign-GUI v0.4 ########################
 ######################       Created by Delta-57        ########################
 ######################          07 Sep 2019             ########################
 ################################################################################
 ################################################################################

### This needs to be looked at again.


### This script will update callsigns across all the radio applications.

### Check for rpl installed and install it if needed ###
if ! hash rpl 2>/dev/null; then
	sudo apt -y install rpl ||  { echo "The package rpl is needed for this script and failed to install.  Try sudo apt install rpl."; exit 1; }
fi

### Check for yad installed and install if needed 

if ! hash yad 2>/dev/null; then
	sudo apt -y install yad || { echo "The package yad is needed for this script and failed to install. Try sudo apt install yad."; exit 1; }
fi

# Find the screen size. Will default to 800x480 if it can not be detected.
function set_screen_size() {
screen_size=$(xdpyinfo | grep dimensions | \
sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/' | sed s/x/" "/ || echo 800 480)
width=$(echo $screen_size | awk '{print $1}')
height=$(echo $screen_size | awk '{print $2}')
#xdpyinfo  | grep 'dimensions:'
# Divide by two so the dialogs take up half of the screen, which looks nice.
rows=$(( height / 2 ))
columns=$(( width / 2 ))
# Unless the screen is tiny
rows=$(( rows < 400 ? 400 : rows ))
columns=$(( columns < 700 ? 700 : columns ))
c="--width=${columns}"
r="--height=${rows}"
}


function change_call() {
CHANGECS=$(yad --center --wrap --title="Change Station Callsign" --text="Please answer the following selections:" \
--form --separator="," --item-separator="," \
--field="Current Callsign" "N0CALL" \
--field="Desired Callsign" "" \
--field="Tip::TXT" "This process will change your callsign as directed from gARIM, JS8Call, WSJT-X, FLAMP, FLMSG, FLDigi, FLDigi CK-IN Macro, and PAT." ${c} ${r}) || exit 1

FROMCALL=$(echo $CHANGECS | awk -F "," '{print $1}')
TOCALL=$(echo $CHANGECS | awk -F "," '{print $2}')

if [ -z "$TOCALL" ]; then
echo "You must provide a TO Callsign" | yad --center --text-info --wrap --title=ChangeCall --button="Start Over:1" ${c} ${r} ||
	{ change_call; return; }
fi

replace_call
}

function replace_call() {
echo 'Replacing callsign' $FROMCALL 'with callsign' $TOCALL

rpl -i -v "$FROMCALL" "$TOCALL" ~/garim/garim.ini
rpl -i -v "$FROMCALL" "$TOCALL" ~/.config/JS8Call.ini
rpl -i -v "$FROMCALL" "$TOCALL" ~/.config/WSJT-X.ini
rpl -i -v "$FROMCALL" "$TOCALL" ~/.nbems/FLAMP/FLAMP.prefs
rpl -i -v "$FROMCALL" "$TOCALL" ~/.nbems/FLMSG.prefs
rpl -i -v "$FROMCALL" "$TOCALL" ~/.fldigi/macros/MacroTxt/CK-IN.txt
rpl -i -v "$FROMCALL" "$TOCALL" ~/.fldigi/fldigi_def.xml
rpl -i -v "$FROMCALL" "$TOCALL" ~/.wl2k/config.json
}

set_screen_size
change_call

