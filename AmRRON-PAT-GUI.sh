#!/bin/bash
 ################################################################################
 ################################################################################
 #################  AmRRON PAT Setup and Alias Manager v1.1.1  ##################
 #################              Created by TB-14               ##################
 #################                08 FEB 25                    ##################
 ################################################################################
 ################################################################################

 function check_for_updates() {
  ACTION='\033[1;90m'
  FINISHED='\033[1;96m'
  READY='\033[1;92m'
  NOCOLOR='\033[0m' # No Color
  ERROR='\033[0;31m'

  echo
  echo -e ${ACTION}Checking Git repo
  echo -e =======================${NOCOLOR}
  BRANCH=$(git rev-parse --abbrev-ref HEAD)
  if [ "$BRANCH" != "master" ]
  then
    echo -e ${ERROR}Not on master. Aborting. ${NOCOLOR}
    echo
    exit 0
  fi


  git fetch
  HEADHASH=$(git rev-parse HEAD)
  UPSTREAMHASH=$(git rev-parse master@{upstream})

  if [ "$HEADHASH" != "$UPSTREAMHASH" ]
  then
    echo -e ${ERROR}You are not running the most current version of these scripts. Aborting.${NOCOLOR}
    echo -e ${ERROR}Please run "git pull" from the setup-scripts directory to receive updates.${NOCOLOR}
    exit 0
  else
    echo -e ${FINISHED}Current branch is up to date with origin/master.${NOCOLOR}
  fi
 }

function yad_install() {
# Find out if we have yad and install if not
echo "Yad is used to create the interactive menus this program uses."
echo "If yad is not installed, we will attempt to install it now."
sleep 2
if ! hash yad 2>/dev/null; then
sudo apt-get -y install yad ||
{ echo "Yad install failed.  Yad is required to run this installer.  Please install yad and try again or check your internet connection."; exit 1; }

fi
}

function set_screen_size() {
screen_size=$(xdpyinfo | grep dimensions | \
sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/' | sed s/x/" "/ || echo 800 480)
width=$(echo $screen_size | awk '{print $1}')
height=$(echo $screen_size | awk '{print $2}')
#xdpyinfo  | grep 'dimensions:'
# Divide by two so the dialogs take up half of the screen, which looks nice.
rows=$(( height / 2 ))
columns=$(( width / 2 ))
# Unless the screen is tiny
rows=$(( rows < 400 ? 400 : rows ))
columns=$(( columns < 700 ? 700 : columns ))
c="--width=${columns}"
r="--height=${rows}"

}


# Determine location user is running script from.
SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to
          #resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
     #echo "$DIR"

function check_repo_version() {

    echo "To ensure your scripts are up to date with current features and fixes, \
Quit this installer and run the command 

git pull

in the setup-scripts directory and then relaunch the scripts. If your repo is up to \
date, click continue." | yad \
--image=$DIR/images/corps-icon.png \
--center \
--text-info \
--wrap \
--button="Quit:1" \
--button="Continue:0" \
--title "Ensure your scripts are up to date." \
${r} ${c}
rc=$?
  if [[ $rc == 0 ]]; then
    echo "Continuing with current version"
    elif  [[ $rc == 1 ]]; then
        exit 1
  fi
}

function dep_check() {


if ! hash jq 2>/dev/null; then
    sudo apt install -y jq
fi

if ! hash yad 2>/dev/null; then
    sudo apt install -y yad
fi

if ! hash pat 2>/dev/null; then
    echo "Pat is not installed.  Please install pat and try again.

    https://getpat.io" | yad --center --text-info --wrap --image=$DIR/images/corps-icon.png --show-uri --button="Exit:1" --title "Install PAT" ${r} ${c} || exit 1
fi


}


function getardoptools_install() {
  # Get Grid Square Map
  

  if ! hash evince; then
  sudo apt-get install -y evince
fi
  cp $HOME/setup-scripts/Files/2013_GridSquareMap.pdf $HOME/Documents/grid-map.pdf ||
  { ERRCODE="Failed to copy grid-map.pdf to Documents folder for findardop / findvara"; errors; return; }
  # Get ardop list script

if ! hash getardoplist; then
  wget -P $HOME -N https://gist.githubusercontent.com/km4ack/2b5db90724cab67\
ed50cbf48ed223efd/raw/efe402285290b46c700d9d29e285c0686b79314f/getardoplist ||
{ ERRCODE="Failed to download getardoplist script"; errors; return; }

  chmod +x $HOME/getardoplist && sudo mv $HOME/getardoplist /usr/local/bin/ ||
{ ERRCODE="Failed to add getardoplist to /usr/local/bin"; errors; return; }

fi
  # Get find ardop script
if ! hash findardop; then
  wget -P $HOME -N https://gist.githubusercontent.com/km4ack/7af44d08d95215d\
a45be3e69decf6def/raw/1aeff0d78f85e86b868a4ba83ebce99e90532f48/findardop ||
{ ERRCODE="Failed to download findardop script"; errors; return; }

  chmod +x $HOME/findardop && sudo mv $HOME/findardop /usr/local/bin/ ||
{ ERRCODE="Failed to add findardop to /usr/local/bin"; errors; return; }

fi

if ! hash getvaralist; then
  wget -P $HOME -N https://gist.githubusercontent.com/km4ack/2b5db90724cab67\
ed50cbf48ed223efd/raw/efe402285290b46c700d9d29e285c0686b79314f/getardoplist ||
{ ERRCODE="Failed to download getardoplist script"; errors; return; }

  sed 's/ardop/vara/g' $HOME/getardoplist >> $HOME/getvaralist ||
{ ERRCODE="Failed to clone and modify getardop to gitvara"; errors; return; }

  chmod +x $HOME/getvaralist && sudo mv $HOME/getvaralist /usr/local/bin/ ||
{ ERRCODE="Failed to add getvaralist to /usr/local/bin"; errors; return; }
fi
  # Get find ardop script
if ! hash findvara; then
  wget -P $HOME -N https://gist.githubusercontent.com/km4ack/7af44d08d95215d\
a45be3e69decf6def/raw/1aeff0d78f85e86b868a4ba83ebce99e90532f48/findardop ||
{ ERRCODE="Failed to download findardop script"; errors; return; }

    sed 's/ardop/vara/g' $HOME/findardop >> $HOME/findvara ||
{ ERRCODE="Failed to clone and modify getardop to gitvara"; errors; return; }

    chmod +x $HOME/findvara && sudo mv $HOME/findvara /usr/local/bin/ ||
{ ERRCODE="Failed to add findvara to /usr/local/bin"; errors; return; }

fi


}


#Lets grab the AmRRON Banner.
#wget -N http://s9756.storage.proboards.com/5619756/a/KWTGc2YscxSclVEeB5jD.png -O /tmp/banner.png


#create a finish trap
function finish {
  if [[ -f /tmp/patconfig.json ]]; then
rm /tmp/patconfig.json
fi

if [[ -f /tmp/stationlist ]]; then
rm /tmp/stationlist
fi

if [[ -f /tmp/aliaschoices ]]; then
rm /tmp/aliaschoices
fi

if [[ -f /tmp/currentaliases ]]; then
rm /tmp/currentaliases
fi

if [[ -f /tmp/removealiases ]]; then
rm /tmp/removealiases
fi

if [[ -f /tmp/removealiases2 ]]; then
rm /tmp/removealiases2
fi


if [[ -f /tmp/banner.png ]]; then
rm /tmp/banner.png
fi

}
trap finish EXIT


##Some variables needed
PATCONFIG="$HOME/.config/pat/config.json"
TMPFILE="/tmp/patconfig.json"


function skip_setup() {

## Make backup of current config.
cp $PATCONFIG $PATCONFIG.$(date +%h-%d-%Y@%H%M).bak


if ( [[ "$(jq .mycall $PATCONFIG | tr -d \")" != "" ]] && [[ "$(jq .locator $PATCONFIG | tr -d \")" != "" ]] ); then
        findalias_menu
    else
        main_menu
    fi
}


function main_menu() {
if [[ ! -f $PATCONFIG ]]; then
    pat configure | sleep 2 | kill $(pidof pat) ||
        { echo "No config.json file exists.  Please run 'pat configure' from a terminal and then CTRL-X to close the file.  This will generate a blank file which this tool will assist you with."; exit 1; }
    fi

CURRENTCALL=$(jq ".mycall" $PATCONFIG | tr -d \")
CURRENTPASS=$(jq ".secure_login_password" $PATCONFIG | tr -d \")
CURRENTLOCATOR=$(jq ".locator" $PATCONFIG | tr -d \")
CURRENTHTTP=$(jq ".http_addr" $PATCONFIG | tr -d \")
CURRENTRIG=$(jq ".ardop.rig" $PATCONFIG | tr -d \")

PATSETUP=$(yad --center --wrap --title="PAT Setup and Alias Manager" \
--text="--Please ensure you have a valid winlink account and password established.\n--The raspberry pi sometimes has problems understanding 'localhost.' Use 127.0.0.1 Using 0.0.0.0 will allow you to access the PAT UI from your local network.\n--Point your browser to the  local ip and port.  Example: 192.168.1.15:8080" \
--image=$DIR/images/corps-icon.png \
--form --separator="," --item-separator="," \
--field="Enter your callsign" "$CURRENTCALL" \
--field="Enter your winlink password:H" "$CURRENTPASS" \
--field="Enter your station grid square, ex. DN23" "$CURRENTLOCATOR" \
--field="Select or enter your local address and port:"CBE "$CURRENTHTTP,localhost:8080,0.0.0.0:8080,127.0.0.1:8080" \
--field="Radio Model. i.e IC-7300" "$CURRENTRIG" \
--field="Add/Change Rig Definition to PAT Config?:CHK" "False" \
--field="Disable Rig Control for PAT?:CHK" "False" \
${c} ${r}) || exit 1

NEWCALL=$(echo "$PATSETUP" | awk -F "," '{print $1}')
NEWPASS=$(echo "$PATSETUP" | awk -F "," '{print $2}')
NEWLOCATOR=$(echo "$PATSETUP" | awk -F "," '{print $3}')
NEWHTTP=$(echo "$PATSETUP" | awk -F "," '{print $4}')
NEWRIG=$(echo "$PATSETUP" | awk -F "," '{print $5}')
CHANGEDEF=$(echo "$PATSETUP" | awk -F "," '{print $6}')
DISABLEDEF=$(echo "$PATSETUP" | awk -F "," '{print $7}')


if [[ $CURRENTCALL != $NEWCALL ]]; then
    jq '.mycall = '\"$NEWCALL\"'' $PATCONFIG > $TMPFILE
    cp $TMPFILE $PATCONFIG
fi

if [[ $CURRENTPASS != $NEWPASS ]]; then
    jq '.secure_login_password = '\"${NEWPASS}\"'' $PATCONFIG > $TMPFILE
    cp $TMPFILE $PATCONFIG
fi

if [[ $CURRENTLOCATOR != $NEWLOCATOR ]]; then
    jq '.locator = '\"${NEWLOCATOR}\"'' $PATCONFIG > $TMPFILE
    cp $TMPFILE $PATCONFIG
fi

if [[ $CURRENTHTTP != $NEWHTTP ]]; then
    jq '.http_addr = '\"${NEWHTTP}\"'' $PATCONFIG > $TMPFILE
    cp $TMPFILE $PATCONFIG
fi

if [[ $CHANGEDEF == "TRUE" ]]; then

    if [[ $NEWRIG != "" ]]; then

   jq '.hamlib_rigs = {'\"$NEWRIG\"': {"address": "127.0.0.1:4532", "network": "tcp"}}' $PATCONFIG | jq '.ardop.rig = '\"${NEWRIG}\"'' | jq '.varahf.rig = '\"${NEWRIG}\"'' | jq '.ardop.ptt_ctrl = true' | jq '.varahf.ptt_ctrl = true' > $TMPFILE
    cp $TMPFILE $PATCONFIG

    else

	echo "You must enter a Rig name if selecting to change the rig definition before continuing." | yad --center --text-info --wrap --title=TriMode --button="Start Over:1" ${c} ${r} ||
	{ main_menu; return; }


    fi

fi

 if [[ $DISABLEDEF == "TRUE" ]]; then

     if [[ $CHANGEDEF == "TRUE" ]]; then

    echo "You may select EITHER Add/Change or Remove as an option, not both." | yad \
        --center \
        --text-info \
        --wrap -\
        -title=TriMode \
        --button="Start Over:1" \
        ${c} ${r} ||
     { main_menu; return; }

    else

    jq '.ardop.ptt_ctrl = false' $PATCONFIG | jq '.ardop.rig = '\"\"'' | jq 'del(.hamlib_rigs.'\"$CURRENTRIG\"')' > $TMPFILE
     cp $TMPFILE $PATCONFIG
 fi

fi

findalias_menu


}

function findalias_menu() {

LISTLOC="$HOME/Documents"


FINDALIAS=$(yad --center --wrap --show-uri \
    --title="Select Modem" \
    --text="Please choose the modem type you would like to search aliases." \
    --image=$DIR/images/corps-icon.png \
    --button="VARA:3" \
    --button="ARDOP:2" \
    --button="Exit:1" \
    --button="Manage Existing Aliases:4" \
${c} ${r})
rc=$?
if [[ $rc -eq 2 ]]; then
    if [[ -f $LISTLOC/ardop-list/ardoplist.txt ]]; then
    LASTUPDATE=$(cat $HOME/Documents/ardop-list/ardoplist.txt | head -n 1)
    else
       getardoplist | yad --center --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 --title "Downloading ARDOP Station lists" --text "Please wait while I download the current list of ARDOP stations..." ${r} ${c}
    fi
    MODE=ardop
    MODEUPDATE=getardoplist
    LISTLOC="$HOME/Documents/ardop-list"
    LISTTXT=ardoplist.txt
elif [[ $rc -eq 1 ]]; then
    exit 1
elif [[ $rc -eq 3 ]]; then
    if [[ -f $LISTLOC/vara-list/varalist.txt ]]; then
    LASTUPDATE=$(cat $HOME/Documents/vara-list/varalist.txt | head -n 1)
    else
       getvaralist | yad --center --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 --title "Downloading VARA Station lists" --text "Please wait while I download the current list of VARA stations..." ${r} ${c}
    fi
    MODE=vara
    MODEUPDATE=getvaralist
    LISTLOC="$HOME/Documents/vara-list"
    LISTTXT=varalist.txt
elif [[ $rc -eq 4 ]]; then
    { manage_aliases; return; }
fi




VIEWER="evince -w"
GRIDMAP="$HOME/Documents/grid-map.pdf"
BANDS=$(echo 20M,30M,40M,80M)
GRIDS=$(echo CN,DN,EN,FN,CM,DM,EM,FM,DL,EL,FL)
if [[ $(jq .$MODE.rig $PATCONFIG | tr -d \") == "" ]]; then
    FORMAT=$(echo "Manual Tuning,Rigctl Tuning")
else
    FORMAT=$(echo "Rigctl Tuning,Manual Tuning")
fi

FINDALIAS=$(yad --center --wrap --show-uri \
    --title="PAT Setup and Alias Manager" \
    --text="$LASTUPDATE\n\nSelect PAT Setup to open user and radio config.\nSelect MANAGE to view or remove current aliases.\nSelect Update to Re-Download the list of stations\nThis may take a couple minutes.\nUpdates will be visable from the PAT UI after restarting the pat http server." \
    --image=$DIR/images/corps-icon.png \
    --button="PAT Setup:3" \
    --button="MANAGE:4" \
    --button="UPDATE:2" \
    --button="Exit:1" \
    --button="Find Stations:0" \
    --form --separator="," --item-separator="," \
    --field="Select desired band:CB" $BANDS \
    --field="Enter Target Grid Square:CBE" "$GRIDS" \
    --field="Click to View Grid Square Map:BTN" "$VIEWER $GRIDMAP" \
    --field="Alias Format.:CB" "$FORMAT" \
${c} ${r})
rc=$?
if [[ $rc -eq 2 ]]; then
    { $MODEUPDATE | yad --center --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 --title "Downloading Station lists" --text "Please wait while I download the current list of stations..." ${r} ${c}; findalias_menu; return; }
elif [[ $rc -eq 1 ]]; then
    exit 1
elif [[ $rc -eq 3 ]]; then
    { main_menu; return; }
elif [[ $rc -eq 4 ]]; then
    { manage_aliases; return; }
fi

BAND=$(echo $FINDALIAS | awk -F "," '{print $1}' | tr [:upper:] [:lower:])
GRID=$(echo $FINDALIAS | awk -F "," '{print $2}')
TUNEMODE=$(echo $FINDALIAS | awk -F "," '{print $4}' | awk '{print $1}')


#cat $LISTLOC/${BAND}${LISTTXT} | awk '$2 ~/'${GRID}'/{print $1,$2,$3,$4,$7,$11}' | sed 's/^/false /' | tr ' ' '\012' > /tmp/stationlist
#cat $LISTLOC/${BAND}${LISTTXT} | awk '$2 ~/'${GRID}'/{print $1,$2,$3,$4,$(NF-4),$(NF-0)}' | sed 's/^/false /' | tr ' ' '\012' | sed 's/&/&amp;/g' > /tmp/stationlist
cat $LISTLOC/${BAND}${LISTTXT} | awk '$2 ~/'${GRID}'/{print $1,$2,$3,$4,$(NF-4),$(NF-0)}' | sed 's/^/false /' | tr ' ' '\012'  > /tmp/stationlist


cat /tmp/stationlist | yad --image=$DIR/images/corps-icon.png --center \
    --checklist \
    --separator="," \
    --title "Stations" \
    --text "Select the stations you would like to add to your PAT alias list" \
    --list \
    --button="Exit:1" \
    --button="Change Band:2" \
    --button="Add Aliases:0" \
    --column="Add to Aliases?" \
    --column="Callsign" \
    --column="Grid-Square" \
    --column="Distance" \
    --column="Direction" \
    --column="Dial Freq" \
    --column="Alias" \
    ${r} ${c} > /tmp/aliaschoices
rc=$?

if [[ $rc -eq 0 ]]; then

while read file;
do
   CALLSIGN=$(echo $file |  awk -F "," '{print $2}')
   DISTANCE=$(echo $file |  awk -F "," '{print $4}')
   FREQUENCY=$(echo $file | awk -F "," '{print $6}')
   if [[ "$TUNEMODE" == "Rigctl" ]]; then
   ALIAS=$(echo $file |  awk -F "," '{print $7}')
   else
   ALIAS=$(echo $file |  awk -F "," '{print $7}' | awk -F "?" '{print $1}')
   fi
   NAME="$CALLSIGN--$DISTANCE--$FREQUENCY"

jq '.connect_aliases += {'\""$NAME"\"' : '\"$ALIAS\"'}' $PATCONFIG > $TMPFILE
cp $TMPFILE $PATCONFIG

done < /tmp/aliaschoices

{ findalias_menu; return; }

elif [[ $rc -eq 2 ]]; then
    { findalias_menu; return; }

elif [[ $rc -eq 1 ]]; then
    exit 1

fi

}

function manage_aliases() {

jq .connect_aliases $PATCONFIG | sed '/^}/d' | sed '/^{/d' | sed 's/^ *//g' | sed 's/^/false /' | tr ' ' '\012' | sed 's/&/&amp;/g' > /tmp/currentaliases



CURRENT=$(cat /tmp/currentaliases | yad --image=$DIR/images/corps-icon.png --center --checklist --separator="," \
    --title "Current Aliases" \
    --text "Select any aliases you would like to remove" \
    --list \
    --column="Saved Alias" \
    --column="Name-Dist-Freq" \
    --column="Alias" \
    --button="Exit:1" \
    --button="Return:2" \
    --button="Apply:0"   ${r} ${c} > /tmp/removealiases)
#--print-column=2 \

rc=$?

if [[ $rc -eq 0 ]]; then
sed -i 's/TRUE,//g' /tmp/removealiases
awk -F ":" '{print $1}' /tmp/removealiases > /tmp/removealiases2

while read file;
do
   jq 'del(.connect_aliases.'$file')' $PATCONFIG > $TMPFILE

   cp $TMPFILE $PATCONFIG

done < /tmp/removealiases2

{ findalias_menu; return; }


elif [[ $rc -eq 2 ]]; then

    findalias_menu
elif [[ $rc -eq 1 ]]; then
    exit 1
fi



}



check_for_updates
yad_install
set_screen_size
#check_repo_version
dep_check
getardoptools_install
skip_setup
