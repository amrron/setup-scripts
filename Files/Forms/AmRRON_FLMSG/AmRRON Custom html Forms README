New AmRRON Custom Forms (2023) | V 5.0  

The new AmRRON Custom Forms are now available for download, and implementation is effective as of 25 October 2023.  These forms will be included in the next Version 5.0 of the AmRRON Signals Operating Instructions. 

*STATREP | amrron_statrep_V5.00.html	Significant changes
SPOTREP | amrron_spotrep_V5.00.html		No changes other than V5.0 file name
SITREP | amrron_sitrep_V5.00.html		Significant changes
Blank Form | amrron_blank_Form_V5.00.html	Noticeable changes 

    *STATREP V5.1 was added to the inventory on 27 November 2024, replacing amrron_statrep_V5.00.html, with
     significant improvements and optimization.  V5.00 will be obsolete and no longer used after Januay 01, 2025.

You can find the new forms at:  https://amrron.com/amrron-forms/ 

If you a new to AmRRON custom forms, or need a refresher on downloading and installing, go to:
https://amrron.com/downloading-custom-forms/ 


Purpose of new forms:  We’re continually striving to improve how we operate over radio.  Over time our operators have shared their observations and have made recommendations for improving.   We look for ways we can be more effective, efficient, or otherwise streamline processes for sharing messages and information across the network over radio.  

ALL of the forms to which we’ve made changes now have a sample file naming convention in the header of the form.  This will aid the operator with structuring the file name after filling out a report, and increase standardization across the network:

 	Sample file naming protocol: "CALL-ST-RR-YYMMDD-HHMMZ-REPORT"

The primary impetus for revisiting and updating the forms was to better delineate between the previous Status Report (STATREP), and the Situation Report (SITREP).  

A STATREP is intended to provide a “snapshot” of the situation at a given location – a systematic way of reporting what services are affected, and to what degree, as it relates to disruptions in the ability for one to conduct normal operations, or life activities.  The STATREP’s primary focus is to bring attention to deviations from “the norm” for that location.  Do you have power?  Cell phone?  Internet?  Is the immediate area stable and status quo, or has something changed, impacting normal activities? 
A small text field is provided for adding very brief descriptions, or helpful text to aid in context of the report.  

A SITREP is intended to provide a narrative – a report – of a developing situation, or to provide updates to an ongoing situation, with the emphasis on the necessary details related to a situation.  
The previous SITREP included nearly identical drop down menus as the STATREP, creating confusion as to when or how one report might be submitted compared to another report (STATREP vs. SITREP). 
The drop down categories have been eliminated from the new (V.5.0) SITREP.   

STATREP = a “snapshot”
SITREP =  a “story to tell” 

STATREP V5.0
    • Elimination of the Precedence level field.  If there is an emergency situation, a SITREP should be used.
    • Field 6, changed to ‘Map Pin’ (this drives the color of the pin when using Commstat mapping).  
    • Expanded and improved descriptions of categories in the form instructions portion of the form (this part does not get transmitted, so does not add to the volume of text
      being transmitted). 
    • This form, when received by voice over the air, or as in a custom html form, may easily be used to submit a Commstat STATREP by the receiving station, on behalf of the
      submitting station, adding to the overall aggregate data, providing for everyone’s increased situational awareness. 


SITREP V5.0
Elimination of the drop down categories.  Incident number has been eliminated.  Emphasis is on the Narrative (Field 10).  


AmRRON Blank Form V5.0

    • To/From blocks moved to the top of the fields
    • Removed Message Number field
    • Blank for is used for messages and reports that do not fall within the SITREP/STATREP/SPOTREP parameters.  The Blank form is suitable for a multitude of reports and
      other uses:
        ◦ AIB (AmRRON Intelligence Brief)
        ◦ EXSUM (Executive Summaries)
        ◦ Welfare requests and other welfare related messages
        ◦ Party-to-party messages or requests
        ◦ Schedules, Rosters, or other reference material
        ◦ News, intelligence, or announcements

==============================================

AmRRON Custom Forms 5.0 will be the only officially sanctioned custom html forms for use in AmRRON nets after January 1st, 2024.  However, all stations are strongly encouraged to begin using these latest forms immediately.  

All AmRRON members are encouraged to maintain the previous versions of custom forms in their ‘Custom’ forms folders so they can continue to open and read traffic sent by stations who have not updated their forms to Version 5.0, for some unknown reason.  
