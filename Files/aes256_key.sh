#!/bin/bash

generate_key_random() {
    read -p "Enter the number of keys to generate: " num_keys
    echo

    keys_file_path="aes256_keys.txt"

    for ((i = 1; i <= num_keys; i++)); do
        salt=$(openssl rand -hex 16)  # Generate a random salt

        # Derive a 32-byte key using the passphrase and salt
        key=$(echo -n "$passphrase$salt" | openssl dgst -sha256 | awk '{print $2}')

        # Create a date-time group
        dtg=$(date +"%Y%m%d%H%M%S")

        # Append the derived key to the keys file with the date-time group
        echo "Generated AES-256 Key $i - $dtg: $key" | sed -e 's/SHA2-256(stdin)=//' >> "$keys_file_path"
    done

    echo "Keys saved to: $(realpath "$keys_file_path")"
}

generate_key_user() {
    read -s -p "Enter your passphrase: " passphrase
    echo

    read -s -p "Enter the salt: " salt
    echo

    # Derive a 32-byte key using the passphrase and salt
    key=$(echo -n "$passphrase$salt" | openssl dgst -sha256 | awk '{print $2}')

    keys_file_path="aes256_keys.txt"

    # Create a date-time group
    dtg=$(date +"%Y%m%d%H%M%S")

    # Append the derived key to the keys file with the date-time group
    echo "Generated AES-256 Key - $dtg: $key" | sed -e 's/SHA2-256(stdin)=//' >> "$keys_file_path"

    echo "Key saved to: $(realpath "$keys_file_path")"
}

read -p "Use random values for passphrase and salt? (y/n): " use_random
echo

if [[ $use_random == "y" ]]; then
    generate_key_random
else
    generate_key_user
fi

