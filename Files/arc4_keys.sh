#!/bin/bash

# Number of keys to generate
num_keys=10

# Output file
output_file="arc4-keys.txt"

echo "Generating $num_keys random ARC4 encryption keys of length 10 and saving to $output_file:"

# Create an empty file or overwrite the existing one
> "$output_file"

for ((i=1; i<=$num_keys; i++)); do
    # Generate a random 10-character key using openssl
    key=$(openssl rand -hex 5 | tr -d '\n')
    
    echo "Key $i: $key" >> "$output_file"
done

echo "Keys have been saved to $output_file"
exit 0
