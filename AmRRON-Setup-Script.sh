#!/bin/bash
################################################################################
################################################################################
########################  AmRRON Setup Script v1.2.1    ########################
########################        Created by TB-14        ########################
########################           08 FEB 2025          ########################
################################################################################
################################################################################
#
#
# Sample code gathered from too many soruces to list across the internet.
# Thank you to all the people making guides and posting on StackOverflow or
# any of the other sites that I found answers to my searches.
# Specific attribution to the PiVPN project's script in which OS Detection
# and screen size modeling was used.
#
#
################################################################################
################################################################################
###############################   Instructions  ################################
################################################################################
################################################################################
# This tool will assist with the setup of a linux / raspberry pi AmRRON machine.
# It is designed to check what is installed, compare versions with the newest
# updates, and allow the user to install what they want easily.  Most packages
# are built from source to ensure you have the newest versions compiled for
# your system.
#
# To run this program it is recommended to open a terminal, resize the window
# to full screen, make the script executable and run it. You can make it
# executable by typing:
#
# chmod +x 'filename'
#
# You can run it with ./'filename'
#
# Alternatively, a 'debug' mode can be used to see what errors occur by
# running with:
#
#	bash -x filename
#
# You should also be able to run it by double clicking and selecting 'Run in terminal'
# but any errors will simply close the session and you won't know what errored or why.
#
################################################################################
################################################################################
################################################################################
###  Admin setup for the installer.  Setting window size and determining OS.####
################################################################################
################################################################################
################################################################################
# Ask for the administrator password upfront
clear;echo
echo "###########################################"
echo "###########################################"
echo "╔═╗╔╦╗╦═╗╦═╗╔═╗╔╗╔  ╔═╗╔═╗╦═╗╔═╗╔═╗";
echo "╠═╣║║║╠╦╝╠╦╝║ ║║║║  ║  ║ ║╠╦╝╠═╝╚═╗";
echo "╩ ╩╩ ╩╩╚═╩╚═╚═╝╝╚╝  ╚═╝╚═╝╩╚═╩  ╚═╝";
echo "###########################################"
echo "###########################################"

if $(sudo -n true); then 
  echo "sudo is active"
else 
echo "You will be prompted for your sudo password"
echo "below in order to install dependencies."
echo "###########################################"
echo "###########################################"
sudo -v

# Keep-alive: update existing `sudo` time stamp until script has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
fi

function check_for_updates() {
 ACTION='\033[1;90m'
 FINISHED='\033[1;96m'
 READY='\033[1;92m'
 NOCOLOR='\033[0m' # No Color
 ERROR='\033[0;31m'

 echo
 echo -e ${ACTION}Checking Git repo
 echo -e =======================${NOCOLOR}
 BRANCH=$(git rev-parse --abbrev-ref HEAD)
 if [ "$BRANCH" != "master" ]
 then
   echo -e ${ERROR}Not on master. Aborting. ${NOCOLOR}
   echo
   exit 0
 fi


 git fetch
 HEADHASH=$(git rev-parse HEAD)
 UPSTREAMHASH=$(git rev-parse master@{upstream})

 if [ "$HEADHASH" != "$UPSTREAMHASH" ]
 then
   echo -e ${ERROR}You are not running the most current version of these scripts. Aborting.${NOCOLOR}
   echo -e ${ERROR}Please run "git pull" from the setup-scripts directory to receive updates.${NOCOLOR}
   exit 0
 else
   echo -e ${FINISHED}Current branch is up to date with origin/master.${NOCOLOR}
 fi
}

function yad_install() {
# Find out if we have yad and install if not
echo "Yad is used to create the interactive menus this program uses."
echo "If yad is not installed, we will attempt to install it now."
sleep 2
if ! hash yad 2>/dev/null; then
sudo apt-get -y install yad ||
{ echo "Yad install failed.  Yad is required to run this installer.  Please install yad and try again or check your internet connection."; exit 1; }

fi
}

# Find the screen size. Will default to 800x480 if it can not be detected.
function set_screen_size() {
screen_size=$(xdpyinfo | grep dimensions | \
sed -r 's/^[^0-9]*([0-9]+x[0-9]+).*$/\1/' | sed s/x/" "/ || echo 800 480)
width=$(echo $screen_size | awk '{print $1}')
height=$(echo $screen_size | awk '{print $2}')
#xdpyinfo  | grep 'dimensions:'
# Divide by two so the dialogs take up half of the screen, which looks nice.
rows=$(( height / 2 ))
columns=$(( width / 2 ))
# Unless the screen is tiny
rows=$(( rows < 400 ? 400 : rows ))
columns=$(( columns < 700 ? 700 : columns ))
c="--width=${columns}"
r="--height=${rows}"
}



# Determine location user is running script from.
SOURCE="${BASH_SOURCE[0]}"
     # While $SOURCE is a symlink, resolve it
     while [ -h "$SOURCE" ]; do
          DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
          SOURCE="$( readlink "$SOURCE" )"
          # If $SOURCE was a relative symlink (so no "/" as prefix, need to
          #resolve it relative to the symlink base directory
          [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
     done
     DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
     #echo "$DIR"

#Set image location


# Create a error function to make life easier later on
function errors() {
echo "${ERRCODE}" >> /tmp/failures
	}


### Check if scripts are up to date


function check_repo_version() {

    echo "To ensure your scripts are up to date with current features and fixes, \
Quit this installer and run the command 

git pull

in the setup-scripts directory and then relaunch the scripts. If your repo is up to \
date, click continue." | yad \
--image=$DIR/images/corps-icon.png \
--center \
--text-info \
--wrap \
--button="Quit:1" \
--button="Continue:0" \
--title "Ensure your scripts are up to date." \
${r} ${c}
rc=$?
  if [[ $rc == 0 ]]; then
    echo "Continuing with current version"
    elif  [[ $rc == 1 ]]; then
        exit 1
  fi


}

# Set variables used in the script.

# Determine what is already installed.
function check_installed_programs() {

echo "Please wait while I determine what programs and versions are already present...."

if hash rigctl 2>/dev/null; then
    HAMLIB=$(rigctl --version | awk 'FNR ==1 {print $3}')
        else
            HAMLIB="NOT INSTALLED"
fi


if hash fldigi 2>/dev/null; then
    FLDIGI=$(fldigi --version | awk 'FNR == 1 {print $2}')
        else
            FLDIGI="NOT INSTALLED"
fi

if hash flmsg 2>/dev/null; then
    FLMSG=$(flmsg --version | awk 'FNR == 1 {print $2}')
        else
            FLMSG="NOT INSTALLED"
fi

if hash flamp 2>/dev/null; then
    FLAMP=$(flamp --version | awk 'FNR == 1 {print $2}')
        else
            FLAMP="NOT INSTALLED"
fi

if hash flrig 2>/dev/null; then
    FLRIG=$(flrig --version | awk 'FNR == 1 {print $2}')
        else
            FLRIG="NOT INSTALLED"
fi

if hash flwrap 2>/dev/null; then
    FLWRAP=$(flwrap --version | awk 'FNR == 1 {print $2}')
        else
            FLWRAP="NOT INSTALLED"
fi

if hash pat 2>/dev/null; then
    PAT=$(pat version | awk -F 'v' '{print $2}' | awk -F ' ' '{print $1}')
        else
            PAT="NOT INSTALLED"
fi

if hash chirp 2>/dev/null; then
    CHIRP=$(chirp --version | awk -F 'next-' '{print $2}' | head -c 8)
        else
          if hash chirp-snap 2>/dev/null; then
            CHIRP=$(chirp-snap --version | awk -F 'daily-' '{print $2}' | head -c 8)
            else
            CHIRP="NOT INSTALLED"
          fi
fi

if hash electrum 2>/dev/null; then
	ELECTRUM=$(electrum version --offline)
		else
	ELECTRUM="NOT INSTALLED"
fi

if hash arim 2>/dev/null; then
    ARIM=$(arim --version | head -n1 | awk -F ' ' '{print $2}')
        else
            ARIM="NOT INSTALLED"
fi

if hash garim 2>/dev/null; then
    GARIM=$(garim --version | head -n1 | awk -F ' ' '{print $2}')
        else
            GARIM="NOT INSTALLED"
fi

if hash varim 2>/dev/null; then
    VARIM=$(varim --version | head -n1 | awk -F ' ' '{print $2}')
        else
            VARIM="NOT INSTALLED"
fi

if hash findardop 2>/dev/null; then
    FINDARDOPVER="INSTALLED"
        else
            FINDARDOPVER="NOT INSTALLED"
fi

if hash keepassxc 2>/dev/null; then
    KEEPASSXCVER="INSTALLED"
   else
            KEEPASSXCVER="NOT INSTALLED"
fi

if hash minicom 2>/dev/null; then
    MINICOMVER="INSTALLED"
   else
            MINICOMVER="NOT INSTALLED"
fi

if hash gtkterm 2>/dev/null; then
    GTKTERMVER="INSTALLED"
   else
            GTKTERMVER="NOT INSTALLED"
fi

if hash putty 2>/dev/null; then
    PUTTYVER="INSTALLED"
   else
            PUTTYVER="NOT INSTALLED"
fi

if hash gedit 2>/dev/null; then
    GEDITVER="INSTALLED"
   else
            GEDITVER="NOT INSTALLED"
fi

if hash screen 2>/dev/null; then
    SCREENVER="INSTALLED"
   else
            SCREENVER="NOT INSTALLED"
fi

if hash speedtest 2>/dev/null; then
    SPEEDTESTVER="INSTALLED"
   else
            SPEEDTESTVER="NOT INSTALLED"
fi

if hash matchbox-keyboard 2>/dev/null; then
    KEYBOARDVER="INSTALLED"
   else
            KEYBOARDVER="NOT INSTALLED"
fi

if hash fail2ban-client 2>/dev/null; then
    FAIL2BANVER="INSTALLED"
   else
            FAIL2BANVER="NOT INSTALLED"
fi

if hash iceweasel 2>/dev/null; then
    ICEWEASELVER="INSTALLED"
   else
            ICEWEASELVER="NOT INSTALLED"
fi

if hash pte 2>/dev/null; then
    PTEVER="INSTALLED"
   else
            PTEVER="NOT INSTALLED"
fi

if hash pfe 2>/dev/null; then
    PFEVER="INSTALLED"
   else
	    PFEVER="NOT INSTALLED"
fi

if hash wsjtx 2>/dev/null; then
    WSJTXVER=$(wsjtx_app_version -v | awk -F ' ' '{print $2}')
else
            WSJTXVER="NOT INSTALLED"
fi

if hash conky 2>/dev/null; then
	CONKYVER="INSTALLED"
else
	CONKYVER="NOT INSTALLED"
fi

if hash js8call 2>/dev/null; then
    JS8CALLVER=$(js8call --version | awk -F ' ' '{print $2}')
else
            JS8CALLVER="NOT INSTALLED"
fi

if hash yaac 2>/dev/null; then
	YAACVER="INSTALLED"
else
	YAACVER="NOT INSTALLED"
fi

if hash libreoffice 2>/dev/null; then
	LIBREVER="INSTALLED"
else
	LIBREVER="NOT INSTALLED"
fi

if hash vim 2>/dev/null; then
	VIMVER="INSTALLED"
else
	VIMVER="NOT INSTALLED"
fi

if hash filezilla 2>/dev/null; then
	FZVER="INSTALLED"
else
	FZVER="NOT INSTALLED"
fi

if hash gtkhash 2>/dev/null; then
	GTKHASHVER="INSTALLED"
else
	GTKHASHVER="NOT INSTALLED"
	
fi

if hash gpa 2>/dev/null; then
	GPAVER="INSTALLED"
else
	GPAVER="NOT INSTALLED"
fi

if hash kleopatra 2>/dev/null; then
    KLEOPATRAVER="INSTALLED"
else
    KLEOPATRAVER="NOT INSTALLED"
fi

if hash veracrypt 2>/dev/null; then
  VCVER=$(veracrypt --text --version | awk -F ' ' '{print $2}')
else
  VCVER="NOT INSTALLED"
fi

if hash pybitmessage 2>/dev/null; then
  BMVER="INSTALLED"
elif [ -f $HOME/Security/PyBitmessage*.AppImage ]; then
  BMVER="INSTALLED-AppImage" 
else
  BMVER="NOT INSTALLED"
fi

if hash voacapl 2>/dev/null; then
    VOACAPLVER=$(voacapl --version | awk -F 'release' '{print $2}' | sed s/\ //)
else
    VOACAPLVER="NOT INSTALLED"
fi

if [ -f $HOME/Radio/ARDOP/*.txt ]; then 
    ARDOPCFVER=$(ls $HOME/Radio/ARDOP/v* | awk -F 'ARDOP/v' '{print $2}' | awk -F '.txt' '{print $1}' )
else
    ARDOPCFVER="NOT INSTALLED"
fi

}


# Query what the most current versions available are.  Check that curl is
#available before starting.

function query_updates() {

echo "Checking if curl is installed..."

if ! hash curl 2>/dev/null; then
	sudo apt-get install -y curl
fi

echo "Please wait while I determine the most current versions available...."

NEWHAMLIB=$(curl -s https://sourceforge.net/projects/hamlib/files/latest/download | \
grep -o https://downloads.sourceforge.net/project/hamlib/hamlib/[^/]*/ | \
head -n 1 | awk -F "/" '{print $7}')
if [[ $NEWHAMLIB == "" ]]; then
	NEWHAMLIB="SERVER DOWN"
fi


NEWFLDIGI=$(curl -s https://www.w1hkj.org/files/fldigi/ | grep -Eo fldigi-'[0-9]{1,4}.[0-9]{1,4}.[0-9]{1,4}'.tar.gz | \
    sort -rn | head -n 1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLDIGI == "" ]]; then
	NEWFLDIGI="SERVER DOWN"
fi

echo "10% Complete"

NEWFLMSG=$(curl -s https://www.w1hkj.org/files/flmsg/ | grep -Eo flmsg-'[0-9]{1,4}.[0-9]{1,4}.[0-9]{1,4}'.tar.gz | \
    sort -rn | head -n 1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLMSG == "" ]]; then
	NEWFLMSG="SERVER DOWN"
fi

echo "20% Complete"

NEWFLAMP=$(curl -s https://www.w1hkj.org/files/flamp/ | grep -Eo flamp-'[0-9]{1,4}.[0-9]{1,4}.[0-9]{1,4}'.tar.gz | \
    sort -rn | head -n 1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLAMP == "" ]]; then
	NEWFLAMP="SERVER DOWN"
fi

echo "30% Complete"

NEWFLRIG=$(curl -s https://www.w1hkj.org/files/flrig/ | grep -Eo flrig-'[0-9]{1,4}.[0-9]{1,4}.[0-9]{1,4}'.tar.gz | \
    sort -rn | head -n 1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLRIG == "" ]]; then
	NEWFLRIG="SERVER DOWN"
fi

NEWFLWRAP=$(curl -s https://www.w1hkj.org/files/flwrap/ | grep -Eo flwrap-'[0-9]{1,4}.[0-9]{1,4}.[0-9]{1,4}'.tar.gz | \
    sort -rn | head -n 1 | awk -F "-" '{print $2}' | awk -F ".tar.gz" '{print $1}')
if [[ $NEWFLWRAP == "" ]]; then
	NEWFLWRAP="SERVER DOWN"
fi

echo "40% Complete"

NEWARIM=$(curl -s https://www.whitemesa.net/arim/arim.html | grep -m 1 \
"armv7l.tar.gz" | awk -F '-' '{print $2}')
if [[ $NEWARIM == "" ]]; then
	NEWARIM="SERVER DOWN"
fi

echo "50% Complete"

NEWGARIM=$(curl -s https://www.whitemesa.net/garim/garim.html | grep -m 1 \
"armv7l.tar.gz" | awk -F '-' '{print $2}')
if [[ $NEWGARIM == "" ]]; then
	NEWGARIM="SERVER DOWN"
fi

NEWVARIM=$(curl -s https://www.whitemesa.net/varim/varim.html | grep -m 1 \
"armv7l.tar.gz" | awk -F '-' '{print $2}')
if [[ $NEWVARIM == "" ]]; then
  NEWVARIM="SERVER DOWN"
fi

echo "60% Complete"

NEWPAT=$(curl -s https://github.com/la5nta/pat/releases | grep -m 1 "amd64.deb"\
 | awk -F '_' '{print $2}')
if [[ $NEWPAT == "" ]]; then
	NEWPAT="SERVER DOWN"
fi

echo "70% Complete"

NEWCHIRP=$(curl -sL https://trac.chirp.danplanet.com/download\?stream\=next | \
grep .tar.gz | awk -F 'chirp-' '{print $2}' | head -c 8)
if [[ $NEWCHIRP == "" ]]; then
	NEWCHIRP="SERVER DOWN"
fi

echo "80% Complete"

#NEWELECTRUM=$(curl -s https://github.com/spesmilo/electrum/blob/master/RELEASE-\
#NOTES | grep Release | head -n 1 | grep -o '[0-9].[0-9].[0-9]')
#if [[ $NEWELECTRUM == "" ]]; then
#	NEWELECTRUM="SERVER DOWN"
#fi
#NEWELECTRUM="SERVER DOWN"

NEWVOACAPL=$(curl -sL https://api.github.com/repos/jawatson/voacapl/releases/latest | grep tag_name | awk -F 'v.' '{print $2}' | sed s/\",//)
if [[ $NEWVOACAPL == "" ]]; then
    NEWVOACAPL="SERVER DOWN"
fi

echo "90% Complete"

#NEWVC=$(curl -s https://www.veracrypt.fr/en/Downloads.html | sed -n 's/<h3>Latest\ Stable\ Release\ //p' | awk -F '(' '{print $1}' | sed 's/-\ //' | sed 's/\ //' )
#NEWVC=$(curl -s https://www.veracrypt.fr/en/Downloads.html | grep "Linux: Version" | awk -F 'Version' '{print $2}' | awk -F ' ' '{print $1}')
NEWVC=$(curl -s https://www.veracrypt.fr/en/Downloads.html | grep "Latest Stable Release - " | awk -F "- " '{print $2}' | awk -F " " '{print $1}')
if [[ $NEWVC == "" ]]; then
	NEWVC="SERVER DOWN"
fi

if [[ ${PLAT} = "Raspbian" ]]; then
NEWARDOP=$(curl -s https://www.cantab.net/users/john.wiseman/Downloads/Beta/ | grep 'href="piardopc"' | grep -o 20[0-9][0-9]-[0-9][0-9]-[0-9][0-9])
else
NEWARDOP=$(curl -s https://www.cantab.net/users/john.wiseman/Downloads/Beta/ | grep 'href="ardopc"' | grep -o 20[0-9][0-9]-[0-9][0-9]-[0-9][0-9])
fi

if [[ $NEWARDOP == "" ]]; then
	NEWARDOP="SERVER DOWN"
fi

NEWARDOPCF=$(curl -L -s https://github.com/pflarue/ardop/releases/latest | grep -m 1 "Release Ardopcf v" | head -1 | awk -F 'v' '{print $2}' | awk -F ' ' '{print $1}')

if [[ $NEWARDOPCF == "" ]]; then
    NEWARDOPCF="SERVER DOWN"
fi

if [[ ${PLAT} = "Raspbian" ]]; then
NEWARDOPGUI=$(curl -s https://www.cantab.net/users/john.wiseman/Downloads/Beta/ | grep 'href="piARDOP_GUI"' | grep -o 20[0-9][0-9]-[0-9][0-9]-[0-9][0-9])
else
NEWARDOPGUI=$(curl -s https://www.cantab.net/users/john.wiseman/Downloads/Beta/ | grep 'href="ARDOP_GUI"' | grep -o 20[0-9][0-9]-[0-9][0-9]-[0-9][0-9])
fi

if [[ $NEWARDOP == "" ]]; then
	NEWARDOP="SERVER DOWN"
fi


#NEWWSJTX=$(curl -s https://physics.princeton.edu/pulsar/K1JT/wsjtx.html | grep -o "wsjtx_[0-9].[0-9].[0-9]*" | awk -F "_" '{print $2}' | head -n 1)
#NEWWSJTX=$(curl -sL https://sourceforge.net/projects/wsjt/files | grep -o "wsjtx-[0-9].[0-9].[0-9]*" | awk -F "-" '{print $2}' | head -n 1)
#NEWWSJTX=$(curl -sL https://sourceforge.net/projects/wsjt/files | grep "released" | awk -F "title=" '{print $2}' | awk -F "/" '{print $2}' | awk -F "wsjtx-" '{print $2}')
NEWWSJTX=$(curl -sL https://wsjt.sourceforge.io/wsjtx.html | grep "Candidate release" | awk -F "<i>WSJT-X " '{print $2}' | awk -F "</i>" '{print $1}')
NEWWSJTX_STABLE=$(curl -sL https://wsjt.sourceforge.io/wsjtx.html | grep "Latest General Availability (GA) releases" | awk -F "<i>WSJT-X " '{print $2}' | awk -F " " '{print $1}')
NEWJS8CALLVER=$(curl -s http://files.js8call.com/latest.html | grep .tgz | sed 's/.*js8call-/js8call-/;s/<.*$//' | sed 's/js8call-//;s/.tgz.*$//')

echo "100% Complete"

}

function add_dialout() { 

if [[ $PLAT = "Linuxmint" ]]; then
CHK_DIALOUT=$(id -nG | grep dialout)

if [ -z "$CHK_DIALOUT" ]; then
echo "Adding user to dialout group"
sudo usermod -a -G dialout $USER ||
{ ERRCODE="Unable to add user to dialout group.  You will need to run sudo usermod -a -G dialout $USER"; errors; return; }
else
  echo "User already a member of dialout group."
fi
fi

}


#Deterimine computer type.
function noOS_Support() {
	yad --image=$DIR/images/corps-icon.png --center --no-buttons \
		--title "INVALID OS DETECTED" \
		--text "A valid OS was not detected.\n\nThis installer was designed for \
Debian based distros; specifically Raspberry Pi OS Buster and Bullseye (32bit and 64bit), Linux Mint 20.3 and 21." \
		${r} ${c}
		exit 1

}

function maybeOS_Support() {
	if (yad --image=$DIR/images/corps-icon.png --center \
	--title "Not Supported OS" \
	--text "Your OS may be compatible with this installer but has not been \
tested.\n\nThis installer has been tested on Raspberry Pi OS Buster and Bullseye \
(32bit and 64bit), Linux Mint 20.3 and 21.  It should work on most Debian 10 / 11 based distros. \n\nWould you like to continue anyway?" ${r} ${c}) then
	    yad --image=$DIR/images/corps-icon.png --center \
	    --title "Not Supported OS" \
	    --text "::: Did not detect perfectly supported OS but,\n\n \
::: Continuing installation at user's own risk..." ${r} ${c} ||
	    { echo "exiting"; exit 1; }
	else
	    yad --image=$DIR/images/corps-icon.png --center \
	    --title "Not Supported OS" \
	    --text "::: Exiting due to unsupported OS" ${r} ${c}
	    exit 1
	fi
}

function check_os_support() {
# Compatibility check for OS
  # if lsb_release command is on their system
  if hash lsb_release 2>/dev/null; then

    PLAT=$(lsb_release -si)
    OSCN=$(lsb_release -sc) # We want this to be trusty xenial bionic cosmic disco; jessie strech buster; sara serena sonya sylvia tara tessa tina

  else # else get info from os-release

    PLAT=$(grep "^NAME" /etc/os-release | awk -F "=" '{print $2}' | tr -d '"' \
    | awk '{print $1}')
    VER=$(grep "VERSION_ID" /etc/os-release | awk -F "=" '{print $2}' | tr -d '"')
    declare -A VER_MAP=(["11"]="bullseye" ["10"]="buster" ["9"]="stretch" ["8"]="jessie" ["16.04"]="xenial" \
    ["14.04"]="trusty" ["18.04"]="bionic" ["18.10"]="cosmic" ["19.04"]="disco" ["19.10"]="eoan" ["20.04"]="focal" ["18"]="sara" ["18.1"]="serena" \
    ["18.2"]="sonya" ["18.3"]="sylvia" ["19"]="tara" ["19.1"]="tessa" ["19.2"]="tina" ["19.3"]="tricia" ["20"]="ulyana" ["20.3"]="una" ["21"]="vanessa" ["21.1"]="vera" ["21.2"]="victoria" ["21.3"]="virginia" ["22"]="wilma" ["22.1"]="xia" )
    OSCN=${VER_MAP["${VER}"]}

  fi

  case ${PLAT} in
    Raspbian|Debian|Linuxmint)
      case ${OSCN} in
        buster|bullseye|una|vanessa|vera|victoria|virginia|wilma|xia)
          ;;
        *)
          maybeOS_Support
          ;;
      esac
      ;;
    *)
      noOS_Support
      ;;
  esac

###Determine if we have a Pi4
}

###
#create a finish trap
function finish {
  if [[ -f /tmp/results ]]; then
rm /tmp/results
fi

if [[ -f /tmp/mastermenu ]]; then
rm /tmp/mastermenu
fi

if [[ -f /tmp/cleanup ]]; then
rm /tmp/cleanup
fi

if [[ -f /tmp/failures ]]; then
rm /tmp/failures
fi

}
trap finish EXIT


##make sure we dont already have a cleanup, mastermenu, or results text file

if [[ -f /tmp/results ]]; then
	echo "Results file previously exists.  Moving to /tmp/results.bak"
	mv /tmp/results /tmp/results.bak
fi

if [[ -f /tmp/mastermenu ]]; then
	echo "/tmp/mastermenu file previously exists.  Moving to /tmp/mastermenu.bak"
	mv /tmp/mastermenu /tmp/mastermenu.bak
fi

if [[ -f /tmp/cleanup ]]; then
	echo "/tmp/cleanup file previously exists.  Moving to /tmp/cleanup.bak"
	mv /tmp/cleanup /tmp/cleanup.bak
fi

if [[ -f /tmp/failures ]]; then
	echo "/tmp/failures file previously exists.  Moving to /tmp/failures.bak"
	mv /tmp/failures /tmp/failures.bak
fi

################################################################################
################################################################################
####################### Radio Tools Functions ##################################
################################################################################
################################################################################

function hamlib_install() {
if [[ $NEWHAMLIB == "SERVER DOWN" ]]; then
	echo "HAMLIB server unresponsive or broken link." >> /tmp/failures
	return
fi

 if [[ ${HAMLIB} == ${NEWHAMLIB} ]]; then
      echo "HAMLIB is already at the most current version"

     

else
# tcl8.5-dev libsigc++-1.2-dev, Trying libsigc++-2.0-dev tcl8.6-dev

##This was Debian 10 based.
##sudo apt-get install -y cmake build-essential libusb-1.0-0.dev libltdl-dev libusb-1.0-0 libhamlib-utils libsamplerate0 libsamplerate0-dev libsigx-2.0-dev libsigc++-2.0-dev libpopt-dev tcl8.6-dev libspeex-dev libasound2-dev alsa-utils libgcrypt20-dev libpopt-dev libfltk1.3-dev libpng++-dev portaudio19-dev libpulse-dev libportaudiocpp0 libsndfile1-dev ||

#This seems to be all thats needed for Debian 11 based
sudo apt-get install -y libusb-1.0-0.dev libusb-1.0-0 g++ ||

{ ERRCODE="Failed to build dependencies for Hamlib"; errors; return; }

cd $USERDIR && wget -N https://sourceforge.net/projects/hamlib/files/hamlib/${NEWHAMLIB}/hamlib-${NEWHAMLIB}.tar.gz ||
 { ERRCODE="Failed to download Hamlib $NEWHAMLIB"; errors; return; }

tar -xvzf hamlib-${NEWHAMLIB}.tar.gz && rm hamlib-${NEWHAMLIB}.tar.gz && cd hamlib-${NEWHAMLIB} ||
 { ERRCODE="Failed to extract Hamlib $NEWHAMLIB"; errors; return; }


./configure && make && sudo make install ||
 { ERRCODE="Failed to compile Hamlib $NEWHAMLIB"; errors; return; }

sudo ldconfig ||
{ ERRCODE="ldconfig failed"; errors; return; }

#Cleanup space from compiling
#if hash rigctld 2>/dev/null; then
#  sudo rm -r $USERDIR/hamlib-${NEWHAMLIB}
#fi

fi


}

function hamlib4.5.5_install() {
if [[ $NEWHAMLIB == "SERVER DOWN" ]]; then
    echo "HAMLIB server unresponsive or broken link." >> /tmp/failures
    return
fi

 if [[ ${HAMLIB} == 4.5.5 ]]; then
      echo "HAMLIB is already at the most current version"

     

else
# tcl8.5-dev libsigc++-1.2-dev, Trying libsigc++-2.0-dev tcl8.6-dev

##This was Debian 10 based.
##sudo apt-get install -y cmake build-essential libusb-1.0-0.dev libltdl-dev libusb-1.0-0 libhamlib-utils libsamplerate0 libsamplerate0-dev libsigx-2.0-dev libsigc++-2.0-dev libpopt-dev tcl8.6-dev libspeex-dev libasound2-dev alsa-utils libgcrypt20-dev libpopt-dev libfltk1.3-dev libpng++-dev portaudio19-dev libpulse-dev libportaudiocpp0 libsndfile1-dev ||

#This seems to be all thats needed for Debian 11 based
sudo apt-get install -y libusb-1.0-0.dev libusb-1.0-0 g++ ||

{ ERRCODE="Failed to build dependencies for Hamlib"; errors; return; }

cd $USERDIR && wget -N https://github.com/Hamlib/Hamlib/releases/download/4.5.5/hamlib-4.5.5.tar.gz ||
 { ERRCODE="Failed to download Hamlib $NEWHAMLIB"; errors; return; }

tar -xvzf hamlib-4.5.5.tar.gz && rm hamlib-4.5.5.tar.gz && cd hamlib-4.5.5 ||
 { ERRCODE="Failed to extract Hamlib $NEWHAMLIB"; errors; return; }


./configure && make && sudo make install ||
 { ERRCODE="Failed to compile Hamlib $NEWHAMLIB"; errors; return; }

sudo ldconfig ||
{ ERRCODE="ldconfig failed"; errors; return; }

#Cleanup space from compiling
#if hash rigctld 2>/dev/null; then
#  sudo rm -r $USERDIR/hamlib-${NEWHAMLIB}
#fi

fi


}


function fldigi_install() {
if [[ $NEWFLDIGI == "SERVER DOWN" ]]; then
	echo "FLDIGI server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLDIGI} == ${NEWFLDIGI} ]]; then
    	echo "FLDIGI is already at the most current version"

     else

       #if apt-cache show libjpeg62-turbo-dev &>/dev/null; then
       #   LIBJPG=libjpeg62-turbo-dev 
       #     else
       #   LIBJPG=libjpeg9-dev
       # fi
       if [[ $(uname -m) = aarch64 ]]; then
        LIBJPG=libjpeg62-turbo-dev
      else
        LIBJPG=libjpeg9-dev
      fi



    echo "Installing / Updating FLDIGI version ${NEWFLDIGI}"
#		sudo apt-get install -y cmake build-essential libusb-1.0-0.dev libudev-dev libltdl-dev libusb-1.0-0 libsamplerate0 libsamplerate0-dev libsigx-2.0-dev libsigc++-2.0-dev libpopt-dev tcl8.6-dev libspeex-dev libasound2-dev alsa-utils libgcrypt20-dev libpopt-dev libfltk1.3-dev libpng++-dev portaudio19-dev libpulse-dev libportaudiocpp0 libsndfile1-dev ||
		sudo apt-get install -y libfltk1.3-dev $LIBJPG libxft-dev libxinerama-dev libxcursor-dev libsndfile1-dev libsamplerate0-dev portaudio19-dev libpulse-dev libusb-1.0-0-dev texinfo libudev-dev || { ERRCODE="Failed to get dependencies for FLDIGI."; errors; return; }

                  cd $USERDIR && wget -N https://www.w1hkj.org/files/fldigi/fldigi-${NEWFLDIGI}.tar.gz ||
                  { ERRCODE="Failed to download FLDIGI"; errors; return; }

                  tar xzvf fldigi-${NEWFLDIGI}.tar.gz && rm fldigi-${NEWFLDIGI}.\
tar.gz && cd fldigi-${NEWFLDIGI} ||
        		  { ERRCODE="Failed to extract FLDIGI"; errors; return; }

			 if [[ $(uname -m) = x86_64 ]]; then
            			./configure --enable-optimizations=native && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLDIGI"; errors; return; }
        			
              else
		        	  ./configure && make && sudo make \
install ||
{ ERRCODE="Failed to compile FLDIGI"; errors; return; }
	fi

#Cleanup space from compiling
#if hash fldigi 2>/dev/null; then
#  sudo rm -r $USERDIR/fldigi-${NEWFLDIGI}
#fi

fi
}


function flmsg_install() {

if [[ $NEWFLMSG == "SERVER DOWN" ]]; then
	echo "FLMSG server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLMSG} == ${NEWFLMSG} ]]; then
            echo "FLMSG is already at the most current version"

       else
              echo "Installing / Updating FLMSG to version ${NEWFLMSG}"
            	cd $USERDIR && wget -N https://www.w1hkj.org/files/flmsg/flmsg-${NEWFLMSG}.tar.gz ||
            		  { ERRCODE="Failed to download FLMSG"; errors; return; }
            	  tar xzvf flmsg-${NEWFLMSG}.tar.gz && rm flmsg-${NEWFLMSG}.tar.gz\
                 && cd flmsg-${NEWFLMSG} ||
            		  { ERRCODE="Failed to extract FLMSG"; errors; return; }

            	  if [[ $(uname -m) = x86_64 ]]; then
            			./configure --enable-optimizations=native && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLMSG"; errors; return; }
        			else
		        	  ./configure && make && sudo make \
install ||
   { ERRCODE="Failed to compile FLMSG"; errors; return; }
	fi
 
#Cleanup space from compiling
#if hash flmsg 2>/dev/null; then
#  sudo rm -r $USERDIR/flmsg-${NEWFLMSG}
#fi

 fi
    }

function flmsg_install_19() {

    if [[ ${FLMSG} == 4.0.19 ]]; then
            echo "FLMSG is already at 4.0.19"

       else
              echo "Installing / Updating FLMSG to version 4.0.19"
                cd $USERDIR && wget -N http://www.w1hkj.com/files/archives/Source/flmsg-4.0.19.tar.gz ||
                      { ERRCODE="Failed to download FLMSG"; errors; return; }
                  tar xzvf flmsg-4.0.19.tar.gz && rm flmsg-4.0.19.tar.gz\
                 && cd flmsg-4.0.19 ||
                      { ERRCODE="Failed to extract FLMSG"; errors; return; }

                  if [[ $(uname -m) = x86_64 ]]; then
                        ./configure --enable-optimizations=native && make && sudo make install ||
                    { ERRCODE="Failed to compile FLMSG"; errors; return; }
                    else
                      ./configure && make && sudo make \
install ||
   { ERRCODE="Failed to compile FLMSG"; errors; return; }
    fi
 
#Cleanup space from compiling
#if hash flmsg 2>/dev/null; then
#  sudo rm -r $USERDIR/flmsg-${NEWFLMSG}
#fi

 fi
    }

function flamp_install() {
if [[ $NEWFLAMP == "SERVER DOWN" ]]; then
	echo "FLAMP server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLAMP} == ${NEWFLAMP} ]]; then
            echo "FLAMP is already at the most current version"

         else
              echo "Installing / Updating FLAMP to version ${NEWFLAMP}"
                    sleep 2
                  cd $USERDIR && wget -N https://www.w1hkj.org/files/flamp/flamp-${NEWFLAMP}.tar.gz ||
                      { ERRCODE="Failed to download FLAMP"; errors; return; }
                  tar xzvf flamp-${NEWFLAMP}.tar.gz && \
                  rm flamp-${NEWFLAMP}.tar.gz && cd flamp-${NEWFLAMP} ||
                      { ERRCODE="Failed to extract FLAMP"; errors; return; }

                  if [[ $(uname -m) = x86_64 ]]; then
            			./configure --enable-optimizations=native && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLAMP"; errors; return; }
        			else
		        	  ./configure && make && sudo make \
install ||
 { ERRCODE="Failed to compile FLAMP"; errors; return; }
	fi

#Cleanup space from compiling
#if hash flamp 2>/dev/null; then
#  sudo rm -r $USERDIR/flamp-${NEWFLAMP}
#fi

fi
    }

function flamp_install_14() {
    if [[ ${FLAMP} == 2.2.14 ]]; then
            echo "FLAMP is already at the most current version"

         else
              echo "Installing / Updating FLAMP to version 2.2.14"
                    sleep 2
                  cd $USERDIR && wget -N http://www.w1hkj.com/files/flamp/flamp-2.2.14.tar.gz ||
                      { ERRCODE="Failed to download FLAMP 2.2.14"; errors; return; }
                  tar xzvf flamp-2.2.14.tar.gz && \
                  rm flamp-2.2.14.tar.gz && cd flamp-2.2.14 ||
                      { ERRCODE="Failed to extract FLAMP"; errors; return; }

                  if [[ $(uname -m) = x86_64 ]]; then
                        ./configure --enable-optimizations=native && make && sudo make install ||
                    { ERRCODE="Failed to compile FLAMP"; errors; return; }
                    else
                      ./configure && make && sudo make \
install ||
 { ERRCODE="Failed to compile FLAMP"; errors; return; }
    fi

#Cleanup space from compiling
#if hash flamp 2>/dev/null; then
#  sudo rm -r $USERDIR/flamp-${NEWFLAMP}
#fi

fi
    }


function flrig_install() {
if [[ $NEWFLRIG == "SERVER DOWN" ]]; then
	echo "FLRIG server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLRIG} == ${NEWFLRIG} ]]; then
            echo "FLRIG is already at the most current version"

         else
              echo "Installing / Updating FLRIG to version ${NEWFLRIG}"
                    sleep 2
                  cd $USERDIR && wget -N https://www.w1hkj.org/files/flrig/flrig-${NEWFLRIG}.tar.gz ||
                      { ERRCODE="Failed to download FLRIG"; errors; return; }
                  tar xzvf flrig-${NEWFLRIG}.tar.gz && rm flrig-${NEWFLRIG}\
.tar.gz && cd flrig-${NEWFLRIG} ||
                      { ERRCODE="Failed to extract FLRIG"; errors; return; }

                  if [[ $(uname -m) = x86_64 ]]; then
            			./configure --enable-optimizations=native && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLRIG"; errors; return; }
        			else
		        	  ./configure && make && sudo make \
install ||
{ ERRCODE="Failed to compile FLRIG"; errors; return; }
	fi

#Cleanup space from compiling
#if hash flrig 2>/dev/null; then
#  sudo rm -r $USERDIR/flrig-${NEWFLRIG}
#fi

fi
    }


function flwrap_install() {
if [[ $NEWFLWRAP == "SERVER DOWN" ]]; then
	echo "NEWFLWRAP server unresponsive or broken link." >> /tmp/failures
	return
fi
    if [[ ${FLWRAP} == ${NEWFLWRAP} ]]; then
            echo "FLWRAP is already at the most current version"

         else
            if [[ $OSCN == "vanessa" ]]; then
              sudo apt install -y flwrap ||
              { ERRCODE="Unable to install FLWRAP from repositories."; errors; return; }
            else
              echo "Installing / Updating FLWRAP to version ${NEWFLWRAP}"
                    sleep 2
                  cd $USERDIR && wget -N https://www.w1hkj.org/files/flwrap/flwrap-${NEWFLWRAP}.tar.gz ||
                      { ERRCODE="Failed to download FLWRAP"; errors; return; }
                  tar xzvf flwrap-${NEWFLWRAP}.tar.gz && rm flwrap-${NEWFLWRAP}\
.tar.gz && cd flwrap-${NEWFLWRAP} ||
                      { ERRCODE="Failed to extract FLWRAP"; errors; return; }

                  if [[ $(uname -m) = x86_64 ]]; then
            			./configure --enable-optimizations=native && make && sudo make install ||
         		  	{ ERRCODE="Failed to compile FLWRAP"; errors; return; }
        			else
		        	  ./configure CXXFLAGS="-std=c++14" && make && sudo make \
install ||
{ ERRCODE="Failed to compile FLWRAP"; errors; return; }
	fi

#Cleanup space from compiling
#if hash flwrap 2>/dev/null; then
#  sudo rm -r $USERDIR/flwrap-${NEWFLWRAP}
#fi
fi
fi
    }



function ardop_install() {

    ARDOPDIR=$USERDIR/ARDOP

    if [[ $(uname -m) =~ (armv7l|aarch64) ]]; then
      ARDOP=$"piardopc"

      if [[ -f $ARDOPDIR/$ARDOP ]]; then
        echo "Making backup of current version"
                    mv $ARDOPDIR/$ARDOP $ARDOPDIR/${ARDOP}-$(date +%h%d).bak ||
              { ERRCODE="Failed to create backup of current ardop file"; errors; return; }
      fi

      echo "Copying archived working $ARDOP version from "$DIR"/Files/piardopc."
                      #wget -P $ARDOPDIR -N http://www.cantab.net/users/john.\
#wiseman/Downloads/Beta/$ARDOP && chmod +x $ARDOPDIR/$ARDOP ||
      mkdir -p $ARDOPDIR && cp $DIR/Files/piardopc $ARDOPDIR/$ARDOP && chmod +x $ARDOPDIR/$ARDOP ||
                        { ERRCODE="Failed to download ardop"; errors; return; }


    else
      ARDOP=$"ardopc"


      if [[ -f $ARDOPDIR/$ARDOP ]]; then
        echo "Making backup of current version"
                    mv $ARDOPDIR/$ARDOP $ARDOPDIR/${ARDOP}-$(date +%h%d).bak ||
              { ERRCODE="Failed to create backup of current ardop file"; errors; return; }
      fi

      echo "Copying archived working $ARDOP version from "$DIR"/Files/piardopc."
                      #wget -P $ARDOPDIR -N http://www.cantab.net/users/john.\
#wiseman/Downloads/Beta/$ARDOP && chmod +x $ARDOPDIR/$ARDOP ||
      mkdir -p $ARDOPDIR && cp $DIR/Files/ardopc $ARDOPDIR/$ARDOP && chmod +x $ARDOPDIR/$ARDOP ||
                        { ERRCODE="Failed to download ardop"; errors; return; }
    fi

  if [[ ! -f $HOME/.asoundrc ]]; then
      echo "pcm.ARDOP {
      type rate
      slave {
      pcm "\""hw:1,0"\""
      rate 48000
      }
      }" > $HOME/.asoundrc ||
            { ERRCODE="Failed to create .asoundrc configuration file"; errors; return; }
  fi
    echo ardop >> /tmp/cleanup
  echo asoundrc >> /tmp/cleanup

   if ! hash ardop-status 2>/dev/null; then
    ardop_launcher
    fi


      }

function ardopcf_install() {

    ARDOPDIR=$USERDIR/ARDOP

    if [[ $(uname -m) =~ (armv7l|aarch64) ]]; then
      ARDOP=$"piardopc"

    else
      ARDOP=$"ardopc"


      if [[ -f $ARDOPDIR/$ARDOP ]]; then
        echo "Making backup of current version"
                    mv $ARDOPDIR/$ARDOP $ARDOPDIR/${ARDOP}-$(date +%h%d).bak ||
              { ERRCODE="Failed to create backup of current ardop file"; errors; return; }
      fi
        if [[ -f $ARDOPDIR/*.txt ]]; then
        rm $ARDOPDIR/*.txt ||
                { ERRCODE="Failed to remove previous date marker file"; errors; return; }
        fi
        mkdir -p $ARDOPDIR && mkdir $ARDOPDIR/build && cd $ARDOPDIR/build ||
                { ERRCODE="Failed to create ardopcf build directory"; errors; return; }
        git clone https://github.com/pflarue/ardop.git ||
                { ERRCODE="Failed to clone ardopcf repo."; errors; return; }
        cd $ARDOPDIR/build/ardop && make ||
                { ERRCODE="Failed to compile ardopcf"; errors; return; }
        cp $ARDOPDIR/build/ardop/ardopcf $ARDOPDIR/$ARDOP ||
                { ERRCODE="Failed to copy ardopcf to the ARDOP Directory"; errors; return; }
        rm -r $ARDOPDIR/build && touch $ARDOPDIR/v$NEWARDOPCF.txt ||
                { ERRCODE="Failed to remove build directory and create current date marker"; errors; return; }
    fi

  if [[ ! -f $HOME/.asoundrc ]]; then
      echo "pcm.ARDOP {
      type rate
      slave {
      pcm "\""hw:1,0"\""
      rate 48000
      }
      }" > $HOME/.asoundrc ||
            { ERRCODE="Failed to create .asoundrc configuration file"; errors; return; }
  fi
    echo ardopcf >> /tmp/cleanup
  echo asoundrc >> /tmp/cleanup

   if ! hash ardop-status 2>/dev/null; then
    ardop_launcher
    fi


      }

function ardop_gui_install() {

		ARDOPDIR=$USERDIR/ARDOP

		if [[ $(uname -m) =~ (armv7l|aarch64) ]]; then
			ARDOPGUI=$"piARDOP_GUI"

	  		if [[ -f $ARDOPDIR/$ARDOPGUI ]]; then
				echo "Making backup of current version"
              			mv $ARDOPDIR/$ARDOPGUI $ARDOPDIR/${ARDOPGUI}-$(date +%h%d).bak ||
     				  { ERRCODE="Failed to create backup of current ardop_gui file"; errors; return; }
                    	fi

			echo "Downloading current $ARDOPGUI version."
                    	wget -P $ARDOPDIR -N http://www.cantab.net/users/john.\
wiseman/Downloads/Beta/$ARDOPGUI && chmod +x $ARDOPDIR/$ARDOPGUI ||
                    		{ ERRCODE="Failed to download ardop_gui"; errors; return; }




else

		ARDOPGUI=$"ARDOP_GUI"

			if [[ -f $ARDOPDIR/$ARDOPGUI ]]; then
				echo "Making backup of current version"
              			mv $ARDOPDIR/$ARDOPGUI $ARDOPDIR/${ARDOPGUI}-$(date +%h%d).bak ||
     				  { ERRCODE="Failed to create backup of current ardop_gui file"; errors; return; }
                    	fi

               #Debian 10 version
               #sudo apt-get -y install g++ qt5-default ||
              #Debian 11 version
              sudo apt-get -y install g++ qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools libqt5widgets5:i386 ||
		{ ERRCODE="Failed to get dependencies for ARDOP_GUI"; errors; return; }

               wget -P $ARDOPDIR -N http://www.cantab.net/users/john.\
wiseman/Downloads/Beta/$ARDOPGUI && chmod +x $ARDOPDIR/$ARDOPGUI ||
                        { ERRCODE="Failed to download ardop_gui"; errors; return; }
		

fi





      }

function ardop_launcher() {

if [[ $(uname -m) =~ (armv7l|aarch64) ]]; then
	local var1="piardopc"
	local var13="piARDOP_GUI"
	else
	local var1="ardopc"
	local var13="ARDOP_GUI"
fi
local var2='$(pidof '"$var1"')'
local var3='$(pidof pat)'
local var14='$(pidof '"$var13"')'
local var4='$ardopstat'
local var5='$ardoppid'
local var6='$patstat'
local var7='$patpid'
local var15='$guistat'
local var16='$guipid'
local var8='$(cat /tmp/process-status | head -n 1 | awk -F "|" '"'"'{print $1}'"'"')'
local var9='$(cat /tmp/process-status | sed -n '"'"2p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var17='$(cat /tmp/process-status | sed -n '"'"3p"'"' | awk -F "|" '"'"'{print $1}'"'"')'
local var10='${ARDOPRESULTS}'
local var11='${PATRESULTS}'
local var18='${GUIRESULTS}'
local var12=$USERDIR



cat > $USERDIR/ardop-status <<EOF

#!/bin/bash



#create a finish trap
function finish {
  if [[ -f /tmp/process-status ]]; then
rm /tmp/process-status
fi

}
trap finish EXIT


while true
do



if pidof $var1 2>/dev/null; then
ardopstat="TRUE"
ardoppid=$var2
else
ardopstat="FALSE"
ardoppid="None"
fi

if pidof $var13 2>/dev/null; then
guistat="TRUE"
guipid=$var14
else
guistat="FALSE"
guipid="None"
fi

if pidof pat 2>/dev/null; then
patstat="TRUE"
patpid=$var3
else
patstat="FALSE"
patpid="None"
fi

if ! hash pat 2>/dev/null; then
patstat="NOT_INSTALLED"
patpid="NOT_INSTALLED"
fi

yad --center --checklist --list --print-all \
--title=Status \
--button=Close:1 --button=Toggle:0 \
--column=Active --column=Name --column=PID  \
$var4 ARDOPC $var5 \
$var6 PAT $var7 \
$var15 ARDOP_GUI $var16 \
--width=250 --height=200 >> /tmp/process-status || exit 0



ARDOPRESULTS=$var8
PATRESULTS=$var9
GUIRESULTS=$var17

if ( [[ $var4 == TRUE ]] && [[ $var10 == FALSE ]] ); then
kill $var5
fi

if ( [[ $var4 == FALSE ]] && [[ $var10 == TRUE ]] ); then
$var12/ARDOP/$var1 &
fi

if ( [[ $var6 == TRUE ]] && [[ $var11 == FALSE ]] ); then
kill $var7
fi

if ( [[ $var6 == FALSE ]] && [[ $var11 == TRUE ]] ); then
pat http &
fi

if ( [[ $var15 == TRUE ]] && [[ $var18 == FALSE ]] ); then
kill $var16
fi

if ( [[ $var15 == FALSE ]] && [[ $var18 == TRUE ]] ); then
$var12/ARDOP/$var13 &
fi


finish
sleep 2

done

EOF

chmod +x $USERDIR/ardop-status
sudo mv $USERDIR/ardop-status /usr/local/bin/ardop-status

}


function arim_install() {
if [[ $NEWARIM == "SERVER DOWN" ]]; then
	echo "ARIM server unresponsive or broken link." >> /tmp/failures
	return
fi
if [[ ${ARIM} == ${NEWARIM} ]]; then
            echo "ARIM is already at the most current version"

         else
              echo "Installing / Updating ARIM to version ${NEWARIM}"
			if [[ ${ARIM} == "NOT INSTALLED" ]]; then
			sudo apt-get -y install build-essential ncurses-dev zlib1g-dev ||
			{ ERRCODE="Failed to get dependencies for ARIM"; errors; return; }
			fi
		cd $USERDIR && wget -N https://www.whitemesa.net/arim/src/\
arim-${NEWARIM}.tar.gz ||
	  	{ ERRCODE="Failed to download ARIM"; errors; return; }

		tar xzvf arim-${NEWARIM}.tar.gz && rm arim-${NEWARIM}.tar.gz && \
cd arim-${NEWARIM} ||
 	 	{ ERRCODE="Failed to extract ARIM"; errors; return; }

		./configure && make && sudo make install ||
  		{ ERRCODE="Failed to compile ARIM"; errors; return; }

      #Cleanup space from compiling
      #if hash arim 2>/dev/null; then
      #  sudo rm -r $USERDIR/arim-${NEWARIM}
      #fi
	fi
   }


function garim_install() {
if [[ $NEWGARIM == "SERVER DOWN" ]]; then
	echo "GARIM server unresponsive or broken link." >> /tmp/failures
	return
fi

if [[ ${GARIM} == ${NEWGARIM} ]]; then
            echo "GARIM is already at the most current version"

         else
              echo "Installing / Updating GARIM to version ${NEWGARIM}"
			if [[ ${GARIM} == "NOT INSTALLED" ]]; then
			sudo apt-get -y install build-essential libfltk1.3-dev zlib1g-dev ||
			{ ERRCODE="Failed to get dependencies for GARIM"; errors; return; }
			fi
		cd $USERDIR && wget -N https://www.whitemesa.net/garim/src/\
garim-${NEWGARIM}.tar.gz ||
	  	{ ERRCODE="Failed to download GARIM"; errors; return; }

		tar xzvf garim-${NEWGARIM}.tar.gz && rm garim-${NEWGARIM}.tar.gz && \
cd garim-${NEWGARIM} ||
 	 	{ ERRCODE="Failed to extract GARIM"; errors; return; }

		./configure && make && sudo make install ||
  		{ ERRCODE="Failed to compile GARIM"; errors; return; }
	
  fi
   }




function varim_install() {
if [[ $NEWVARIM == "SERVER DOWN" ]]; then
  echo "VARIM server unresponsive or broken link." >> /tmp/failures
  return
fi

if [[ ${VARIM} == ${NEWVARIM} ]]; then
            echo "VARIM is already at the most current version"

         else
              echo "Installing / Updating VARIM to version ${NEWVARIM}"
      if [[ ${VARIM} == "NOT INSTALLED" ]]; then
      sudo apt-get -y install build-essential libfltk1.3-dev zlib1g-dev ||
      { ERRCODE="Failed to get dependencies for VARIM"; errors; return; }
      fi
    cd $USERDIR && wget -N https://www.whitemesa.net/varim/src/\
varim-${NEWVARIM}.tar.gz ||
      { ERRCODE="Failed to download VARIM"; errors; return; }

    tar xzvf varim-${NEWVARIM}.tar.gz && rm varim-${NEWVARIM}.tar.gz && \
cd varim-${NEWVARIM} ||
    { ERRCODE="Failed to extract VARIM"; errors; return; }

    ./configure && make && sudo make install ||
      { ERRCODE="Failed to compile VARIM"; errors; return; }
  
  fi
   }



function pat_install() {
if [[ $NEWPAT == "SERVER DOWN" ]]; then
	echo "PAT server unresponsive or broken link." >> /tmp/failures
	return
fi
		if [[ $(uname -m) = armv7l ]]; then
    SYS=$"armhf"

    elif [[ $(uname -m) = aarch64 ]]; then
    SYS=$"arm64"

    elif [[ $(uname -m) = x86_64 ]]; then
    SYS=$"amd64"

    else
    SYS=$"i386"
    fi


		if [[ ${PAT} == ${NEWPAT} ]]; then
           		echo "PAT is already at the most current version"

        	 else
			cd $USERDIR && wget -N https://github.com/la5nta/pat/releases/download\
/v$NEWPAT/pat_"$NEWPAT"_linux_$SYS.deb ||
			{ ERRCODE="Failed to download PAT"; errors; return; }

			sudo dpkg -i pat_"$NEWPAT"_linux_$SYS.deb ||
			{ ERRCODE="Failed to install PAT"; errors; return; }

			rm pat_"$NEWPAT"_linux_$SYS.deb ||
			{ ERRCODE="Failed to remove PAT deb file."; errors; return; }
		fi
}

function getardoptools_install() {
  # Get Grid Square Map
  if ! hash evince; then
  sudo apt-get install -y evince
fi
  cp $HOME/setup-scripts/Files/2013_GridSquareMap.pdf $HOME/Documents/grid-map.pdf ||
  { ERRCODE="Failed to copy grid-map.pdf to Documents folder for findardop / findvara"; errors; return; }
  # Get ardop list script

if ! hash getardoplist; then
  wget -P $USERDIR -N https://gist.githubusercontent.com/km4ack/2b5db90724cab67\
ed50cbf48ed223efd/raw/efe402285290b46c700d9d29e285c0686b79314f/getardoplist ||
{ ERRCODE="Failed to download getardoplist script"; errors; return; }

  chmod +x $USERDIR/getardoplist && sudo mv $USERDIR/getardoplist /usr/local/bin/ ||
{ ERRCODE="Failed to add getardoplist to /usr/local/bin"; errors; return; }

fi
  # Get find ardop script
if ! hash findardop; then
  wget -P $USERDIR -N https://gist.githubusercontent.com/km4ack/7af44d08d95215d\
a45be3e69decf6def/raw/1aeff0d78f85e86b868a4ba83ebce99e90532f48/findardop ||
{ ERRCODE="Failed to download findardop script"; errors; return; }

  chmod +x $USERDIR/findardop && sudo mv $USERDIR/findardop /usr/local/bin/ ||
{ ERRCODE="Failed to add findardop to /usr/local/bin"; errors; return; }

fi

if ! hash getvaralist; then
  wget -P $USERDIR -N https://gist.githubusercontent.com/km4ack/2b5db90724cab67\
ed50cbf48ed223efd/raw/efe402285290b46c700d9d29e285c0686b79314f/getardoplist ||
{ ERRCODE="Failed to download getardoplist script"; errors; return; }

  sed 's/ardop/vara/g' $USERDIR/getardoplist >> $USERDIR/getvaralist ||
{ ERRCODE="Failed to clone and modify getardop to gitvara"; errors; return; }

  chmod +x $USERDIR/getvaralist && sudo mv $USERDIR/getvaralist /usr/local/bin/ ||
{ ERRCODE="Failed to add getvaralist to /usr/local/bin"; errors; return; }
fi
  # Get find ardop script
if ! hash findvara; then
  wget -P $USERDIR -N https://gist.githubusercontent.com/km4ack/7af44d08d95215d\
a45be3e69decf6def/raw/1aeff0d78f85e86b868a4ba83ebce99e90532f48/findardop ||
{ ERRCODE="Failed to download findardop script"; errors; return; }

    sed 's/ardop/vara/g' $USERDIR/findardop >> $USERDIR/findvara ||
{ ERRCODE="Failed to clone and modify getardop to gitvara"; errors; return; }

    chmod +x $USERDIR/findvara && sudo mv $USERDIR/findvara /usr/local/bin/ ||
{ ERRCODE="Failed to add findvara to /usr/local/bin"; errors; return; }

fi


  echo ardoptools >> /tmp/cleanup

}


function pulse_audio_suite_install() {

sudo apt-get -y install pulseaudio pavucontrol pulseaudio-utils ||
{ ERRCODE="Failed to install pulseaudio suite"; errors; return; }

}

function pipewire() {

if [[ $OSCN =~ wilma|xia ]]; then

    sudo apt purge -y pipewire pipewire-bin ||
    { ERRCODE="Failed to purge pipewire"; errors; return; }

    systemctl enable --user pulseaudio ||
    { ERRCODE="Failed to enable legacy pulseaudio support"; errors; return; }
fi

}

function voacap_install_old() {

  if apt-cache show gnome-doc-utils &>/dev/null; then

  sudo apt-get -y install gfortran ||
                { ERRCODE="Failed to get gfortran dependency for VOACAPL"; errors; return; }

	cd $USERDIR && wget -N https://github.com/jawatson/voacapl/archive/refs/tags/v.0.7.6.tar.gz ||
                { ERRCODE="Failed to download VOACAPL"; errors; return; }

	tar -xzf voacapl-0.7.6.tar.gz && rm voacapl-0.7.6.tar.gz && cd voacapl-0.7.6 ||
                { ERRCODE="Failed to extract VOACAPL"; errors; return; }


	./configure && make -j4 && sudo make install ||
                { ERRCODE="Failed to compile VOACAPL"; errors; return; }


	makeitshfbc ||
                { ERRCODE="VOACAPL makeitshfbc failed"; errors; return; }

	sudo apt-get -y install yelp python3-gi python3-gi-cairo rarian-compat gnome-doc-utils \
pkg-config python3-dateutil python3-cairocffi libgtk-3-dev gettext ||
                { ERRCODE="Failed to get dependencies for VOACAPL pythonprop GUI"; errors; return; }

	cd $USERDIR && wget -N www.qsl.net/hz1jw/pythonprop/downloads/pythonprop-0.28.tar.gz ||
                { ERRCODE="Failed to download VOACAPL pythonprop GUI"; errors; return; }


	tar -xzf pythonprop-0.28.tar.gz && rm pythonprop-0.28.tar.gz && cd pythonprop-0.28 ||
		{ ERRCODE="Failed to extract VOACAPL pythonprop GUI"; errors; return; }


	./configure && sudo make install ||
		{ ERRCODE="Failed to compile VOACAPL pythonprop GUI"; errors; return; }

else

      { echo voacap_os >> /tmp/cleanup; } && { ERRCODE="VOACAP GUI is not supported on your OS."; errors; return; }
  

fi
}




function voacap_install() {

  if [[ $NEWVOACAPL == "SERVER DOWN" ]]; then
    echo "VOACAPL server unresponsive or broken link." >> /tmp/failures
    return
fi
    if [[ ${VOACAPLVER} == ${NEWVOACAPL} ]]; then
            echo "VOACAPL is already at the most current version"

         else
              echo "Installing / Updating VOACAPL to version ${NEWVOACAPL}"
              NEWVOACAPLFILE=v.${NEWVOACAPL}.tar.gz

  sudo apt-get install -y gfortran yelp python3-gi python3-gi-cairo pkg-config python3-dateutil python3-cairocffi libgtk-3-dev ||
                { ERRCODE="Failed to get dependencies for VOACAPL"; errors; return; }

    cd $USERDIR && wget -N https://github.com/jawatson/voacapl/archive/refs/tags/${NEWVOACAPLFILE} ||
                { ERRCODE="Failed to download VOACAPL"; errors; return; }

    tar -xzf ${NEWVOACAPLFILE} && rm ${NEWVOACAPLFILE} && cd voacapl-v.${NEWVOACAPL} ||
                { ERRCODE="Failed to extract VOACAPL"; errors; return; }


    ./configure && make -j 1 && sudo make install ||
                { ERRCODE="Failed to compile VOACAPL"; errors; return; }


    makeitshfbc ||
                { ERRCODE="VOACAPL makeitshfbc failed"; errors; return; }

    fi


    sudo apt-get -y install build-essential yelp-tools python3-matplotlib python3-cartopy python3-scipy autotools-dev automake ||
                { ERRCODE="Failed to get dependencies for VOACAPL pythonprop GUI"; errors; return; }

    cd $USERDIR && wget -N https://github.com/jawatson/pythonprop/archive/refs/tags/v0.30.1.tar.gz ||
                { ERRCODE="Failed to download VOACAPL pythonprop GUI"; errors; return; }


    tar -xzf v0.30.1.tar.gz && rm v0.30.1.tar.gz && cd pythonprop-0.30.1 ||
        { ERRCODE="Failed to extract VOACAPL pythonprop GUI"; errors; return; }


    ./autogen.sh && ./configure && sudo make install ||
        { ERRCODE="Failed to compile VOACAPL pythonprop GUI"; errors; return; }

    cp $USERDIR/pythonprop-0.30.1/data/voacapgui.desktop ~/Desktop/ ||
        { ERRCODE="Failed to copy VOACAPL pythonprop GUI desktop shortcut"; errors; return; }


}



function chirp_install_old2() {
if [[ $NEWCHIRP == "SERVER DOWN" ]]; then
  echo "CHIRP server unresponsive or broken link." >> /tmp/failures
  return
fi

if [[ ${CHIRP} == ${NEWCHIRP} ]]; then
            echo "Chirp is already at the most current version"


         else
          if [ $PLAT = Linuxmint ]; then
             echo "Installing / Updating CHIRP to version ${NEWCHIRP}"
                    sleep 2
              if ! hash flatpak 2>/dev/null; then
              sudo apt-get install -y flatpak ||
              { ERRCODE="Failed to install flatpak."; errors; return; }
              fi
          
          sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo ||
          { ERRCODE="Failed to add flathub remote."; errors; return; }
          
          cd $USERDIR && wget -N https://trac.chirp.danplanet.com/chirp_daily/LATEST/chirp-daily-$NEWCHIRP.flatpak ||
          { ERRCODE="Failed to download new version of chirp."; errors; return; }
          
          sudo flatpak install -y --noninteractive chirp-daily-$NEWCHIRP.flatpak ||
          { ERRCODE="Failed to install chirp flatpak."; errors; return; }
        
          else
          echo snap_fail_chirp >> /tmp/cleanup

          sudo apt-get install -y snapd ||
            { ERRCODE="Failed to install Snap for Chirp installation"; errors; return; }
          ### Testing to see if this avoids a restart requirement 
          sudo modprobe squashfs ||
            { ERRCODE="Failed to install Chirp from snap store."; errors; return; }

          sudo snap install chirp-snap --edge ||
            { ERRCODE="Failed to install Chirp from snap store."; errors; return; }
          
          sudo snap connect chirp-snap:raw-usb ||
            { ERRCODE="Failed to set USB permissions for Chirp-snap."; errors; return; }
          fi

fi
}

function chirp_install() {
if [[ $NEWCHIRP == "SERVER DOWN" ]]; then
  echo "CHIRP server unresponsive or broken link." >> /tmp/failures
  return
fi

if [[ ${CHIRP} == ${NEWCHIRP} ]]; then
            echo "Chirp is already at the most current version"


         else
          cd $USERDIR && wget https://trac.chirp.danplanet.com/chirp_next/next-$NEWCHIRP/chirp-$NEWCHIRP.tar.gz ||
        { ERRCODE="Failed to download Chirp-Next"; errors; return; }

        tar -xvf chirp-$NEWCHIRP.tar.gz && cd chirp-$NEWCHIRP/ ||
        { ERRCODE="Failed to extract Chirp-Next"; errors; return; }

        sudo apt install -y git python3-wxgtk4.0 python3-serial python3-six python3-future python3-requests python3-pip pipx python3-yattag ||
        

        { ERRCODE="Failed to install python dependencies for Chirp-Next"; errors; return; }

        sudo python3 setup.py install ||
        { ERRCODE="Failed to install Chirp-Next"; errors; return; }
        fi
        
}


function wsjtx_install() {

  if [[ $NEWWSJTXVER == "SERVER DOWN" ]]; then
  echo "WSJTX server unresponsive or broken link." >> /tmp/failures
  return
  fi

if [[ ${WSJTXVER} == ${NEWWSJTX} ]]; then
              echo "WSJTX is already at the most current version"

           else

      #install from source 
        sudo apt install -y qtmultimedia5-dev libqt5serialport5-dev qttools5-dev qttools5-dev-tools libqt5multimedia5-plugins libboost-all-dev libfftw3-dev libreadline-dev libusb-1.0-0-dev libudev-dev portaudio19-dev cmake libgfortran5
        


WSJTXPKG=wsjtx-${NEWWSJTX}.tgz
WSJTXBASEDIRVER=$(echo ${NEWWSJTX} | awk -F "-" '{print $1}')
WSJTXDIR=wsjtx-${WSJTXBASEDIRVER}
### Testing
echo $WSJTXPKG
echo $WSJTXBASEDIRVER
echo $WSJTXDIR
###

#  wget -P $USERDIR/ -N https://physics.princeton.edu/pulsar/k1jt/$WSJTXPKG ||

#  cd $USERDIR && curl -sL https://sourceforge.net/projects/$WSJTXDIR/$WSJTXPKG/download ||
#       { ERRCODE="Failed to download WSJTX"; errors; return; }

 cd $USERDIR && wget --tries 2 --connect-timeout=60 https://sourceforge.net/projects/wsjt/files/$WSJTXPKG ||
        { ERRCODE="Failed to download WSJTX"; errors; return; }

  cd $USERDIR && tar -xvf $USERDIR/$WSJTXPKG && rm $USERDIR/$WSJTXPKG ||
  { ERRCODE="Unable to extract WSJTX Source Code"; errors; return; }

  cd $USERDIR/$WSJTXDIR

  cmake . && cmake -DWSJT_SKIP_MANPAGES=ON -DWSJT_GENERATE_DOCS=OFF && cmake --build . -- -j4 && sudo make install ||
   { ERRCODE="Failed to build WSJTX."; errors; return; }



fi
}


function wsjtx_stable_install() {

  if [[ $NEWWSJTX_STABLE == "SERVER DOWN" ]]; then
  echo "WSJTX server unresponsive or broken link." >> /tmp/failures
  return
  fi

if [[ ${WSJTXVER} == ${NEWWSJTX_STABLE} ]]; then
              echo "WSJTX is already at the most current GA version"

           else

      #install from source 
        sudo apt install -y qtmultimedia5-dev libqt5serialport5-dev qttools5-dev qttools5-dev-tools libqt5multimedia5-plugins libboost-all-dev libfftw3-dev libreadline-dev libusb-1.0-0-dev libudev-dev portaudio19-dev cmake libgfortran5
        


WSJTXPKG=wsjtx-${NEWWSJTX_STABLE}.tgz
WSJTXBASEDIRVER=$(echo ${NEWWSJTX_STABLE} | awk -F "-" '{print $1}')
WSJTXDIR=wsjtx-${WSJTXBASEDIRVER}
### Testing
echo $WSJTXPKG
echo $WSJTXBASEDIRVER
echo $WSJTXDIR
###

#  wget -P $USERDIR/ -N https://physics.princeton.edu/pulsar/k1jt/$WSJTXPKG ||

#  cd $USERDIR && curl -sL https://sourceforge.net/projects/$WSJTXDIR/$WSJTXPKG/download ||
#       { ERRCODE="Failed to download WSJTX"; errors; return; }

 cd $USERDIR && wget --tries 2 --connect-timeout=60 https://sourceforge.net/projects/wsjt/files/$WSJTXPKG ||
        { ERRCODE="Failed to download WSJTX"; errors; return; }

  cd $USERDIR && tar -xvf $USERDIR/$WSJTXPKG && rm $USERDIR/$WSJTXPKG ||
  { ERRCODE="Unable to extract WSJTX Source Code"; errors; return; }

  cd $USERDIR/$WSJTXDIR

  cmake . && cmake -DWSJT_SKIP_MANPAGES=ON -DWSJT_GENERATE_DOCS=OFF && cmake --build . -- -j4 && sudo make install ||
   { ERRCODE="Failed to build WSJTX."; errors; return; }



fi
}


function amrronforms_install() {
     FORMSDIR=$HOME/.nbems/CUSTOM
     if [ ! -d $HOME/.nbems/CUSTOM ]; then
   mkdir -p $HOME/.nbems/CUSTOM/ ||
     { ERRCODE="Failed to create directory at $HOME/.nbems/CUSTOM/"; errors; return; }
      fi

     if ls $FORMSDIR/amrron_statrep_V5.1.html 1>/dev/null 2>&1; then
     echo "V5.1 of Forms are already installed"
        else
         cd $FORMSDIR && wget -N https://amrron.com/wp-content/uploads/2023/02/V5.0-Forms_27Nov2024.zip ||
       { ERRCODE="Failed to download AmRRON Custom Forms V5.0 27NOV24 update with STATREP 5.1"; errors; return; }
        fi

      if ls $FORMSDIR/*zip 1>/dev/null 2>&1; then
        echo "Extracting forms..."
           unzip -oj $FORMSDIR/'*'.zip -d $FORMSDIR/ && rm $FORMSDIR/*.zip ||
       { ERRCODE="Failed to move forms to $HOME/.nbems/CUSTOM/"; errors; return; }
            else
                echo "All custom forms are already installed."
            fi
 }


function comstat_install() {

if [[ $OSCN =~ una|vanessa|vera|virginia|wilma|xia ]]; then

cd $USERDIR && git clone https://github.com/W5DMH/commstatone.git ||
{ ERRCODE="Failed to download comstat archive"; errors; return; }

cd $USERDIR/commstatone && chmod +x linuxinstall.sh ||
{ ERRCODE="Failed to make linuxinstall.sh executable."; errors; return; }

echo comstatone >> /tmp/cleanup
else
  echo "Comstat install has not been tested on your distribution."

fi

if [[ $OSCN =~ wilma|xia ]]; then

cd ~/Radio/commstatone && sudo apt install -y python3.12-venv python3-pyqt5.qtwebengine ||
{ ERRCODE="Failed to install python3.12-venv virtual environment dependencies"; errors; return; }

sed -i -e '110d' install.py ||
{ ERRCODE="Failed to remove Start Settings line from install.py IOT allow for automated install."; errors; return; }

python3 -m venv venv ||
{ ERRCODE="Failed to create python virtual environment"; errors; return; }

source venv/bin/activate ||
{ ERRCODE="Failed to activate python virtual environment"; errors; return; }

pip3 install pyqt5 psutil||
{ ERRCODE="Failed to download pyqt5 and psutil dependency"; errors; return; }

pip3 install pyqtwebengine ||
{ ERRCODE="Failed to download pyqtwebengine dependency"; errors; return; }

python3 install.py ||
{ ERRCODE="Failed to run linux install"; errors; return; }

deactivate ||
{ ERRCODE="Failed to deactivate python virtual environment"; errors; return; }

cp $HOME/setup-scripts/Files/traffic.db3 $USERDIR/commstatone/traffic.db3 ||
{ ERRCODE="Failed to replace commstat database with cleaned version"; errors; return; }
fi


if [[ -f $USERDIR/commstatone/commstat.py ]]; then

   if [[ $OSCN =~ wilma|xia ]]; then
echo "[Desktop Entry]
Name=CommstatOne
GenericName=CommstatOne
Comment=Commstat JS8 SA Tool
Path=$USERDIR/commstatone/
Exec=$USERDIR/commstatone/venv/bin/python3 commstat.py
Icon=$USERDIR/icons/commstat.jpg
Terminal=false
Type=Application
Categories=Radio;" > $HOME/Desktop/commstat.desktop ||
{ ERRCODE="Failed to create desktop icon for CommstatOne"; errors; }

    else
echo "[Desktop Entry]
Name=CommstatOne
GenericName=CommstatOne
Comment=Commstat JS8 SA Tool
Path=$USERDIR/commstatone/
Exec=python3 commstat.py
Icon=$USERDIR/icons/commstat.jpg
Terminal=false
Type=Application
Categories=Radio;" > $HOME/Desktop/commstat.desktop ||
{ ERRCODE="Failed to create desktop icon for CommstatOne"; errors; }

 fi

chmod +x $HOME/Desktop/commstat.desktop ||
{ ERRCODE="Failed to make desktop icon for CommstatOne executable"; errors; }

if [[ ! -f $USERDIR/icons/commstat.jpg ]]; then
    mkdir -p $USERDIR/icons && wget --no-check-certificate -O $USERDIR/icons/commstat.jpg -N https://amrron.com/wp-content/uploads/2023/02/JS8Call_Commstat_Data.jpg ||
    { ERRCODE="Failed to icon image for commstat"; errors; }
    fi

fi

}


function js8call_install() {

if [[ $NEWJS8CALLVER == "SERVER DOWN" ]]; then
  echo "JS8Call server unresponsive or broken link." >> /tmp/failures
  return
fi

if [[ ${JS8CALLVER} == ${NEWJS8CALLVER} ]]; then
            echo "JS8Call is already at the most current version"

        elif [[ ${JS8CALLVER} == "2.2.1-devel" ]] && [[ ${NEWJS8CALLVER} == "2.2.0" ]]; then
            echo "JS8Call is already at the most current version"

         else

  sudo apt-get install -y build-essential gfortran autoconf automake libtool cmake git asciidoctor libfftw3-dev qtdeclarative5-dev texinfo libqt5multimedia5 libqt5multimedia5-plugins qtmultimedia5-dev libusb-1.0.0-dev libqt5serialport5-dev asciidoc libudev-dev

  JS8PKG=$(curl -s http://files.js8call.com/latest.html | grep .tgz | sed 's/.*js8call-/js8call-/;s/<.*$//')
  JS8VERSION=$(echo $JS8PKG | sed 's/js8call-//;s/.tgz.*$//')


  wget -P $USERDIR/ -N http://files.js8call.com/$JS8VERSION/$JS8PKG ||
        { ERRCODE="Failed to download JS8Call"; errors; return; }

  cd $USERDIR && tar -xvf $USERDIR/$JS8PKG && rm $USERDIR/$JS8PKG ||
  { ERRCODE="Unable to extract JS8call Source Code"; errors; return; }

  cd $USERDIR/js8call

  cmake . && cmake --build . -- -j4 && sudo make install ||
   { ERRCODE="Failed to build JS8Call."; errors; return; }


#Cleanup space from compiling
#if hash js8call 2>/dev/null; then
#  sudo rm -r $USERDIR/js8call
#fi

fi
}

function ublox_gps_setup() {

#Deps from Debian 10
#sudo apt-get -y install gpsd gpsd-clients python-gps chrony ||

#if apt-cache show gpsd-tools &>/dev/null; then
#  GPSDEP=gpsd-tools
#else
#  GPSDEP=python-gps
#fi

#Bullseye and Mint 21
if [[ $OSCN =~ (bullseye|vanessa|vera|wilma|xia) ]]; then
sudo apt install -y gpsd gpsd-clients chrony gpsd-tools ||
       { ERRCODE="Failed to install gpsd related packages"; errors; return; }

#Buster
elif [[ $OSCN = buster ]]; then
sudo apt install -y gpsd gpsd-clients chrony python-gps ||
       { ERRCODE="Failed to install gpsd related packages"; errors; return; }

#Mint 20.3
elif [[ $OSCN = una ]]; then 
sudo apt install -y gpsd gpsd-clients chrony ||
       { ERRCODE="Failed to install gpsd related packages"; errors; return; }
fi

if [[ -f /etc/default/gpsd ]]; then

local var1=$(cat /etc/default/gpsd | grep START_DAEMON= | awk -F "=" '{print $2}' | sed s/\"//g)

local var2=$(cat /etc/default/gpsd | grep USBAUTO= | awk -F "=" '{print $2}' | sed s/\"//g)

local var3=$(cat /etc/default/gpsd | grep DEVICES= | awk -F "=" '{print $2}' | sed s/\"//g)

local var4=$(cat /etc/default/gpsd | grep GPSD_OPTIONS= | awk -F "=" '{print $2}' | sed s/\"//g)

if [[ $var1 != "true" ]]; then

sudo sed -i s/START_DAEMON=\"$var1\"/START_DAEMON=\"true\"/g /etc/default/gpsd ||
       { ERRCODE="Failed to set START_DAEMON to true in /etc/default/gpsd"; errors; return; }

fi


if [[ $var2 != "false" ]]; then

sudo sed -i s/USBAUTO=\"$var2\"/USBAUTO=\"false\"/g /etc/default/gpsd ||
       { ERRCODE="Failed to set USBAUTO to false in /etc/default/gpsd"; errors; return; }

fi

if [[ $var3 != "/dev/ttyACM0" ]]; then

sudo sed -i s@DEVICES=\"$var3\"@DEVICES=\"/dev/ttyACM0\"@g /etc/default/gpsd ||
       { ERRCODE="Failed to set DEVICES to /dev/ttyACM0 in /etc/default/gpsd"; errors; return; }

fi

if [[ $var4 != "-n" ]]; then

sudo sed -i s/GPSD_OPTIONS=\"$var4\"/GPSD_OPTIONS=\"-n\"/g /etc/default/gpsd ||
       { ERRCODE="Failed to set GPSD_OPTIONS to -n in /etc/default/gpsd"; errors; return; }

fi

fi

if [[ -f /etc/chrony/chrony.conf ]]; then

if grep -q "refclock SHM 0 offset 0.0 delay 0.2 refid NMEA" /etc/chrony/chrony.conf; then
	echo "Parameters already set"
	else
	echo 'refclock SHM 0 offset 0.0 delay 0.2 refid NMEA' | sudo tee -a /etc/chrony/chrony.conf ||
       { ERRCODE="Failed add: refclock SHM 0 offset 0.0 delay 0.2 refid NMEA to /etc/chrony/chrony.conf"; errors; return; }
fi
fi

}

function create_icons() {

function assign_directories() {

UNSETDIRS=$(yad --image=$DIR/images/corps-icon.png --center --form --separator="|" \
--title "Set Installed Directories" \
--field="Tip:TXT" "One or more of the Directory Variables have not been set.  Likely due to running install icons by itself.

Please inform the installer of the following Directories so it can make the requested desktop icons.

The default locations are prefilled.

If you did not install any programs for the associated section, you can leave the selection at default." \
--field="Radio Directory:MDIR" "$HOME/Radio" \
--field="Packet Directory:MDIR" "$HOME/Radio/Packet" \
--field="Security Directory:MDIR" "$HOME/Security" \
${r} ${c})

USERDIR=$(echo $UNSETDIRS | awk -F "|" '{print $2}')
PACKETDIR=$(echo $UNSETDIRS | awk -F "|" '{print $3}')
SECDIR=$(echo $UNSETDIRS | awk -F "|" '{print $4}')

}


if ( [[ $USERDIR == "" ]] || [[ $SECDIR == "" ]] || [[ $PACKETDIR == "" ]] ); then
assign_directories
fi



if hash fldigi 2>/dev/null; then
    echo "[Desktop Entry]
    Name=Fldigi
    GenericName=Amateur Radio Digital Modem
    Comment=Amateur Radio Sound Card Communications
    Exec=fldigi
    Icon=fldigi
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/fldigi.desktop ||
       { ERRCODE="Failed to create desktop icon for FLDIGI"; errors; }
fi

if hash flmsg 2>/dev/null; then
       echo "[Desktop Entry]
       Name=Flmsg
       GenericName=Amateur Radio Digital Modem
       Comment=Amateur Radio Sound Card Communications
       Exec=flmsg
       Icon=flmsg
       Terminal=false
       Type=Application
       Categories=Network;HamRadio;" > $HOME/Desktop/flmsg.desktop ||
          { ERRCODE="Failed to create desktop icon for FLMSG"; errors; }
fi

if hash flamp 2>/dev/null; then
          echo "[Desktop Entry]
          Name=Flamp
          GenericName=Amateur Radio Digital Modem
          Comment=Amateur Radio Sound Card Communications
          Exec=flamp
          Icon=flamp
          Terminal=false
          Type=Application
          Categories=Network;HamRadio;" > $HOME/Desktop/flamp.desktop ||
            { ERRCODE="Failed to create desktop icon for FLAMP"; errors; }
fi

if hash flrig 2>/dev/null; then
          echo "[Desktop Entry]
          Name=Flrig
          GenericName=Amateur Radio Digital Modem
          Comment=Amateur Radio Sound Card Communications
          Exec=flrig
          Icon=flrig
          Terminal=false
          Type=Application
          Categories=Network;HamRadio;" > $HOME/Desktop/flrig.desktop ||
            { ERRCODE="Failed to create desktop icon for FLRIG"; errors; }
fi

if hash flwrap 2>/dev/null; then
          echo "[Desktop Entry]
          Name=FLWRAP
          GenericName=Amateur Radio Digital Modem
          Comment=Amateur Radio Sound Card Communications
          Exec=flwrap
          Icon=flwrap
          Terminal=false
          Type=Application
          Categories=Network;HamRadio;" > $HOME/Desktop/flwrap.desktop ||
            { ERRCODE="Failed to create desktop icon for flwrap"; errors; }
fi

if hash voacapgui 2>/dev/null; then

echo "[Desktop Entry]
Name=VOACAPGUI
GenericName=VOACAP Graphical User Interface
Comment=VOACAP Propagation Tool
Exec=voacapgui
Icon=$USERDIR/icons/voacaplIcon.png
Terminal=false
Type=Application
Categories=Network;HamRadio;" > $HOME/Desktop/voacapgui.desktop ||
{ ERRCODE="Failed to create desktop icon for VOACAP GUI"; errors; }

	if [[ ! -f $USERDIR/icons/voacaplIcon.png ]]; then
	mkdir -p $USERDIR/icons && \
wget -P $USERDIR/icons/ -N https://www.qsl.net/hz1jw/voacapl/downloads/voacaplIcon.png ||
	{ ERRCODE="Failed to icon image for VOACAPGUI"; errors; }
	fi
fi

if hash ardop-status 2>/dev/null; then

echo "[Desktop Entry]
Name=Status
GenericName=ARDOP-Status
Comment=Ardop & PAT status tool
Exec=bash -c "ardop-status"
Icon=$USERDIR/icons/tnc_icon.png
Terminal=false
Type=Application
Categories=Network;HamRadio;" > $HOME/Desktop/ardop-status.desktop ||
{ ERRCODE="Failed to create desktop icon for ardop-status tool"; errors; }

	if [[ ! -f $USERDIR/icons/tnc_icon.png ]]; then
	mkdir -p $USERDIR/icons && \
wget -P $USERDIR/icons/ -N http://www.gencat.cat/llengua/appdelasetmana/img_app/2016_03_24_TNC/tnc_icon.png ||
	{ ERRCODE="Failed to icon image for ardop-status tool"; errors; }
	fi
fi


if [[ -f $SECDIR/Paranoia\ Text\ Encryption/pte.jar ]]; then

echo "[Desktop Entry]
Name=PTE
GenericName=Paranoia Text Encrypter
Comment=Text encrypter
Exec=pte
Icon=$USERDIR/icons/pte_icon.png
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/PTE.desktop ||
{ ERRCODE="Failed to create desktop icon for PTE"; errors; }

if [[ ! -f $USERDIR/icons/pte_icon.png ]]; then
	mkdir -p $USERDIR/icons && wget --no-check-certificate -P $USERDIR/icons/ -N https://www.paranoia\
works.mobi/ptepc/img/pte_icon.png ||
	{ ERRCODE="Failed to icon image for PTE"; errors; }
	fi
fi

if [[ -f $SECDIR/SSEFilePC/ssefencgui.jar ]]; then

echo "[Desktop Entry]
Name=PFE
GenericName=Paranoia File Encrypter
Comment=File encrypter
Exec=pfe
Icon=$USERDIR/icons/pfe_icon.png
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/PFE.desktop ||
{ ERRCODE="Failed to create desktop icon for PFE"; errors; }

if [[ ! -f $USERDIR/icons/pfe_icon.png ]]; then
	mkdir -p $USERDIR/icons && wget --no-check-certificate -P $USERDIR/icons/ -N https://www.paranoia\
works.mobi/ssefepc/img/pfe_icon.png ||
	{ ERRCODE="Failed to icon image for PFE"; errors; }
	fi
fi

if hash keepassxc; then

echo "[Desktop Entry]
Name=KeepassXC
GenericName=KeepassXC
Comment=Password Manager
Exec=keepassxc
Icon=keepassxc
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/keepassxc.desktop ||
{ ERRCODE="Failed to create desktop icon for KeepassXC"; errors; }

fi

if hash gtkterm 2>/dev/null; then
    echo "[Desktop Entry]
    Name=GtkTerm
    GenericName=Serial Terminal Application
    Comment=Serial Terminal Application
    Exec=gtkterm
    Icon=$USERDIR/icons/gtkterm-icon.png
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/gtkterm.desktop ||
       { ERRCODE="Failed to create desktop icon for GTKTerm"; errors; }

	if [[ ! -f $USERDIR/icons/gtkterm-icon.png ]]; then
	mkdir -p $USERDIR/icons && wget -N http://icons.iconarchive.com/icons/\
papirus-team/papirus-apps/256/gtkterm-icon.png && mv gtkterm-icon.png \
  $USERDIR/icons/ ||
	{ ERRCODE="Failed to download icon image for GTKTerm"; errors; }
	fi

fi


if hash putty 2>/dev/null; then
    echo "[Desktop Entry]
    Name=PUTTY
    GenericName=Serial Terminal Application
    Comment=Serial Terminal Application
    Exec=putty
    Icon=putty
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/putty.desktop ||
       { ERRCODE="Failed to create desktop icon for Putty"; errors; }

fi

if hash pybitmessage 2>/dev/null; then
echo "[Desktop Entry]
Name=PyBitmessage
GenericName=Bitmessage
Comment=Secure Email
Exec=pybitmessage
Icon=pybitmessage
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/pybitmessage.desktop ||
	{ ERRCODE="Failed to create desktop icon for Bitmessage"; errors; }
fi


###########
if  [ -f $SECDIR/PyBitmessage*.AppImage ]; then
  BMAIVER=$(ls $SECDIR | grep PyBitmessage.*.AppImage)

echo "[Desktop Entry]
Name=PyBitmessage
GenericName=Bitmessage
Comment=Secure Email
Exec=$SECDIR/$BMAIVER &
Icon=$USERDIR/icons/bitmessage.png
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/pybitmessage.desktop ||
  { ERRCODE="Failed to create desktop icon for Bitmessage"; errors; }
if [[ ! -f $USERDIR/icons/bitmessage.png ]]; then
  mkdir -p $USERDIR/icons && wget -N https://wiki.bitmessage.org/images/f/f1/Bitmessagelogo-reduced.png && mv Bitmessagelogo-reduced.png \
  $USERDIR/icons/bitmessage.png ||
  { ERRCODE="Failed to download icon image for PyBitmessage"; errors; }
  fi

fi


################



if hash veracrypt 2>/dev/null; then
echo "[Desktop Entry]
Name=Veracrypt
GenericName=Veracrypt
Comment=Text encrypter
Exec=veracrypt --use-dummy-sudo-password
Icon=veracrypt
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/Veracrypt.desktop ||
	{ ERRCODE="Failed to create desktop icon for Veracrypt"; errors; }
fi

if hash electrum 2>/dev/null; then
echo "[Desktop Entry]
Name=electrum
GenericName=BTC Wallet
Comment=Electrum Bitcoin Wallet
Exec=electrum
Icon=electrum
Terminal=false
Type=Application
Categories=Cryptocurrency;" > $HOME/Desktop/electrum.desktop ||
	{ ERRCODE="Failed to create desktop icon for Electrum"; errors; }
fi

if hash wsjtx 2>/dev/null; then
echo "[Desktop Entry]
Version=1.0
Name=wsjtx
Comment=Amateur Radio Weak Signal Operating
Exec=wsjtx
Icon=wsjtx_icon
Terminal=false
X-MultipleArgs=false
Type=Application
Categories=AudioVideo;Audio;HamRadio;
StartupNotify=true;" > $HOME/Desktop/wsjtx.desktop ||
	{ ERRCODE="Failed to create desktop icon for WSJTX"; errors;  }
fi

if hash js8call 2>/dev/null; then
    echo "[Desktop Entry]
    Name=JS8Call
    GenericName=JS8Call
    Comment=Amateur Radio Weak Signal mode
    Exec=js8call
    Icon=js8call_icon
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/js8call.desktop ||
       { ERRCODE="Failed to create desktop icon for JS8Call"; errors; }
fi

if hash direwolf 2>/dev/null; then
  if [[ $(uname -m) = aarch64 ]]; then
    cd $PACKETDIR/direwolf && make install-rpi ||
	{ ERRCODE="Failed to create desktop icon for Direwolf"; errors;  }
  fi
fi

if hash yaac 2>/dev/null; then
  echo "[Desktop Entry]
    Name=YAAC
    GenericName=Yet Another APRS Client
    Comment=YAAC
    Exec=yaac
    Icon=$USERDIR/icons/yaaclogo.png
    Terminal=false
    Type=Application
    Categories=Network;HamRadio;" > $HOME/Desktop/yaac.desktop ||
       { ERRCODE="Failed to create desktop icon for YAAC"; errors; }

	if [[ ! -f $USERDIR/icons/yaaclogo.png ]]; then
	mkdir -p $USERDIR/icons && wget -P $USERDIR/icons/ -N  www.ka2ddo.org/ka2ddo/yaaclogo.png ||
	{ ERRCODE="Failed to download icon image for YAAC"; errors; }
	fi

fi

if hash gpa 2>/dev/null; then
echo "[Desktop Entry]
Name=GnuPrivacyAssist
GenericName=GPA
Comment=GPG Key Management
Exec=gpa
Icon=gpa
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/gpa.desktop ||
    { ERRCODE="Failed to create desktop icon for GPA"; errors; }
fi

if hash kleopatra 2>/dev/null; then
echo "[Desktop Entry]
Name=Kleopatra
GenericName=Kleopatra
Comment=GPG Key Management
Exec=kleopatra
Icon=kleopatra
Terminal=false
Type=Application
Categories=Network;Security;" > $HOME/Desktop/kleopatra.desktop ||
    { ERRCODE="Failed to create desktop icon for Kleopatra"; errors; }
fi

if hash chirp 2>/dev/null; then
echo "[Desktop Entry]
Name=Chirp
GenericName=Chirp
Comment=Radio Programing
Exec=chirp
Icon=$USERDIR/icons/chirp.png
Terminal=false
Type=Application
Categories=Radio;" > $HOME/Desktop/chirp.desktop ||
    { ERRCODE="Failed to create desktop icon for chirp"; errors; }


if [[ ! -f $USERDIR/icons/chirp.png ]]; then
    mkdir -p $USERDIR/icons && wget --no-check-certificate -O $USERDIR/icons/chirp.png -N https://user-images.githubusercontent.com/46135240/61046260-f7616a80-a3dc-11e9-9bb3-da248bcb73a9.png ||
    { ERRCODE="Failed to icon image for chirp"; errors; }
    fi
fi


if [[ -f $USERDIR/commstatone/commstat.py ]]; then

   if [[ $OSCN =~ wilma|xia ]]; then
echo "[Desktop Entry]
Name=CommstatOne
GenericName=CommstatOne
Comment=Commstat JS8 SA Tool
Path=$USERDIR/commstatone/
Exec=$USERDIR/commstatone/venv/bin/python3 commstat.py
Icon=$USERDIR/icons/commstat.jpg
Terminal=false
Type=Application
Categories=Radio;" > $HOME/Desktop/commstat.desktop ||
{ ERRCODE="Failed to create desktop icon for CommstatOne"; errors; }

    else
echo "[Desktop Entry]
Name=CommstatOne
GenericName=CommstatOne
Comment=Commstat JS8 SA Tool
Path=$USERDIR/commstatone/
Exec=python3 commstat.py
Icon=$USERDIR/icons/commstat.jpg
Terminal=false
Type=Application
Categories=Radio;" > $HOME/Desktop/commstat.desktop ||
{ ERRCODE="Failed to create desktop icon for CommstatOne"; errors; }
 fi

if [[ ! -f $USERDIR/icons/commstat.jpg ]]; then
    mkdir -p $USERDIR/icons && wget --no-check-certificate -O $USERDIR/icons/commstat.jpg -N https://amrron.com/wp-content/uploads/2023/02/JS8Call_Commstat_Data.jpg ||
    { ERRCODE="Failed to icon image for commstat"; errors; }
    fi
fi


chmod +x $HOME/Desktop/*.desktop

}
################################################################################
######################   Packet tools Functions ################################
################################################################################

function minicom_install() {
sudo apt-get -y install minicom
}


function gtkterm_install() {
sudo apt-get -y install gtkterm
}

function putty_install() {
sudo apt-get -y install putty
}


function direwolf_install() {

  if [[ -d $PACKETDIR/direwolf ]]; then
    cd $PACKETDIR/direwolf && git pull ||
	{ ERRCODE="Failed to update Direwolf"; errors; return; }
  else
  sudo apt-get install -y libasound2-dev ax25* libc6-dev ||
	{ ERRCODE="Failed to get dependencies for Direwolf"; errors; return; }

  cd $PACKETDIR && git clone https://www.github.com/wb2osz/direwolf ||
  	{ ERRCODE="Failed to download Direwolf"; errors; return; }

  mkdir $PACKETDIR/direwolf/build && cd $PACKETDIR/direwolf/build && cmake .. && make -j4 && sudo make install && make install-conf ||
  	{ ERRCODE="Failed to compile Direwolf"; errors; return; }

  fi

}

function yaac_install() {

wget -P $PACKETDIR -N https://www.ka2ddo.org/ka2ddo/YAAC.zip ||
  	{ ERRCODE="Failed to download YAAC"; errors; return; }

sudo apt-get -y install ca-certificates-java ||
  	{ ERRCODE="Failed to get ca-certificates-java dependencies for YAAC"; errors; return; }

sudo apt-get -y install openjdk-11-jre librxtx-java unzip ||
  	{ ERRCODE="Failed to get openjdk-8-jre librxtx-java unzip dependencies for YAAC"; errors; return; }

unzip -o -d $PACKETDIR/YAAC $PACKETDIR/YAAC.zip ||
  	{ ERRCODE="Failed to unzip YAAC"; errors; return; }

if ! hash yaac; then

cat > $PACKETDIR/yaac <<EOF
#!/bin/bash

java -jar $PACKETDIR/YAAC/YAAC.jar

EOF

chmod +x $PACKETDIR/yaac && sudo mv $PACKETDIR/yaac /usr/local/bin/yaac ||
  	{ ERRCODE="Failed to create launcher for YAAC in /usr/local/bin/"; errors; return; }

fi

}

function packet_guide() {
cat > $HOME/Documents/Packet-Radio-setup.txt <<EOF

You should have a few tools installed for use with packet radio modes.

If you have a non-KISS TNC (i.e. Kantronics KPC3+ etc) use minicom to talk \
to your TNC.
Minicom will need to run as root. You can specify the device with a -d argument.

i.e. sudo minicom -d /dev/ttyUSB0

Or

sudo minicom -s

This will start with the setup prompt where you can select your device and \
then set it as default. Set speed to 9600.

You should then be able to access your TNC with just

sudo minicom

I found using the RT Systems Kantronics to USB cable it didnt want to work \
properly.

type lsusb -v

write down your product and vendor id for the cable.

Next,

sudo chmod 666 /sys/bus/usb-serial/drivers/ftdi_sio/new_id

Finally, add your product and vendor id into the file.

nano /sys/bus/usb-serial/drivers/ftdi_sio/new_id

For example the only text in mine is
2100 004


If you do not have a TNC.

direwolf is a software TNC which you can use for packet. I recommend reading \
up online or watching some youtube videos but here are the basics.

Type:

nano direwolf.conf

Set audio device, usually hw1,0 only need to uncomment. Change Callsign

ctrl-x with y to save the filename.

now run direwolf with a -p flag and itll create a new port for the TNC.

direwolf -p

Now we will open a new terminal.

From here we want to change our ax25 ports.
sudo nano /etc/ax25/axports

Follow the example listing
1	MYCALL-1	1200	255	2	144.930 MHz (1200 bps)

1 is the name of this port. You can change that to what ever works for you.

Now we will attach the TNC to the AX25 stack

sudo kissattach /tmp/kisstnc 1

We can use axlisten or axcall.

If you want to connect to W7ABC-8 you can type

axcall 1 w7abc-8

I've noticed that my first call doesnt seem to go out properly every time.

If I kill it with ctrl-c and then try again it seems to work.

We can release the TNC by killing kissattach

sudo killall kissattach


This should get you started playing with packet.

If you are using direwolf, your soundcard to radio cable is important.
I've had good luck with the EasyDigi VOX from ebay. A real kantronics works \
much better for me so far. A basic KISS TNC is handled just like direwolf \
(sudo kissattach DEVICE PORT and axcall to send)
Most KISS TNC's have very limited buffers and are intended for use with \
APRS, not full packet.

Search youtube for some great videos on setting up direwolf.

EOF
}
################################################################################
######################   Other tools Functions #################################
################################################################################
function vim_install() {
sudo apt-get -y install vim
}

function filezilla_install() {
sudo apt-get -y install filezilla
}

function gedit_install() {
sudo apt-get -y install gedit
}

function libreoffice_install() {
sudo apt -y install libreoffice
}

function keyboard_install() {
sudo apt-get -y install matchbox-keyboard
}

function screen_install() {
sudo apt-get -y install screen
}

function speedtest_install() {
sudo apt-get -y install speedtest-cli
}

function raspap_install() {

  if [[ $(uname -m) = aarch64 ]]; then
  wget -q https://git.io/voEUQ -O $HOME/Desktop/raspap ||
    { ERRCODE="Failed to download RaspAP script"; errors; return; }

  cat > $HOME/Documents/RaspAP-Hotspot-Setup.txt <<EOF
  RaspAP from https://github.com/billz/raspap-webgui#quick-installer

  After the reboot at the end of the installation the wireless network will \
  be configured as an access point as follows:

    IP address: 10.3.141.1
    Username: admin
    Password: secret
    DHCP range: 10.3.141.50 to 10.3.141.255
    SSID: raspi-webgui
    Password: ChangeMe


EOF

echo raspapsetup >> /tmp/cleanup

else

  echo noraspapsetup >> /tmp/cleanup

fi

}


function electrum_install() {
if [[ $NEWELECTRUM == "SERVER DOWN" ]]; then
	echo "Electrum server unresponsive or broken link." >> /tmp/failures
	return
fi
  if [[ ${ELECTRUM} == ${NEWELECTRUM} ]]; then
        echo "Electrum is already at the Version $NEWELECTRUM"
    else

      if hash python3 2>/dev/null; then
        echo "Getting dependencies"
        sudo apt-get install -y python3-setuptools python3-pyqt5 python3-pip libsecp256k1-dev ||
	{ ERRCODE="Failed to get dependencies for Electrum"; errors; return; }

        sudo python3 -m pip install https://download.electrum.org/$NEWELECTRUM/\
Electrum-${NEWELECTRUM}.tar.gz ||
        { ERRCODE="Failed to install Electrum.  Likely due to running older OS without python3 support."; errors; return; }
      else
        echo noelectrum >> /tmp/cleanup
      fi
  fi
}

function conky_install() {

if ! hash conky; then
sudo apt-get -y install conky-all ||
        { ERRCODE="Failed to install Conky"; errors; return; }
fi

if [[ -f $HOME/.conkyrc ]]; then
mv $HOME/.conkyrc $HOME/.conkyrc-$(date +%h%d).bak ||
        { ERRCODE="Failed to backup old .conkyrc file"; errors; return; }
fi

### Move Conky.Config before size case statement since so much is shared --CS

	cat > $HOME/.conkyrc <<\EOF
conky.config = {
    alignment = 'top_right',
    background = false,
    border_width = 0,
	border_inner_margin = 15,
    color1 = '19A094',
    color2 = 'FF5252',
    cpu_avg_samples = 2,
	default_color = 'grey',
    default_outline_color = 'grey',
    default_shade_color = 'grey',
	double_buffer = true,
	draw_borders = true,
    draw_graph_borders = false,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    font = 'Monaco:size=12',
    gap_x = 20,
    gap_y = 50,
    minimum_height = 5,
    maximum_width = 400,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    own_window_class = 'Conky',
    own_window_type = 'desktop',
    own_window_transparent = true,
    own_window_argb_visual = true,
    own_window_argb_value = 0,
    stippled_borders = 0,
    update_interval = 1.2,
    uppercase = none,
    use_spacer = 'left',
    show_graph_scale = false,
    show_graph_range = false
}
EOF

case $CONKYSEL in
	LARGE)
    # Use Conky.Config as is, append conky.text
	cat >> $HOME/.conkyrc <<\EOF
conky.text = [[
${color1}${font Roboto Mono:style=Bold:pixelsize=22}\
Central:${goto 175}Eastern:${alignr}Zulu:
$color $hr
${color grey}${font Roboto:pixelsize=22}${tztime America/Chicago %H:%M:%S}\
${goto 175}${tztime America/New_York %H:%M:%S}\
${alignr}${tztime Zulu %H:%M:%S}
${font Roboto:pixelsize=12}${tztime America/Chicago %A %m-%d-%y}\
${goto 175}${tztime America/New_York %A %m-%d-%y}\
${alignr}${tztime Zulu %A %m-%d-%y}

$alignc ${font Roboto:style=Medium:pixelsize=35} ${color grey}\
${color D4AF37} BTC: ${execi 60 echo -n "$" ; curl -s https://api.coinbase.com/v2/prices/spot?currency=USD | grep -o [0-9][0-9][0-9][0-9][0-9].[0-9][0-9]}
#Uptime:$color $uptime
${color1}${font Roboto Mono:style=Bold}\
SYSTEM$color $hr\
${font}
${color2}CPU : ${color1}${goto 240}\
$cpu% ${goto 280}${cpubar cpu0 10,140}
${color2}RAM :${color grey} $mem/$memmax\
 ${color1}${goto 240}$memperc% ${goto 280}${membar 10,140}
${color2}Swap:${color grey} $swap/$swapmax\
 ${color1}${goto 240}$swapperc% ${goto 280}${swapbar 10,140}
${color2}Temp: ${color grey}${acpitemp}°C
${color2}/ ${color grey}\
 ${goto 90}${fs_used /}/${fs_size /}${color}\
 ${goto 250}${color grey}${color1}${fs_used_perc /}% ${fs_bar 10,120 /}
${color2}/home ${color grey}\
 ${goto 90}${fs_used /home}/${fs_size /home}${color}\
 ${goto 250}${color grey}${color1}${fs_used_perc /home}% ${fs_bar 10,120 /home}

${color1}${font Roboto Regular:style=Bold}\
NETWORKING$color${font} $hr
${color2}External IP:$color ${exec curl -s www.icanhazip.com}${alignr}${color2}Country: $color${execi 60 curl ipinfo.io/country}
${if_up eth0}\
${color2}eth0\
 ${color}${font}${goto 270}${addrs eth0}
 ${color}Total:${totaldown eth0} \
 ${goto 210}${color}Total:${totalup eth0}
${downspeedgraph eth0 30,195 00ffff 19a094} \
${upspeedgraph eth0 30,195 00ffff 19A094}
${endif}\
${if_up wlp1s0}\
${color}${font}\
${color2}wlp1s0: ${color}${wireless_essid wlp1s0}\
 ${color}${font}${goto 270}${addr wlp1s0}
 ${color white}${font}Strength:$color${wireless_link_qual_perc wlp1s0}%\
 ${goto 200}${color white}MAC: ${color}${wireless_ap wlp1s0}
 ${color}Total:${totaldown wlp1s0} \
 ${goto 210}${color}Total:${totalup wlp1s0}
${downspeedgraph wlp1s0 30,195 00ffff 19A094} \
${upspeedgraph wlp1s0 30,195 00ffff 19A094}
${endif}\
${if_up tun0}\
${color}${font}\
${color2}tun0\
 ${color}${font}${goto 270}${addr tun0}
 ${color}Total:${totaldown tun0} \
 ${goto 210}${color}Total:${totalup tun0}
${downspeedgraph tun0 30,195 00ffff 19A094} \
${upspeedgraph tun0 30,195 00ffff 19A094}${endif}
${if_empty ${exec cat /proc/net/route | grep tun}}${color red}VPN DOWN${color}${else}${color green}VPN UP${color}${endif}
${alignr}${if_running tor}${color green}TOR ACTIVE${else}${color red}TOR OFF$endif

${color1}${font Roboto Mono:style=Bold}\
PROCESSES$color$font $hr
Total:$processes  Running:$running_processes
${color2} Name             ${goto 205}PID         ${goto 275}CPU%     ${goto 350}MEM%
${color} ${top name 1} ${goto 200}${top pid 1} ${goto 275}${top cpu 1} ${goto 350}${top mem 1}
${color} ${top name 2} ${goto 200}${top pid 2} ${goto 275}${top cpu 2} ${goto 350}${top mem 2}
${color} ${top name 3} ${goto 200}${top pid 3} ${goto 275}${top cpu 3} ${goto 350}${top mem 3}
${color} ${top name 4} ${goto 200}${top pid 4} ${goto 275}${top cpu 4} ${goto 350}${top mem 4}
${color} ${top name 5} ${goto 200}${top pid 5} ${goto 275}${top cpu 5} ${goto 350}${top mem 5}

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 curl -s "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/" | grep -m1 -o "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5"}

${color1}${font Roboto Mono:style=Bold}\
Radio Tools $color$font $hr

${if_running ardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP OFF$endif \
${goto 200}${if_running pat}${color green}PAT ACTIVE${else}${color red}PAT OFF$endif
]]
EOF
	;;

	STANDARD)
    ### Edit ConkyConfig Large for us
    sed -i 's/Monaco:size=12/Monaco:size=10/' $HOME/.conkyrc
    sed -i 's/maximum_width = 400/maximum_width = 300/'  $HOME/.conkyrc

###  append conky.text --CS
	cat >> $HOME/.conkyrc <<\EOF
conky.text = [[
${color1}${font Roboto Mono:style=Bold:pixelsize=14}\
Central:${goto 135}Eastern:${alignr}Zulu:
$color $hr
${color grey}${font Roboto:pixelsize=12}${tztime America/Chicago %H:%M:%S}\
${goto 135}${tztime America/New_York %H:%M:%S}\
${alignr}${tztime Zulu %H:%M:%S}
${font Roboto:pixelsize=12}${tztime America/Chicago %m-%d-%y}\
${goto 135}${tztime America/New_York %m-%d-%y}\
${alignr}${tztime Zulu %m-%d-%y}

#Uptime:$color $uptime
${color1}${font Roboto Mono:style=Bold}\
SYSTEM$color $hr\
${font}
${color2}CPU : ${color1}${goto 180}\
$cpu% ${goto 210}${cpubar cpu0 10,105}
${color2}RAM :${color grey} $mem/$memmax\
 ${color1}${goto 180}$memperc% ${goto 210}${membar 10,105}
${color2}Temp: ${color grey}${acpitemp}°C


${color2}/ ${color grey}\
 ${goto 65}${fs_used /}/${fs_size /}${color}\
 ${goto 180}${color grey}${color1}${fs_used_perc /}% ${goto 210}${fs_bar 10,105 /}
${color2}/home ${color grey}\
 ${goto 65}${fs_used /home}/${fs_size /home}${color}\
 ${goto 180}${color grey}${color1}${fs_used_perc /home}% ${goto 210}${fs_bar 10,105 /home}

${color1}${font Roboto Regular:style=Bold}\
NETWORKING$color${font} $hr
${color2}External IP:$color ${exec curl -s www.icanhazip.com}${alignr}${color2}Country: $color${execi 60 curl ipinfo.io/country}
${if_up eth0}\
${color2}eth0\
 ${color}${font}${goto 270}${addrs eth0}
 ${color}Total:${totaldown eth0} \
 ${goto 210}${color}Total:${totalup eth0}
${downspeedgraph eth0 30,195 00ffff 19a094} \
${upspeedgraph eth0 30,195 00ffff 19A094}
${endif}\
${if_up wlp1s0}\
${color}${font}\
${color2}wlp1s0: ${color}${wireless_essid wlp1s0}\
 ${color}${font}${goto 230}${addr wlp1s0}
 ${color white}${font}Strength:$color${wireless_link_qual_perc wlp1s0}%\
 ${goto 155}${color white}MAC: ${color}${wireless_ap wlp1s0}
 ${color}Total Down:${totaldown wlp1s0} \
 ${goto 200}${color}Total Up:${totalup wlp1s0}
${endif}
${if_up tun0}\
${color}${font}\
${color2}VPN tun0\
 ${color}${font}${goto 230}${addr tun0}
 ${color}Total Down:${totaldown tun0} \
 ${goto 200}${color}Total Up:${totalup tun0}${endif}

${if_empty ${exec cat /proc/net/route | grep tun}}${color red}VPN DOWN${color}${else}${color green}VPN UP${color}${endif}
${if_running tor}${color green}TOR ACTIVE${else}${color red}TOR OFF$endif

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 curl -s "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/" | grep -m1 -o "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5"}

${color1}${font Roboto Mono:style=Bold}\
Radio Tools $color$font $hr

${if_running ardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP OFF$endif \
${goto 200}${if_running pat}${color green}PAT ACTIVE${else}${color red}PAT OFF$endif
]]
EOF
	;;

	MINIMAL)
	### Edit ConkyConfig Large
    sed -i 's/own_window_argb_visual = true/own_window_argb_visual = false/'  $HOME/.conkyrc

    ###  append conky.text --CS
	cat >> $HOME/.conkyrc <<\EOF

conky.text = [[

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 wget -q -O - "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/ "|grep -m1 "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5" -o | head -n1}

${if_running ardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP INACTIVE${endif}
${if_running pat http}${color green}PAT ACTIVE${else}${color red}PAT INACTIVE${endif}
]]
EOF
	;;
esac ||
        { ERRCODE="Failed to create .conkyrc configuration file."; errors; return; }

if [[ $(uname -m) = aarch64 ]]; then
sed -i '/own_window_argb_value/d' $HOME/.conkyrc ||
        { ERRCODE="Failed to remove own_window_argb_value from .conkyrc for raspbian stability"; errors; return; }

sed -i '/own_window_argb_visual/d' $HOME/.conkyrc ||
        { ERRCODE="Failed to remove own_window_argb_visual from .conkyrc for raspbian stability"; errors; return; }

sed -i s/'desktop'/'normal'/ $HOME/.conkyrc ||
        { ERRCODE="Failed to change own_window_type to normal for raspbian stability."; errors; return; }

sed -i s/'ardopc'/'piardopc'/ $HOME/.conkyrc ||
        { ERRCODE="Failed to change ardopc to piardop for raspbian status."; errors; return; }
fi

#### Find ethernet and wifi interfaces and update conkyrc --CS
## default ethernet = eth0  default wifi = wlp1s0
##  use 2nd field from ip link and word starts with e for all ethernet, w for all wireless
##  Only picks first one of each.
export ethernet_interface=$(ip link | cut -d: -f2 | grep  '\be')
export wifi_interface=$(ip link | cut -d: -f2 | grep  '\bw')

### Test to make sure the string isnt blank (doesnt have that type of interface)
[ $ethernet_interface ] && ( sed -i "s/eth0/$ethernet_interface/g" $HOME/.conkyrc )
[ $wifi_interface ]     && ( sed -i "s/wlp1s0/$wifi_interface/g" $HOME/.conkyrc  )


echo conkysetup >> /tmp/cleanup

}

################################################################################
######################   Security tools Functions ##############################
################################################################################
function fail2ban_install() {
sudo apt-get -y install fail2ban
}

function old_keepassxc_install() {

if hash snap 2>/dev/null; then
sudo snap install core && sudo snap install keepassxc ||
{ ERRCODE="Failed to install snap / snap core requirements for KeepassXC"; errors; return; }

else

  if [[ ${PLAT} = "Linuxmint" ]]; then
            if [[ -f /etc/apt/preferences.d/nosnap.pref ]]; then
              sudo mv /etc/apt/preferences.d/nosnap.pref /etc/apt/preferences.d/nosnap.pref.bak ||
              { ERRCODE="Failed to remove Linuxmint block from using snaps.  Remove /etc/apt/preferences.d/nosnap.pref to enable snaps."; errors; return; }
                fi
              fi
sudo apt-get install -y snapd && sudo snap install core && sudo snap install keepassxc ||
{ ERRCODE="Failed to install snap / snap core requirements for KeepassXC"; errors; return; }

  fi
#fi
#flatpak install flathub org.keepassxc.KeePassXC
}

function keepassxc_install() {
       if [ $PLAT = Linuxmint ]; then      
        if ! hash flatpak 2>/dev/null; then
          sudo apt-get install -y flatpak ||
          { ERRCODE="Failed to install flatpak."; errors; return; }
        fi
          sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo ||
          { ERRCODE="Failed to add flathub remote."; errors; return; }
                    
          sudo flatpak install -y --noninteractive flathub org.keepassxc.KeePassXC ||
          { ERRCODE="Failed to install keepassxc flatpak."; errors; return; }
      
      else
          echo snap_fail_keepass >> /tmp/cleanup

          sudo apt-get install -y snapd ||
            { ERRCODE="Failed to install snapd."; errors; return; }

### Testing to see if this avoids a restart requirement 
          sudo modprobe squashfs ||
            { ERRCODE="Failed to set modprobe squashfs to avoid restart."; errors; return; }

          sudo snap install core ||
            { ERRCODE="Failed to install snap / snap core requirements for KeepassXC"; errors; return; }

          sudo snap install keepassxc ||
            { ERRCODE="Failed to install snap / snap core requirements for KeepassXC"; errors; return; }

  fi
}

function iceweasel_install() {
sudo apt-get install -y iceweasel
}

function gtkhash_install() {
sudo apt-get install -y gtkhash
}

function gpa_install() {
sudo apt-get install -y gpa
}

function kleopatra_install() {
sudo apt-get install -y kleopatra scdaemon
}

function harden_ssh() {
if [[ ! -f $HOME/.ssh/authorized_keys ]]; then
	mkdir -p $HOME/.ssh/ && touch $HOME/.ssh/authorized_keys ||
		{ ERRCODE="Failed to create SSH folders"; errors; return; }

	chmod 700 $HOME/.ssh/ ||
		{ ERRCODE="Failed to modify permission on $HOME/.ssh/"; errors; return; }

	chmod 600 $HOME/.ssh/authorized_keys ||
		{ ERRCODE="Failed to modify permission on $HOME/.ssh/authorized_keys"; errors; return; }


fi


cat > $HOME/Documents/SSH-setup.txt <<EOF
In order to setup secure ssh using preshared keys from your computer to the \
pi do the following:

From your computer: If you do not already have a public/private key, \
generate one;

ssh-keygen -b 4096 -t rsa

copy the public key from ~/.ssh/id_rsa.pub to your raspberry pi authorized_keys\
 file in /home/pi/.ssh/authorized_keys

You can do this by entering:
cat ~/.ssh/id_rsa.pub | ssh pi@raspberrypi.local 'cat >> .ssh/authorized_keys'

Substitute your ip address of the pi in the preceding command if \
raspberrypi.local doesn't work for you.

Now try to ssh into the pi (exmaple: ssh pi@192.168.1.2)

If it works with out a password, you're set.

Now disable access from anyone without the key.

sudo nano /etc/ssh/sshd_config

uncomment (remove the '#') and change yes to no on the line that says \
'#PasswordAuthentication yes'

I also recommend changing the default port in this file from 22 to something \
much higher. If you do this, you will then need to specify the port you \
chose when you connect. i.e., ssh pi@192.168.1.2 -p 1776

If you want to make an alias for ssh into your pi from your computer do \
the following.

nano .ssh/config

This will open or create a file called config in your .ssh folder.

Add the following information, adjusting as required / wanted. Again, you can \
replace raspberrypi.local with a specific IP address if needed.

Host radio
	HostName raspberrypi.local
	User pi
	Port XXX

Now we should be able to just type;
ssh radio

EOF

}


function pte_install() {

if ! hash java; then
sudo apt-get install -y default-jre ||
{ ERRCODE="Failed to get default-jre dependency for PFE"; errors; return; }
fi

cd $SECDIR && wget -N http://www.paranoiaworks.mobi/download/files/PTE-PC.zip ||
	{ ERRCODE="Failed to download PTE"; errors; return; }

unzip -o PTE-PC.zip && rm PTE-PC.zip ||
	{ ERRCODE="Failed to unzip PTE"; errors; return; }

if ! hash pte 2>/dev/null; then

cat > /tmp/pte <<EOF
#!/bin/bash
java -jar '$SECDIR/Paranoia Text Encryption/pte.jar'
EOF

chmod +x /tmp/pte && sudo mv /tmp/pte /usr/local/bin/pte ||
{ ERRCODE="Failed to create system launcher for PTE in /usr/local/bin/"; errors; return; }

fi
}


function pfe_install() {

if ! hash java; then
sudo apt-get install -y default-jre ||
{ ERRCODE="Failed to get default-jre dependency for PFE"; errors; return; }
fi

cd $SECDIR && wget -N http://www.paranoiaworks.mobi/download/files/SSEFilePC.zip ||
	{ ERRCODE="Failed to download PFE"; errors; return; }

unzip -o SSEFilePC.zip && rm SSEFilePC.zip ||
	{ ERRCODE="Failed to unzip PFE"; errors; return; }

if ! hash pfe 2>/dev/null; then

cat > /tmp/pfe <<EOF
#!/bin/bash
java -jar '$SECDIR/SSEFilePC/ssefencgui.jar'
EOF

chmod +x /tmp/pfe && sudo mv /tmp/pfe /usr/local/bin/pfe ||
{ ERRCODE="Failed to create system launcher for PFE in /usr/local/bin/"; errors; return; }

fi
}


function veracrypt_install() {
if [[ $NEWVC == "SERVER DOWN" ]]; then
        echo "Veracrypt server unresponsive or broken link." >> /tmp/failures
        return
fi

if [[ $VCVER == $NEWVC ]]; then
      echo "Veracrypt already installed at current version.  No updates available."
    else

      if [[ $(uname -m) =~ (armv7l|aarch64) ]] && [[ $(getconf LONG_BIT) = '32' ]]; then
      SYS=$"armhf"

      elif [[ $(uname -m) = aarch64 ]] && [[ $(getconf LONG_BIT) = '64' ]]; then
      SYS=$"arm64"

      elif [[ $(uname -m) = x86_64 ]]; then
      SYS=$"amd64"

      else
      SYS=$"i386"
      fi

  if [[ ${OSCN} = "buster" ]]; then
    
    VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Debian-10-${SYS}.deb"
    
  elif [[ $OSCN = "bullseye" ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Debian-11-${SYS}.deb"
  
  elif [[ $OSCN =~ (wilma|xia) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Ubuntu-24.04-${SYS}.deb"

  elif [[ $OSCN =~ (vanessa|vera|victoria|virginia) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Ubuntu-22.04-${SYS}.deb"

  elif [[ $OSCN =~ (ulyana|ulyssa|uma|una) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Ubuntu-20.04-${SYS}.deb"

  elif [[ $OSCN =~ (eoan|focal|amber) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Ubuntu-19.10-${SYS}.deb"

  elif [[ $OSCN =~ (disco) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Ubuntu-19.04-${SYS}.deb"

  elif [[ $OSCN =~ (bionic|cosmic|tara|tessa|tina|tricia) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Ubuntu-18.04-${SYS}.deb"

  elif [[ $OSCN =~ (xenial|yakkety|zesty|artful|sarah|serena|sonya|sylvia) ]]; then
      VCLINK=$"https://launchpad.net/veracrypt/trunk/${NEWVC}/+download/veracrypt-${NEWVC}-Ubuntu-16.04-${SYS}.deb"
  fi


VCFILE=$(echo ${VCLINK} | awk -F '/' '{print $8}')


    cd $SECDIR && wget -N ${VCLINK} ||
      { ERRCODE="Failed to download Veracrypt"; errors; return; }

      if [[ $OSCN =~ wilma|xia ]]; then
        sudo apt-get install -y libccid pcscd

    else
        sudo apt-get install -y libwxgtk3.0-gtk3-0v5 libwxbase3.0-0v5 ||
        { ERRCODE="Failed to get dependencies for Veracrypt"; errors; return; }
    fi
    
        sudo apt-get install -y ${SECDIR}/${VCFILE} ||
        { ERRCODE="Failed to install Veracrypt"; errors; return; }
fi
}



function bitmessage_install() {
if apt-cache show python-qt4 &>/dev/null; then

  if hash pybitmessage 2>/dev/null; then

   cd $SECDIR/PyBitmessage/src && git fetch --all && git reset --hard origin/master ||
    { ERRCODE="Failed to update Bitmessage"; errors; return; }

  else
  sudo apt-get install -y python openssl libssl-dev git python-msgpack python-qt4 ||
  { ERRCODE="Failed to get dependencies for Bitmessage"; errors; return; }

  cd $SECDIR/ && git clone https://github.com/Bitmessage/PyBitmessage ||
  { ERRCODE="Failed to download Bitmessage"; errors; return; }

  cd $SECDIR/PyBitmessage && sudo python setup.py install ||
  { ERRCODE="Failed to install Bitmessage"; errors; return; }
  fi

  echo pybitmessagesetup >> /tmp/cleanup


## Get PyBitmessage via appimage if running Linuxmint

elif [[ $PLAT = Linuxmint ]]; then

  BMAIRELDATE=$(curl -s https://appimage.bitmessage.org/releases/ | tail -n 3 | grep -o [0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9] | tail -n 1)
  BMAIFILE=$(curl -s https://appimage.bitmessage.org/releases/20221018/ | grep .AppImage | awk -F '"' '{print $2}')

    if [[ -f $SECDIR/$BMAIFILE ]]; then
    BMAIVER=$(ls $SECDIR | grep PyBitmessage.*.AppImage)

      if [[ $BMAIVER = $BMAIFILE ]]; then
      echo "PyBitmessage AppImage already at current available version"; return
      else

      cd $SECDIR && wget -N https://appimage.bitmessage.org/releases/${BMAIRELDATE}/${BMAIFILE} ||
    { ERRCODE = "Failed to Download PyBitmessage AppImage."; errors; return; }

    chmod a+x $SECDIR/$BMAIFILE ||
    { ERRCODE = "Failed to set execute permissions on PyBitmessage AppImage."; errors; return; }

    rm $SECDIR/$BMAVER ||
    { ERRCODE = "Failed to remove old PyBitmessage AppImage."; errors; return; }
      fi

    else

    cd $SECDIR && wget -N https://appimage.bitmessage.org/releases/${BMAIRELDATE}/${BMAIFILE} ||
    { ERRCODE = "Failed to Download PyBitmessage AppImage."; errors; return; }

    chmod a+x $SECDIR/$BMAIFILE ||
    { ERRCODE = "Failed to set execute permissions on PyBitmessage AppImage."; errors; return; }


    sudo ln -sf /etc/xdg/menus/{gnome-applications,debian-menu}.menu ||
    { ERRCODE = "Failed to create symlink required to run PyBitmessage."; errors; return; }

  fi


else
  echo pybitmessagefail >> /tmp/cleanup
  { ERRCODE="Bitmessage relies upon no longer supported packages."; errors; return; }

fi
}

function wifi_off() {
if [[ $(uname -m) = aarch64 ]]; then
	if grep -q dtoverlay=pi3-disable-wifi /boot/config.txt; then
		echo "Wifi is already disabled at boot"
	else
		echo "dtoverlay=pi3-disable-wifi" | sudo tee -a /boot/config.txt

cat > $HOME/Documents/wifi-bt-disabled.txt <<EOF
Wifi and/or Bluetooth have been disabled from loading at boot.

Should you wish to re-enable these functions you can run the script, \
selecting WIFI_ON or BT_ON or you will need to manually edit your \
/boot/config.txt file.

The bottom two lines will have:
dtoverlay=pi3-disable-bt
dtoverlay=pi3-disable-wifi

You can either comment these out (place a # before the line) or delete \
these lines.  Save your changes and reboot the Pi.

If you want to use the script to disable them in the future please delete \
the lines rather than commenting them out.

EOF

	echo wirelessoff >> /tmp/cleanup

	fi
else
	echo "Wifi Disable is limited to the Raspberry Pi 3"
  echo wirelesserror >> /tmp/cleanup
fi

}

function bt_off() {
if [[ $(uname -m) = aarch64 ]]; then
	if grep -q dtoverlay=pi3-disable-bt /boot/config.txt; then
		echo "Bluetooth is already disabled at boot"
	else
		echo "dtoverlay=pi3-disable-bt" | sudo tee -a /boot/config.txt
cat > $HOME/Documents/wifi-bt-disabled.txt <<EOF
Wifi and/or Bluetooth have been disabled from loading at boot.

Should you wish to re-enable these functions you can run the script, selecting \
WIFI_ON or BT_ON or you will need to manually edit your /boot/config.txt file.

The bottom two lines will have:
dtoverlay=pi3-disable-bt
dtoverlay=pi3-disable-wifi

You can either comment these out (place a # before the line) or delete these \
lines.  Save your changes and reboot the Pi.

If you want to use the script to disable them in the future please delete the \
lines rather than commenting them out.

EOF

	echo wirelessoff >> /tmp/cleanup

	fi
else
	echo "Bluetooth Disable is limited to the Raspberry Pi 3"
  echo wirelesserror >> /tmp/cleanup
fi

}

function wifi_on() {
if [[ $(uname -m) = aarch64 ]]; then
	if grep -q dtoverlay=pi3-disable-wifi /boot/config.txt; then
		sudo sed -i s/dtoverlay=pi3-disable-wifi//g /boot/config.txt
	else
		echo wirelesson >> /tmp/cleanup
	fi
else
	echo "WIFI Enable is limited to the Raspberry Pi 3"
  echo wirelesserror >> /tmp/cleanup
fi
}

function bt_on() {
if [[ $(uname -m) = aarch64 ]]; then
	if grep -q dtoverlay=pi3-disable-bt /boot/config.txt; then
		sudo sed -i s/dtoverlay=pi3-disable-bt//g /boot/config.txt
	else
		echo wirelesson >> /tmp/cleanup
	fi
else
	echo "Bluetooth Enable is limited to the Raspberry Pi 3"
  echo wirelesserror >> /tmp/cleanup
fi
}


################################################################################
###############################   Menus   ######################################
################################################################################

#### Ham Radio tools menu ###


function radio_installer() {

	#Set user install directory For Radio Tools.
	UserDir=$(yad --image=$DIR/images/corps-icon.png --center --radiolist --print-column=2 --separator="" \
	--title "AmRRON Radio Tools Installer / Updater" \
	--text "Select your desired installation folder for HAM Radio Programs." \
	--list --wrap-width=200 --wrap-cols=3 \
	--column="Tick" --column="Option" --column="Description" \
	 true "Default" "Default location will be $HOME/Radio" \
	 false "Custom" "I wish to select my own location" \
	 ${r} ${c}) ||
	 { yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }


	case ${UserDir} in
	    Default)
	      yad --center \
	      --image=$DIR/images/corps-icon.png \
	      --title "Radio Tools Directory" \
	      --text "You have selected $HOME/Radio as your installation directory" ${r} ${c} ||
	      { radio_installer; return; }

	        USERDIR=$HOME/Radio
	        if [[ ! -d $USERDIR ]]; then
	        mkdir -p $USERDIR
	        fi
	        ;;
	    Custom)
	        USERDIR=$(yad --center --form --separator="" --field="MDIR:MDIR" $HOME \
		--image=$DIR/images/corps-icon.png \
	        --title "Radio Tools Directory" \
	        --text "Please select your desired directory or create a new one." ${r} ${c}) ||
	        { radio_installer; return; }
	        if [[ ! -d $USERDIR ]]; then
	        mkdir -p $USERDIR
	        fi
	        ;;
	    esac



yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Radio Programs" \
--text "Select the programs you would like to install"  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Program" --column="Description" \
--column="Installed version" --column="Updated Version" \
 false "HAMLIB" "Hamlib libraries provide rig control for your radio" \
 "${HAMLIB}" "${NEWHAMLIB}" \
  false "HAMLIB4.5.5" "Hamlib 4.5.5 | JS8Call will not build with Hamlib 4.6.  4.5.5 is default with Easy Button" \
 "${HAMLIB}" "${NEWHAMLIB}" \
 false "FLDIGI" "FLDIGI Radio Program" "${FLDIGI}" "${NEWFLDIGI}" \
 false "FLMSG" "FLMSG Radio Program" "${FLMSG}" "${NEWFLMSG}" \
 false "FLMSG_4.0.19" "FLMSG Radio Program" "${FLMSG}" "4.0.19" \
 false "FLAMP" "FLAMP Radio Program" "${FLAMP}" "${NEWFLAMP}" \
 false "FLRIG" "FLRIG Radio Control Program" "${FLRIG}" "${NEWFLRIG}" \
 false "FLWRAP" "FLWRAP Radio Control Program" "${FLWRAP}" "${NEWFLWRAP}" \
 false "ARDOP" "Downloads the newest ARDOP v1 TNC" "Unknown" "${NEWARDOP}" \
 false "ARDOPCF" "Downloads the newest ARDOPCF TNC fork" "${ARDOPCFVER}" "${NEWARDOPCF}" \
 false "ARDOPGUI" "Downloads the newest ARDOP Graphical User Interface" "Unknown" "${NEWARDOPGUI}" \
 false "ARIM" "Amateur Radio Instant Messanger" "${ARIM}" "${NEWARIM}" \
 false "GARIM" "Graphical ARIM interface" "${GARIM}" "${NEWGARIM}" \
 false "VARIM" "VARA Graphical ARIM interface" "${VARIM}" "${NEWVARIM}" \
 false "PAT" "ARDOP email client / Winlink alternative" "${PAT}"  "${NEWPAT}" \
 false "FINDARDOP" "Terminal based offline ARDOP/VARA station finder" "${FINDARDOPVER}" "Unable to determine" \
 false "VOACAP" "VOACAP Propagation tool and python GUI." "${VOACAPLVER}" "${NEWVOACAPL}" \
 false "CHIRP" "Radio Programing Software" "${CHIRP}" "${NEWCHIRP}" \
 false "WSJTX" "WSJTX Weak Signal QSO program Release Candidate" "${WSJTXVER}" "${NEWWSJTX}" \
 false "WSJTX_STABLE" "WSJTX Weak Signal QSO program General Availability Release" "${WSJTXVER}" "${NEWWSJTX_STABLE}" \
 false "JS8CALL" "JS8Call Weak Signal QSO program." "${JS8CALLVER}" "${NEWJS8CALLVER}" \
 false "COMMSTATONE" "JS8Call mapping (Mint only)" "Unknown" "Unknown" \
 false "PULSE" "Pulseaudio suite.  Includes pulseaudio, pavucontrol, and pulseaudio-utils." "Unable to determine" "Unable to determine" \
 false "PIPEWIRE" "Removes Pipewire and reverts system to pulseaudio." "Unable to determine" "Unable to determine" \
 false "UBLOX" "Installs programs and settings to use the UBLOX GPS for system time." "Unable to determine" "Unable to determine" \
 false "FORMS" "AmRRON Form templates will be downloaded." "Unknown" "Version 5.0 / 5.1" \
 ${r} ${c} >>/tmp/results

rc=$?
 if [[ $rc == 2 ]]; then
  radioArray=(HAMLIB FLDIGI FLMSG FLAMP FLRIG FLWRAP ARDOPCF ARDOPGUI ARIM GARIM VARIM PAT FINDARDOP VOACAP CHIRP WSJTX_STABLE JS8CALL PULSE UBLOX FORMS COMMSTATONE)

	for i in "${radioArray[@]}"
		do
		echo "$i" >> /tmp/results

		done

	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1

  fi

}

### Packet Radio tools ###

function packet_installer() {

	#Set user install directory For Packet Tools.
	PacketDir=$(yad --image=$DIR/images/corps-icon.png --center --radiolist --print-column=2 --separator="" \
	--title "AmRRON Packet Tools Installer / Updater" \
	--text "Select your desired installation folder for Packet Radio Programs." \
	--list --wrap-width=200 --wrap-cols=3 \
	--column="Tick" --column="Option" --column="Description" \
	 true "Default" "Default location will be $HOME/Radio/Packet" \
	 false "Custom" "I wish to select my own location" \
	 ${r} ${c}) ||
	 { yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }


	case ${PacketDir} in
	    Default)
	      yad --center \
	      --image=$DIR/images/corps-icon.png \
	      --title "Packet Tools Directory" \
	      --text "You have selected $HOME/Radio/Packet as your installation directory" ${r} ${c} ||
	      { packet_installer; return; }

	        PACKETDIR=$HOME/Radio/Packet
	        if [[ ! -d $PACKETDIR ]]; then
	        mkdir -p $PACKETDIR
	        fi
	        ;;
	    Custom)
	        PACKETDIR=$(yad --image=$DIR/images/corps-icon.png --center --form --separator="" --field="MDIR:MDIR" $HOME \
	        --title "Packet Tools Directory" \
	        --text "Please select your desired directory or create a new one." ${r} ${c}) ||
	        { packet_installer; return; }
	        if [[ ! -d $PACKETDIR ]]; then
	        mkdir -p $PACKETDIR
	        fi
	        ;;
	    esac


yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Packet Programs" \
--text "Select the packet programs you would like to install"  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Category" --column="Description" --column="Installed" \
 false "MINICOM" "Basic command line serial terminal.  Useful for SSH access." "${MINICOMVER}" \
 false "GTKTERM" "GTK based terminal - Recommended" "${GTKTERMVER}" \
 false "PUTTY" "Popular graphical terminal" "${PUTTYVER}" \
 false "DIREWOLF" "Soundcard TNC" "" \
 false "YAAC" "Yet Another APRS Client" "${YAACVER}" \
 false "PACKET_GUIDE" "Creates a guide in $HOME/Documents with basic information to get you started with packet operations" \
 ${r} ${c} >>/tmp/results
rc=$?

 if [[ $rc == 2 ]]; then
  packetArray=(MINICOM GTKTERM PUTTY DIREWOLF YAAC PACKET_GUIDE)

	for i in "${packetArray[@]}"
		do
		echo "$i" >> /tmp/results

		done

	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1

  fi

}

#### Other tools menu ###

function other_installer(){
yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Other Programs" \
--text "Select the other programs you would like to install. RASPAP not included in 'Install All'"  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Category" --column="Description" --column="Installed" \
 false "GEDIT" "Graphical Text Editor." "${GEDITVER}" \
 false "VIM" "VIM text editor." "${VIMVER}" \
 false "FILEZILLA" "Graphical FTP program" "${FZVER}" \
 false "MATCHBOX_KEYBOARD" "Touchscreen keyboard" "${KEYBOARDVER}" \
 false "SCREEN" "Useful tool for terminal management." "${SCREENVER}" \
 false "SPEEDTEST" "Run internet speedtests from the command line." "${SPEEDTESTVER}" \
 false "ELECTRUM" "No longer supported." "" \
 false "RASPAP" "Turn your Pi 3 into an Access Point (Hotspot)." "" \
 false "CONKY" "Conky system monitor" "${CONKYVER}" \
 false "LIBREOFFICE" "LibreOffice Suite" "${LIBREVER}" \
 ${r} ${c} >>/tmp/results
rc=$?

 if [[ $rc == 2 ]]; then
  otherArray=(GEDIT VIM FILEZILLA MATCHBOX_KEYBOARD SCREEN SPEEDTEST CONKY LIBREOFFICE)

	for i in "${otherArray[@]}"
		do
		echo "$i" >> /tmp/results

		done

	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1

  fi


if grep -Fxq "CONKY" /tmp/results; then
    CONKYSEL=$(yad --image=$DIR/images/corps-icon.png --center --radiolist --print-column=2 --separator="" \
--title "Conky Configuration" \
--text "Select the Conky Configuration you would like to install."  \
--list --wrap-width=150 --wrap-cols=3 \
--column="Tick" --column="Name" --column="Description" \
 false "LARGE" "Designed for 1920x1080 screens." \
 true "STANDARD" "Designed for 1280x720 screens." \
 false "MINIMAL" "Provides AmCON and TNC Status for small screen." \
 ${r} ${c})

fi

}


#### Security tools menu ###

function security_installer() {

	#Set user install directory For Security Tools.
	SecDir=$(yad --image=$DIR/images/corps-icon.png --center --radiolist --print-column=2 --separator="" \
	--title "AmRRON Packet Security Installer / Updater" \
	--text "Select your desired installation folder for Security Programs." \
	--list --wrap-width=200 --wrap-cols=3 \
	--column="Tick" --column="Option" --column="Description" \
	 true "Default" "Default location will be $HOME/Security" \
	 false "Custom" "I wish to select my own location" \
	 ${r} ${c}) ||
	 { yad --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }


	case ${SecDir} in
	    Default)
	      yad --center \
	      --image=$DIR/images/corps-icon.png \
	      --title "Security Tools Directory" \
	      --text "You have selected $HOME/Security as your installation directory" ${r} ${c} ||
	      { security_installer; return; }

	        SECDIR=$HOME/Security
	        if [[ ! -d $SECDIR ]]; then
	        mkdir -p $SECDIR
	        fi
	        ;;
	    Custom)
	        SECDIR=$(yad --image=$DIR/images/corps-icon.png --center --form --separator="" --field="MDIR:MDIR" $HOME \
	        --title "Security Tools Directory" \
	        --text "Please select your desired directory or create a new one." ${r} ${c}) ||
	        { security_installer; return; }
	        if [[ ! -d $SECDIR ]]; then
	        mkdir -p $SECDIR
	        fi
	        ;;
	    esac


yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Security Programs" \
--text "Select the security programs you would like to install.  Wireless modifications will not be selected with 'Install All'"  \
--list --wrap-width=150 --wrap-cols=3 \
--button="gtk-close:1" --button="Install All:2" --button="Install Selected:0" \
--column="Tick" --column="Category" --column="Description" --column="Installed" \
 false "FAIL2BAN" "Intrusion Prevention Software. Recommended" "${FAIL2BANVER}" \
 false "KEEPASS" "KepassXC password manager" "${KEEPASSXCVER}" \
 false "ICEWEASEL" "Firefox secure browser" "${ICEWEASELVER}" \
 false "PTE" "Paranoia Tools Text Encryption.  Encrypt/Decrypt messages and perform Steganography" "${PTEVER}" \
 false "PFE" "Paranoia Tools File Encrypter.  Encrypt/Decrypt files" "${PFEVER}" \
 false "GTKHASH" "GTK based file hash tool" "${GTKHASHVER}" \
 false "GPA" "GNU Privacy Assist Key Manager" "${GPAVER}" \
 false "KLEOPATRA" "GnuPG Certificate Manager and Crypto tool" "${KLEOPATRAVER}" \
 false "VERACRYPT" "Disk/File Encryption. $NEWVC" "${VCVER}" \
 false "BITMESSAGE" "Bitmessage secure decentralized email" "${BMVER}" \
 false "HARDEN_SSH" "Change folder permissions and provide SSH tips document." "" \
 false "WIFI-DISABLE" "Disable WiFi -- Raspberry Pi 3 ONLY" "" \
 false "BT-DISABLE" "Disable Bluetooth -- Raspberry Pi 3 ONLY" "" \
 false "WIFI-ENABLE" "Re-Enable WIFI -- Raspberry Pi 3 ONLY" "" \
 false "BT-ENABLE" "Re-Enable WIFI -- Raspberry Pi 3 ONLY" "" \
 ${r} ${c} >>/tmp/results
rc=$?

 if [[ $rc == 2 ]]; then
  securityArray=(FAIL2BAN KEEPASS ICEWEASEL PTE PFE VERACRYPT BITMESSAGE HARDEN_SSH GTKHASH KLEOPATRA)

	for i in "${securityArray[@]}"
		do
		echo "$i" >> /tmp/results

		done

	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1

  fi

}





################################################################################
################################################################################
################################################################################


#Lets give a quick primer on how to use this.
function primer_menu() {

echo "This program will assist you in installing or updating radio tools on your \
machine.

It is recommended to use on a dedicated machine with a fresh install of Linux Mint 22.1 \
with the Cinammon desktop environment.  

It is currently designed and tested mostly for Linux Mint 21.3, and Linux Mint 22.

Future updates will only be made for Mint 22.1 support.

If you previously installed FLDIGI on this machine using a package \
manager, please quit the installer, remove FLDIGI, and restart the program.

This program is designed to build FLDIGI from source to ensure you have the newest version \
possible.

Super User (sudo) priveledges are required to perform the apt-get \
commands.

You will be asked for your password in the terminal to perform some \
tasks but you should not run the entire program as Root. " | yad \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--center \
--title "Welcome to the AmRRON Installer / Updater." \
${r} ${c} ||
{ yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }

}

function notify_sources_menu() {
echo "You will need to ensure Source Code Repositories are enabled prior to \
attempting builds of HAMLIB or the FLDIGI Suite of programs.

Select 'Enable Source Code' for the installer to assist you.

Select 'Already Enabled' if you have previously completed this step." | yad \
--image=$DIR/images/corps-icon.png \
--center \
--text-info \
--wrap \
--button="Quit:1" \
--button="Already Enabled:0" \
--button="Enable Source Code:2" \
--title "Welcome to the AmRRON Installer / Updater." \
${r} ${c}
rc=$?

if [[ $rc == 1 ]]; then
{ yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }

elif [[ $rc == 2 ]]; then
{ enable_source_repositories; return; }

elif [[ $rc == 0 ]]; then
yad --center \
--image=$DIR/images/corps-icon.png \
--title "Source Repositories previously enabled" \
--text "You have confirmed you have enabled source repositories on your system." ${r} ${c} ||
{ yad --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}; exit 1; }
fi
}




function enable_source_repositories() {

if [[ $(uname -m) = aarch64 ]]; then
	if [[ ${OSCN} = "stretch" ]]; then
sudo echo 'deb-src http://raspbian.raspberrypi.org/raspbian/ stretch main contrib non-free rpi' | \
sudo tee -a /etc/apt/sources.list && sudo apt-get -y update && \
yad \
--image=$DIR/images/corps-icon.png \
--center --button="gtk-ok:0" \
--title "Source Code Repositories Enabled" \
--text "Successfully enabled source code repositories" ${r} ${c}

	elif [[ ${OSCN} = "buster" ]]; then

sudo echo 'deb-src http://raspbian.raspberrypi.org/raspbian/ buster main contrib non-free rpi' | \
sudo tee -a /etc/apt/sources.list && sudo apt-get -y update && \
yad \
--image=$DIR/images/corps-icon.png \
--center --button="gtk-ok:0" \
--title "Source Code Repositories Enabled" \
--text "Successfully enabled source code repositories" ${r} ${c}	

	fi
elif [[ ${PLAT} = "Ubuntu" ]]; then

software-properties-gtk

elif [[ ${PLAT} = "LinuxMint" ]]; then

software-sources

else

yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Not a Supported OS" --text "Please manually enable software sources before continuing." ${r} ${c}
exit 1
fi

rc=$?

if [[ $rc -eq 0 ]]; then
{ notify_sources_menu; return; }

elif [[ $rc -eq 1 ]]; then
if [[ $(uname -m) = aarch64 ]]; then
echo "This installer failed to modify your /etc/apt/sources.list/n/nPlease manually \
uncomment the deb-src line by removing the leading # before running this installer" | yad --center \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--button="gtk-ok:0" \
--title "Software Sources Modification Failed" \
${r} ${c}
exit 1

else
echo "This installer failed to open your Sofware Sources menu.\n\nYou can do this \
by pressing the Super-Key (Windows Key) and searching for 'Software Sources' in Mint or \
'Software and Updates,' in Ubuntu.\n\nCheck to ensure Source Code is enabled prior to \
running this installer." | yad --center \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--button="gtk-ok:0" \
--title "Software Sources Modification Failed" \
 ${r} ${c}
{ notify_sources_menu; return; }
fi

fi
}

################################################################################
###################  Perform system updates to begin  ##########################
################################################################################

function system_update_menu() {

echo "It is recommended that you perform a system update before continuing.

If you have not performed a sudo apt-get update and sudo apt-get upgrade today \
select 'Update' to perform it now.

Otherwise, select 'Skip' to continue without checking for updates.

Would you like to perform a repository update at this time?

**RECOMMENDED**" | yad --center \
--image=$DIR/images/corps-icon.png \
--text-info \
--wrap \
--button="gtk-close:1" --button="Skip:2" --button="Update:0" \
--title "System Update Recommended" \
${r} ${c}
rc=$?
  if [[ $rc == 0 ]]; then
    sudo apt-get -y update && sudo apt-get -y upgrade
	elif [[ $rc == 2 ]]; then
		echo "Skipping update check this time"
	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1
  fi



# Install git now.
if hash git 2>/dev/null; then
    echo "Git is already installed"
        else
            sudo apt-get -y install git
fi

}

################################################################################
##############################   Swap Size   ###################################
################################################################################
function swap_size_check() {

if [[ $(uname -m) =~ (armv7l|aarch64) ]]; then

SWAPFILE=/etc/dphys-swapfile

CURRENTSWAPSIZE=$(cat $SWAPFILE | grep CONF_SWAPSIZE= | awk -F "=" '{print $2}')

FREEMEM=$(free -m | grep Mem: | awk '{ print $2 }')


if [[ $FREEMEM -lt 3000 ]]; then

if [[ $CURRENTSWAPSIZE -lt 2000 ]]; then

    echo "Some users have found that increasing the swap size may help prevent crashing during compiling of FLDIGI.

Your current swapfile size is $CURRENTSWAPSIZE.

2000 is recommended.

A reboot will be required before continuing." | yad \
 --image=$DIR/images/corps-icon.png \
 --center \
 --text-info \
 --wrap \
 --button="Quit:1" \
 --button="No:2" \
 --button="Increase:0" \
 --title "Swap Size." \
 ${r} ${c}
rc=$?

 if [[ $rc == 2 ]]; then
	echo "You selected to not increase your swap file size.  If your system freezes up on compiling FLDIGI, please try increasing the swap file." | yad \
--image=$DIR/images/corps-icon.png \
--center \
--text-info \
--wrap \
--button="Quit:1" \
--button="Continue:0" \
--title "Welcome to the AmRRON Installer / Updater." \
${r} ${c}
	elif  [[ $rc == 1 ]]; then
		yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
		exit 1
    elif [[ $rc == 0 ]]; then
        sudo sed -i s/CONF_SWAPSIZE=${CURRENTSWAPSIZE}/CONF_SWAPSIZE=2000/g $SWAPFILE && \
            yad --image=$DIR/images/corps-icon.png \
            --center \
	    --title="Swap Size" \
            --text="Your machine will reboot in 10 seconds" \
            --timeout=10 \
            --timeout-indicator=top \
            --button="Cancel:1" \
            --button="Reboot Now:0" \
            ${r} ${c}
             rc=$?
             if [[ $rc == 1 ]]; then
                 exit 1
             else
                 sudo reboot
             fi

  fi
fi
fi

fi
}

################################################################################
##############################  Master Menu  ###################################
################################################################################

function old_main_menu() {

yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Categories" \
--text "Select the categories of programs you would like to install"  \
--list --wrap-width=200 --wrap-cols=3 \
--column="Tick" --column="Category" --column="Description" \
 false "RADIO" "FLDIGI suite, ARDOP/ARIM, CHIRP, AmRRON Forms, etc." \
 false "PACKET" "Minicom, GTKTerm, Direwolf, etc" \
 false "SDR" "Nothing in here yet.  Stay tuned..." \
 false "SECURITY" "Veracrypt, Fail2ban, KeepassXC, Bitmessage, disable wireless etc." \
 false "OTHER" "Electrum, Hotspot, touch keyboard, gedit,etc" \
 false "ICONS" "Creates Desktop Icons for installed programs" \
 ${r} ${c} >>/tmp/mastermenu



CHOICES=$(cat /tmp/mastermenu)
	while read mastermenu
	do
		case $mastermenu in
			RADIO) echo "Selecting Radio Tools menu"
				radio_installer
				;;
			PACKET) echo "Selecting Packet Tools menu"
				packet_installer
				;;
			SECURITY) echo "selecting Security Tools menu"
				security_installer
				;;
			SDR) echo "selecting SDR Tools menu"
				sdr_installer
				;;
			OTHER) echo "selecting Other Tools menu"
				other_installer
				;;
			ICONS) echo "ICONS" >> /tmp/results
		esac
	done < /tmp/mastermenu

}

function main_menu() {

yad --image=$DIR/images/corps-icon.png --center --checklist --print-column=2 --separator="" \
--title "Select Categories" \
--text "Select the categories of programs you would like to install. Easy Button does everything to default locations."  \
--list --wrap-width=200 --wrap-cols=3 \
--button="gtk-close:1" --button="Easy Button:2" --button="Install Selected:0" \
--column="Tick" --column="Category" --column="Description" \
 false "RADIO" "FLDIGI suite, ARDOP/ARIM, CHIRP, AmRRON Forms, etc." \
 false "PACKET" "Minicom, GTKTerm, Direwolf, etc" \
 false "SDR" "Nothing in here yet.  Stay tuned..." \
 false "SECURITY" "Veracrypt, Fail2ban, KeepassXC, Bitmessage, disable wireless etc." \
 false "OTHER" "Hotspot, touch keyboard, gedit,etc" \
 false "ICONS" "Creates Desktop Icons for installed programs" \
 ${r} ${c} >>/tmp/mastermenu

rc=$?

 if [[ $rc == 2 ]]; then
  cat /dev/null > /tmp/mastermenu
  SECDIR=$HOME/Security
   if [[ ! -d $SECDIR ]]; then
          mkdir -p $SECDIR
          fi
  PACKETDIR=$HOME/Radio/Packet
   if [[ ! -d $PACKETDIR ]]; then
          mkdir -p $PACKETDIR
          fi
  USERDIR=$HOME/Radio
   if [[ ! -d $USERDIR ]]; then
          mkdir -p $USERDIR
          fi
  CONKYSEL=STANDARD
  easyArray=(HAMLIB FLDIGI FLMSG FLAMP FLRIG FLWRAP ARDOPCF ARDOPGUI ARIM GARIM VARIM PAT FINDARDOP VOACAP CHIRP WSJTX_STABLE JS8CALL PULSE UBLOX FORMS MINICOM GTKTERM PUTTY DIREWOLF YAAC PACKET_GUIDE GEDIT VIM FILEZILLA MATCHBOX_KEYBOARD SCREEN SPEEDTEST CONKY LIBREOFFICE FAIL2BAN KEEPASS ICEWEASEL PTE PFE VERACRYPT BITMESSAGE HARDEN_SSH GTKHASH KLEOPATRA ICONS COMMSTATONE)

  for i in "${easyArray[@]}"
    do
    echo "$i" >> /tmp/results

    done

  elif  [[ $rc == 1 ]]; then
    yad --image=$DIR/images/corps-icon.png --center --button="gtk-ok:0" --title "Exit Selected" --text "Exiting installer now" ${r} ${c}
    exit 1

  fi

CHOICES=$(cat /tmp/mastermenu)
  while read mastermenu
  do
    case $mastermenu in
      RADIO) echo "Selecting Radio Tools menu"
        radio_installer
        ;;
      PACKET) echo "Selecting Packet Tools menu"
        packet_installer
        ;;
      SECURITY) echo "selecting Security Tools menu"
        security_installer
        ;;
      SDR) echo "selecting SDR Tools menu"
        sdr_installer
        ;;
      OTHER) echo "selecting Other Tools menu"
        other_installer
        ;;
      ICONS) echo "ICONS" >> /tmp/results
    esac
  done < /tmp/mastermenu

}


################################################################################
#########################  Execute the selections  #############################
################################################################################

function selection_results() {

if [[ -s /tmp/results ]]; then
cat /tmp/results | yad --text-info --center \
--image=$DIR/images/corps-icon.png \
--button="Quit:1" \
--button="Begin:0" \
--title "Confirm Update Selection" \
--text "You have chosen to install / update the following:" ${r} ${c} || exit 1
 else
    yad --center \
	--image=$DIR/images/donate.png \
	--button="gtk-ok:0" \
	--title "Goodbye" \
	--text "You did not make any selections.\n\nThe installer will now exit.\n\nThank you." ${r} ${c}
    exit 1
 fi


 while read results
 do
	 case $results in

  	HAMLIB) echo "Updating HAMLIB"
		hamlib_install
   		;;
    HAMLIB4.5.5) echo "Installing Hamlib 4.5.5"
        hamlib4.5.5_install
        ;;
  	FLDIGI) echo "Installing / Updating FLDIGI"
		fldigi_install
   		;;
  	FLMSG) echo "Updating FLMSG"
  		flmsg_install
   		;;
    FLMSG_4.0.19) echo "Updating FLMSG"
        flmsg_install_19
        ;;
  	FLAMP) echo "Updating FLAMP"
  		flamp_install
   		;;
    FLAMP_2.2.14) echo "Updating FLAMP"
        flamp_install_14
        ;;
	FLWRAP) echo "Updating FLWRAP"
  		flwrap_install
   		;;
	FLRIG) echo "Updating FLRIG"
  		flrig_install
   		;;
  	ARDOP) echo "Updating ARDOP"
   		ardop_install
   		;;
    ARDOPCF) echo "Updating ARDOPCF"
        ardopcf_install
        ;;
	ARDOPGUI) echo "Updating ARDOP_GUI"
   		ardop_gui_install
   		;;
  	ARIM) echo "Updating ARIM"
   		arim_install
   		;;
	GARIM) echo "Updating GARIM"
   		garim_install
   		;;
    VARIM) echo "Updating VARIM"
      varim_install
        ;;
	WSJTX) echo "WSJTX newest Release Candidate"
		wsjtx_install
		;;
    WSJTX_STABLE) echo "WSJTX General Availability Release"
        wsjtx_stable_install
        ;;
	PULSE) echo "Installing pulseaudio suite"
		pulse_audio_suite_install
		;;
    PIPEWIRE) echo "Purging pipewire"
        pipewire
        ;;
  	ICONS) echo "Creating Desktop Shortcuts."
   	 	echo desktopshortcuts >> /tmp/cleanup
   		;;
  	PAT) echo "Updating PAT"
	 	pat_install
		;;
  	FINDARDOP) echo "Getting KM4ACK's find ardop tools."
   		getardoptools_install
   		;;
	VOACAP) echo "VOACAP Propagation tool."
   		voacap_install
   		;;
	CHIRP) echo "Updating Chirp"
		chirp_install
		;;
	JS8CALL)
		js8call_install
		;;
    COMMSTATONE)
        comstat_install
        ;;
	UBLOX)
		ublox_gps_setup
		echo ublox >> /tmp/cleanup
		;;
  	MINICOM) echo "Installing Minicom"
		minicom_install
		;;
	GTKTERM) echo "Installing GtkTerm"
		gtkterm_install
	  	;;
	PUTTY) echo "Installing Putty"
		putty_install
	  	;;
	DIREWOLF) echo "Installing Direwolf"
		direwolf_install
		;;
	YAAC) echo "Skipping YAAC"
		yaac_install
		;;
	PACKET_GUIDE) echo "Creating Packet Radio Guide"
		packet_guide
		;;
	GEDIT) echo "Installing Gedit"
		gedit_install
		;;
	VIM) echo "Installing VIM"
		vim_install
		;;
	FILEZILLA) echo "Installing Filezilla"
		filezilla_install
		;;
	MATCHBOX_KEYBOARD) echo "Installing On-Screen Keyboard"
		keyboard_install
	  	;;
	SCREEN) echo "Installing Screen"
		screen_install
	  	;;
	SPEEDTEST) echo "Installing Speedtest"
		speedtest_install
	  	;;
	RASPAP)
		raspap_install
		;;
	CONKY)
		conky_install
		;;
	FAIL2BAN) echo "Installing fail2ban"
		fail2ban_install
	        ;;
	KEEPASS) echo "Installing keepassxc"
		keepassxc_install
	        ;;
	ICEWEASEL) echo "Installing iceweasel"
		iceweasel_install
	        ;;
	GTKHASH) echo "Installing GTK Hash"
		gtkhash_install
		;;
	GPA) echo "Installing GNU Privacy Assist"
		gpa_install
		;;
    KLEOPATRA) echo "Installing Kleopatra GnuPG manager"
        kleopatra_install
        ;;
	HARDEN_SSH) echo "Hardening SSH and creating guide at $HOME/Documents"
		harden_ssh
	        ;;
	PTE) echo "Installing Paranoia Text Encrypter"
		pte_install
		;;
	PFE) echo "Installing Paranoia File Encrypter"
		pfe_install
		;;
	BITMESSAGE) echo "Installing Bitmessage"
		bitmessage_install
	        ;;
	VERACRYPT) echo "Installing Veracrypt"
		veracrypt_install
		;;
	WIFI-DISABLE) echo "Disabling wireless hardare"
		wifi_off
	        ;;
	BT-DISABLE) echo "Disabling Bluetooth"
		bt_off
	        ;;
	WIFI-ENABLE) echo "Re-Enabling wireless hardare"
		wifi_on
	        ;;
	BT-ENABLE) echo "Re-Enabling Bluetooth"
		bt_on
	        ;;
	LIBREOFFICE) echo "Installing LibreOffice Suite"
		libreoffice_install
		;;
    FORMS) echo "Retriving AmRRON Forms"
        amrronforms_install
        ;;
esac
done < /tmp/results

}


################################################################################
################################################################################
############################  Failure Notifications ############################
################################################################################
################################################################################

function failure_handling() {

FAIL=$(cat /tmp/failures) || echo "No failures recorded"


if [[ -f /tmp/failures ]]; then
yad --image=$DIR/images/corps-icon.png --center --button="Save Log:2" --button="gtk-ok:0" \
--title "Failures" \
--text "The following failures occured during installation:\n\n$FAIL" ${r} ${c} exit=$?
if [[ $? == 2 ]]; then
ERRORLOG=$HOME/AmRRON-Setup-Tool-failures
if [[ -e $ERRORLOG.log ]]; then
    i=1
    while [[ -e $ERRORLOG-$i.log ]] ; do
        let i++
    done
    ERRORLOG=$ERRORLOG-$i
fi
cp /tmp/failures $ERRORLOG.log
yad --center --button="gtk-ok:0" \
--image=$DIR/images/corps-icon.png \
--title "Failure Log Saved" \
--text "A copy of the failure log was saved at $ERRORLOG.log" ${r} ${c}
fi

fi

}

################################################################################
################################################################################
##################################  Clean up  ##################################
################################################################################
################################################################################

function cleanupyad() {
echo "${TEXT}" | yad \
--center --text-info \
--image=$DIR/images/corps-icon.png --wrap \
--title "${TITLE}" \
${r} ${c}
}

function final_cleanup() {

while read cleanup
do
    case $cleanup in

	desktopshortcuts)
	create_icons
	;;
	ardop)

TITLE="ARDOP Versions"
TEXT="There are two main version of the ARDOP TNC.  This installer \
used install version 1 on your system.  Version 1 has increased \
stability and is the default TNC used for AmRRON ARDOP/ARIM \
communications at this time."
    cleanupyad

	;;

        ardopcf)

TITLE="ARDOP Versions"
TEXT="This installer compiled the current fork of ardop from https://github.com/pflarue/ardop.git.

  You can run the setup script and choose ARDOPC for the old version if you have issues.

  Installed Version checking is determined by a text file located in your ARDOP directory.
  Please do not remove this file or add other text files in this directory.
  Re-running the ARDOPCF selection will query if there is more current build."
    cleanupyad

    ;;

	asoundrc)

TITLE="ARDOP configuration"
TEXT="A configuration file is required for your sound device to \
work with the ARDOP TNC.

This file is located at $HOME/.asoundrc

You may need to substitute the appropriate hardware location for your \
device.

You can determine your soundcard location by running the \
command

aplay -l

This will show your attached sound devices and their card numbers.  \
Please edit your $HOME/.asoundrc file appropriately if your card is not 1."
   cleanupyad

	;;

    ardopguifail)
TITLE="ARDOP_GUI Not Supported"
TEXT="ARDOP_GUI requires qt5 in order to operate.

At this time, I have not determined the full set of dependencies in order to run \
ARDOP_GUI on a 64bit OS prior to Mint 19+ and Ubuntu 18.04+"
  cleanupyad
        ;;

 	
      comstatone)
TITLE="ComstatONE Downloaded"
TEXT="ComstatONE has been downloaded but requires manual installation on MINT 21.3 for now.


MINT 21.3: Please navigate to your Radio directory and enter the commstatone directory.  \
Run the script with './linuxinstall.sh'. After installation, \
type python commstat.py or python3 commstat.py from the installaion directory or use the desktop launcher.


MINT 22: This script should have fully installed commstatone.

IF it failed, you will need to execute the following to complete install.

        cd ~/Radio/commstatone
        source venv/bin/activate
        ./linuxinstall.sh

IF you need to run from the terminal (just using the desktop launcher is recommened): 
                        cd ~/Radio/commstatone
                        source /venv/bin/activate
                        python3 commstat.py



You will need to provide the path to you DIRECTED.TXT file of JS8Call.  Default should be $HOME/.local/share/JS8Call \
Setup JS8Call first then determine the location of the DIRECTED.TXT file for your installation. "
  cleanupyad
        ;;
  
  ardoptools)

TITLE="KM4ACK Find Ardop"
TEXT="KM4ACK's script has been installed and a modified version for Vara created.  The commands are getardoplist / findardop \
getvaralist / findvara.

This requires PAT to be installed and configured in order to work properly.

If you'd like to automate the station list update, perform the following from terminal:

crontab -e

Select your favorite editor, nano is easy.

Add the following to the bottom:

30 23 * * * /usr/local/bin/getardoplist
30 23 * * * /usr/local/bin/getvaralist

Save and exit

The above example will update your list every night at 2330 system time."
  cleanupyad

  ;;

	wsjtxunsupported)
TITLE="WSJTX Not Supported"
TEXT="WSJTX version 2.0 and greater do not have an installation package compatible \
with your OS.  Ubuntu 18.04 and Mint 19 or greater are recommended.  You can attempt \
to compile from source.  Future versions of this installer may support compiling."
  cleanupyad

	;;

    js8callnotice)
echo "This installer searched for, downloaded and installed JS8Call in order to make the initial setup of your machine easier and provide an easy way to locate and apply the next updates...

The author's intent however, is for all users of his software join the JS8Call mailing group.  This allows for the author to communicate changes the users, gauge the level of interest in his work, and for users to share bugs with the community.  Please take a moment to follow this link, register an account if needed. and join the JS8Call group.

https://groups.io/g/js8call" | yad --center --text-info --wrap --image=$DIR/images/corps-icon.png --show-uri --title "JS8Call Installed" ${r} ${c}

        ;;

	ublox)
TITLE="UBLOX GPS Time"
TEXT="You selected to setup your system to work with the Ublox GPS for system time.

Settings for this are contained in /etc/default/gpsd and /etc/chrony/chrony.conf.

If you are using a different gps, you may need to modify the settings in these files."
cleanupyad
	;;

	wirelessoff)

TITLE="Wireless Disable"
TEXT="You have selected to disable WIFI / Bluetooth from this system.  \
Changes will take effect upon reboot.

An information text file has been created in $HOME/Documents for details."
  cleanupyad

	;;

	wirelessnotoff)

TITLE="Wireless Enable Failure"
TEXT="You have selected to enable WIFI / Bluetooth from this system but \
no entry disabling these was found."
cleanupyad

	;;

  	wirelesserror)

TITLE="Wireless Modification Failure"
TEXT="You have selected to modify WIFI / Bluetooth from this system.

This function is only applicable to Raspberry Pi 3 but the installer did not \
recognize your system as a Raspberry Pi"
cleanupyad

	;;

  	noelectrum)

TITLE="Electrum Unavailable"
TEXT="This script no longer supports installing electrum.  You will need to manually determine \
how you will need to install it for your system."
cleanupyad

	;;

    snap_fail_chirp)

TITLE="Requires Reboot"
TEXT="If you received an installation error for Chirp, a restart is required.  \
Snapd was installed in order to manage the installation of Chirp on this system. \
Snap requires a reboot after its first installation before being able to install your programs. \
Please reboot, run this installer again and reselect Chirp if you would like to install it."
  cleanupyad
        ;;

    snap_fail_keepass)

TITLE="Requires Reboot"
TEXT="If you received an installation error for KeepassXC, a restart is required.  \
Snapd was installed in order to manage the installation of KeepassXC on this system. \
Snap requires a reboot after its first installation before being able to install your programs. \
Please reboot, run this installer again and reselect KeepassXC if you would like to install it."
  cleanupyad
        ;;

	raspapsetup)

TITLE="RaspAP Setup."
TEXT="A seperate installer for the RaspAP has been downloaded to your desktop.

Additionally, further information can be found in \
$HOME/Documents/RaspAP-Hotspot-Setup.txt

Run the script from the Desktop after rebooting from the current installations."
cleanupyad

  	;;

	noraspapsetup)

TITLE="RaspAP Setup."
TEXT="You have selected the RaspAP installation but the installer did not detect \
your system as Raspbian.

Installation skipped."
    cleanupyad

    	;;

	js8call_os)

TITLE="JS8Call Unsupported Operating System."
TEXT="JS8Call does not yet support Ubuntu 19.10."
    cleanupyad

    	;;

voacap_os)

TITLE="VOACAPL pythonprop GUI Unsupported Operating System."
TEXT="VOACAPL pythonprop GUI does not support your OS."
    cleanupyad

    	;;

    pybitmessagesetup)
if [[ $(uname -m) =~ (armv7l|aarch64) ]]; then
echo "Add @/usr/local/bin/pybitmessage to the file /etc/xdg/lxsession/LXDE-pi/autostart \
to enable autostart of pybitmessage on the Pi. Would you like me to attempt to do this now?" | yad \
--text-info --image=$DIR/images/corps-icon.png --center --wrap \
--button="No:0" --button="Enable Autostart:2" \
--title "PyBitmessage Autostart Setup." \
${r} ${c}
rc=$?
if [[ $rc -eq 2 ]]; then
	echo "@/usr/local/bin/pybitmessage" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart ||
                      { ERRCODE="Failed to setup pybitmessage autostart"; errors; }
fi
else
TITLE="Pybitmessage Autostart Setup."
TEXT="If you would like to auto start pybitmessage on boot, please use your distribution's Startup \
Applications setup and add pybitmessage to the list."
    cleanupyad
fi
	;;

  pybitmessagefail)
if [[ $OSCN =~ (una|vanessa) ]]; then
TITLE="Pybitmessage Support in Mint 20.3/21."
TEXT="PyBitmessage relies upon packages no longer supported in current distros. \
An Appimage is available that works.  Download the newest appimage release from \
https://appimage.bitmessage.org/releases/ \
Make the file executable (chmod a+x 'filename') \
There is a known problem that the developer is working on.  To workaround run the following \
before launching the appimage the first time.

sudo ln -sf /etc/xdg/menus/{gnome-applications,debian-menu}.menu"
    cleanupyad
  else
TITLE="Pybitmessage Support."
TEXT="PyBitmessage relies upon packages no longer supported in current distros. \
An Appimage is available that works for Mint 20.3/21 and likely most x86_64 distros at this time but is not currently \
available for raspberry pi ARM systems.  

For now, PyBitmessage is only available on Raspbian Buster.

Please watch the pybitmessage project for updates.
https://github.com/Bitmessage/PyBitmessage" 
    cleanupyad
fi  

 ;;


	conkysetup)
echo "Conky has been intalled and a configuration file created at $HOME/.conkyrc.  \
You can run conky by typing in a terminal:
conky &
Add @/usr/bin/conky to the \
file /etc/xdg/lxsession/LXDE-pi/autostart on raspbian or in linux create a desktop file \
in $HOME/.config/autostart/ to enable autostart of conky.
Would you like me to attempt to do this now?" | yad --image=$DIR/images/corps-icon.png \
--text-info --wrap --center --button="No:0" --button="Enable Autostart:2" \
--title "Conky Autostart Setup." ${r} ${c}
rc=$?
if [[ $rc -eq 2 ]]; then

    if [[ $(uname -m) = aarch64 ]]; then
	echo "@/usr/bin/conky -p 8 -d" | sudo tee -a /etc/xdg/lxsession/LXDE-pi/autostart ||
                      { ERRCODE="Failed to setup conky autostart"; errors; }

    else

mkdir -p  ~/.config/autostart
echo "[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=Conky
Comment=System Monitor
Exec=/usr/bin/conky -p 8 -d
StartupNotify=false
Terminal=false
Hidden=false" >  $HOME/.config/autostart/conky.desktop ||
 { ERRCODE="Failed to setup conky autostart"; errors; }

fi
fi
	;;

esac
done </tmp/cleanup

echo "All selected updates have been completed." | yad --image=$DIR/images/donate.png \
--text-info --center --wrap --button="gtk-ok:0" \
--title "Complete" ${r} ${c}

echo "It is recommended to restart your system before continuing.

Would you like to restart now?" | yad --center \
--text-info --wrap \
--image=$DIR/images/corps-icon.png \
--title "Restart" --button="No:1" --button="Restart:2" ${r} ${c}
rc=$?
  if [[ $rc -eq 2 ]]; then
	sudo shutdown -r now
  elif [[ $rc -eq 1 ]]; then
echo "Good luck and 73's.   If you have any issues or suggestions please contact tangobravo14@protonmail.com or submit a merge request on gitlab." | yad --image=$DIR/images/corps-icon.png \
--text-info --wrap \
--center --button="gtk-ok:0" \
--title "Thank you." ${r} ${c}
exit 1
  else
	echo "You closed the window without making a selection.  Installer exiting."

exit 1
  fi

}



################################################################################
##########################  Troubleshooting Area  ##############################
################################################################################






################################################################################
########################  Actually run this thing  #############################
################################################################################

check_for_updates
yad_install
set_screen_size
check_os_support
#check_repo_version
check_installed_programs
query_updates
add_dialout
primer_menu
#notify_sources_menu
system_update_menu
swap_size_check
main_menu
selection_results
failure_handling
final_cleanup

################################################################################
################################################################################
#################################  TB-14 2019  #################################
################################################################################
################################################################################
